from django.db import models


class UserGroup(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)

    class Meta:
        db_table = "user_group"


class GroupPermission(models.Model):
    id = models.AutoField(primary_key=True)
    user_group_id = models.ForeignKey(UserGroup, on_delete=models.PROTECT, null=True, blank=True)
    table_name = models.CharField(max_length=200)
    add = models.BooleanField(default=False)
    delete = models.BooleanField(default=False)
    edit = models.BooleanField(default=False)
    list = models.BooleanField(default=False)

    class Meta:
        db_table = "group_permission"


class UserAccount(models.Model):
    id = models.CharField(max_length=50, primary_key=True)
    first_name = models.CharField(max_length=30)
    sur_name = models.CharField(max_length=30)
    user_group_id = models.ForeignKey(UserGroup, on_delete=models.PROTECT, null=True, blank=True)
    user_name = models.CharField(max_length=50)
    password = models.CharField(max_length=50)
    primary_phone = models.CharField(max_length=30, null=True, blank=True)
    email = models.CharField(max_length=50, null=True, blank=True)
    entry_date = models.DateTimeField(null=True, blank=True)
    entered_by = models.CharField(max_length=50, null=True, blank=True)
    last_modified = models.DateTimeField(null=True, blank=True)
    modified_by = models.CharField(max_length=50, null=True, blank=True)

    class Meta:
        db_table = "user_account"


class NrmBook(models.Model):
    id = models.CharField(max_length=50, primary_key=True)
    book_name = models.CharField(max_length=50)
    front_page = models.CharField(max_length=200, null=True, blank=True)
    entry_date = models.DateTimeField(null=True, blank=True)
    entered_by = models.ForeignKey(UserAccount, on_delete=models.PROTECT, null=True, blank=True, related_name="nrm_book_entered_by")
    last_modified = models.DateTimeField(null=True, blank=True)
    modified_by = models.ForeignKey(UserAccount, on_delete=models.PROTECT, null=True, blank=True, related_name="nrm_book_modified_by")

    class Meta:
        db_table = "nrm_book"


class NrmBookPage(models.Model):
    id = models.CharField(max_length=50, primary_key=True)
    nrm_book = models.ForeignKey(NrmBook, on_delete=models.PROTECT, null=True, blank=True,
                                   related_name="nrm_book_page_nrm_book")
    page_number = models.IntegerField(null=True, blank=True)
    number_of_records = models.IntegerField(null=True, blank=True)
    file_attachment = models.CharField(max_length=200, null=True, blank=True)
    entry_date = models.DateTimeField(null=True, blank=True)
    entered_by = models.ForeignKey(UserAccount, on_delete=models.PROTECT, null=True, blank=True, related_name="nrm_book_page_entered_by")
    last_modified = models.DateTimeField(null=True, blank=True)
    modified_by = models.ForeignKey(UserAccount, on_delete=models.PROTECT, null=True, blank=True, related_name="nrm_book_page_modified_by")

    class Meta:
        db_table = "nrm_book_page"


class PageAllocation(models.Model):
    id = models.CharField(max_length=50, primary_key=True)
    nrm_book_page = models.ForeignKey(NrmBookPage, on_delete=models.PROTECT, null=True, blank=True,
                                   related_name="page_allocation_nrm_book_page")
    agent = models.ForeignKey(UserAccount, on_delete=models.PROTECT, null=True, blank=True,
                                   related_name="page_allocation_agent")
    entry_date = models.DateTimeField(null=True, blank=True)
    entered_by = models.ForeignKey(UserAccount, on_delete=models.PROTECT, null=True, blank=True, related_name="page_allocation_entered_by")
    last_modified = models.DateTimeField(null=True, blank=True)
    modified_by = models.ForeignKey(UserAccount, on_delete=models.PROTECT, null=True, blank=True, related_name="page_allocation_modified_by")

    class Meta:
        db_table = "page_allocation"


class NrmMemeber(models.Model):
    id = models.CharField(max_length=50, primary_key=True)
    nrm_book_page = models.ForeignKey(NrmBookPage, on_delete=models.PROTECT, null=True, blank=True,
                                      related_name="nrm_member_nrm_book_page")
    member_number = models.CharField(max_length=50)
    sur_name = models.CharField(max_length=50)
    other_name = models.CharField(max_length=50)
    gender = models.CharField(max_length=10)
    date_of_birth = models.DateField(null=True, blank=True)
    national_id_no = models.CharField(max_length=50)
    telephone_number = models.CharField(max_length=50, null=True, blank=True)
    youth_mark = models.CharField(max_length=10)
    elderly_mark = models.CharField(max_length=10)
    pwd_mark = models.CharField(max_length=10)
    remarks_code = models.CharField(max_length=10)
    entry_date = models.DateTimeField(null=True, blank=True)
    entered_by = models.ForeignKey(UserAccount, on_delete=models.PROTECT, null=True, blank=True, related_name="nrm_member_entered_by")
    last_modified = models.DateTimeField(null=True, blank=True)
    modified_by = models.ForeignKey(UserAccount, on_delete=models.PROTECT, null=True, blank=True, related_name="nrm_member_modified_by")

    class Meta:
        db_table = "nrm_member"


class PaymentRequest(models.Model):
    id = models.CharField(max_length=50, primary_key=True)
    agent = models.ForeignKey(UserAccount, on_delete=models.PROTECT, null=True, blank=True, related_name="payment_request_agent")

    number_of_records = models.IntegerField(null=True, blank=True)
    rate = models.FloatField(null=True, blank=True)
    amount = models.FloatField(null=True, blank=True)

    status = models.CharField(max_length=10)
    status_note = models.CharField(max_length=200)
    evidence_attachment = models.CharField(max_length=200, null=True, blank=True)

    entry_date = models.DateTimeField(null=True, blank=True)
    entered_by = models.ForeignKey(UserAccount, on_delete=models.PROTECT, null=True, blank=True, related_name="payment_request_entered_by")
    last_modified = models.DateTimeField(null=True, blank=True)
    modified_by = models.ForeignKey(UserAccount, on_delete=models.PROTECT, null=True, blank=True, related_name="payment_request_modified_by")

    class Meta:
        db_table = "payment_request"


class PaymentRequestDetail(models.Model):
    id = models.CharField(max_length=50, primary_key=True)
    payment_request = models.ForeignKey(PaymentRequest, on_delete=models.PROTECT, null=True, blank=True,
                              related_name="payment_request_detail_payment_request")

    nrm_member = models.ForeignKey(NrmMemeber, on_delete=models.PROTECT, null=True, blank=True,
                                        related_name="payment_request_detail_nrm_member")

    entry_date = models.DateTimeField(null=True, blank=True)
    entered_by = models.ForeignKey(UserAccount, on_delete=models.PROTECT, null=True, blank=True,
                                   related_name="payment_request_detail_entered_by")
    last_modified = models.DateTimeField(null=True, blank=True)
    modified_by = models.ForeignKey(UserAccount, on_delete=models.PROTECT, null=True, blank=True,
                                    related_name="payment_request_detail_modified_by")

    class Meta:
        db_table = "payment_request_detail"