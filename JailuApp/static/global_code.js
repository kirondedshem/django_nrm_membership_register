
// Check if same text
function sameText(o1, o2) {
    return (String(o1).toLowerCase() == String(o2).toLowerCase());
}

// Check if same string
function sameString(o1, o2) {
    return (String(o1) == String(o2));
}


function ShowForm(action, object, object_id = null, current_page = 1)
{
	const ex_filter_form_values = CollectExFilterParameters(object);

	var pagination = "{}";
	if(action === EW.action.List || action === EW.action.ExcelExport)
	{

		pagination = JSON.stringify({'records_per_page': $("#page_size").val(), 'current_page': current_page});
	}


    $.ajax({
		type: "POST",
		cache: false,
		url: "api",
		dataType :"json",
		data: { rand_time: new Date().getTime(), api_object: object, api_action: action, object_id: object_id, pagination: pagination, ex_filter_values: JSON.stringify(ex_filter_form_values)} ,
		//url: "test_api.php",
		success: function(response){
		  //console.log(response);
		  //read data out
		  if(response.error === false)
		  {
		  	//check if it an export and dont bother with content
			  if (action === EW.action.ExcelExport)
			  {
			  	console.log(response);
			  	const win = window.open('download?file_name='+response.download_file_name, '_blank');
				if (win) {
					//Browser has allowed it to be opened
					win.focus();
				} else {
					//Browser has blocked it
					ShowFailureMessage('Please allow popups for this website, To download exported files');
				}
			  }
			  else // any other actions deal with showing content
			  {
			  	$("#header_title").html(response.header_title);
			  $("#content_title").html(response.content_title);
			  $("#content_body").html(response.content_body);
			  }

		  }
		  else
		  {
		       //show error message
			  ShowFailureMessage(response.error_msg)
			  console.log('failed to pick get_map_activity_route error:'+response.error_msg);
		  }
		},
		error: function(data){
		console.log('failed to Load form:',data);
		//show error message
		ShowFailureMessage("Oops Server failed to complete this request");
		}
		});

}

function CollectFormParameters(object)
{
	var field_identifier = '[table_object_name="'+object+'"]';
	var form_values = {};
	//get all fields
	$.each( $(field_identifier), function( key, value ) {
		//console.log(this);
		var field_name = $(this).attr('field_object_name');
		var field_value = $(this).val();

		//add to collection
		form_values[field_name] = field_value;
	});
	return form_values;
}


function CollectExFilterParameters(object)
{
	var field_identifier = '#list_ex_filter_content [table_object_name="'+object+'"]';
	var form_values = {};
	//get all fields
	$.each( $(field_identifier), function( key, value ) {
		//console.log(this);
		var field_name = $(this).attr('field_object_name');
		var field_value = $(this).val();

		//add to collection
		form_values[field_name] = field_value;
	});
	return form_values;
}

function ShowSuccessMessage(mesage)
{
	Swal.fire({
		toast: true,
		position: 'top-end',
		showConfirmButton: false,
		timer: 3000,
		type: 'success',
		title: mesage
      })
}

function ShowFailureMessage(mesage)
{
	Swal.fire({toast: false,
		position: 'center',
	    type: 'error',
	    title: 'Oops',
		text: mesage,
		showConfirmButton: true
})
}

function ConfirmDelete(object,object_id,message)
{
	Swal.fire({
		position: 'center',
		title: 'Are you sure?',
		text: message,
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Yes delete it!',
}).then((result) => {
  if (result.value) {
    SubmitForm(EW.action.Delete, object, EW.action.List, object_id);
  }
})
}





function SubmitForm(action, object, next_action = EW.action.List, object_id = null)
{
	const active_form_values = CollectFormParameters(object);
	//console.log(active_form_values);
    $.ajax({
		type: "POST",
		cache: false,
		url: "api",
		dataType :"json",
		data: { rand_time:new Date().getTime(), api_object: object, api_action: action, object_id: object_id, form_values: JSON.stringify(active_form_values)} ,
		//url: "test_api.php",
		success: function(response){
		  //console.log(response);
		  //read data out
		  if(response.error === false)
		  {
		  	   //show success message
		  	   ShowSuccessMessage(response.error_msg);
			  //got to next page
			  ShowForm(next_action,object);
		  }
		  else
		  {
		      //show error message
			  ShowFailureMessage(response.error_msg)
		  }
		},
		error: function(data){
		console.log('failed to Load form:',data);
		//show error message
		ShowFailureMessage("Oops Server failed to complete this request");
		}
		});

}



function SubmitPermissionsForm(object_id = null)
{
	const action = "submit_permissions";
	const object = "user_group";
	const next_action = EW.action.List;
	var field_identifier = '[item_type="permission_row"]';
	var permission_list = [];
	//get all fields
	$.each( $(field_identifier), function( key, value ) {
		const permission_row = {};
		//console.log(this);
		const table_name = $(this).attr('table_name');
		permission_row["table_name"] = table_name;
		permission_row["add"] = $('[item_type="add"][table_value="'+table_name+'"]input').prop("checked");
		permission_row["edit"] = $('[item_type="edit"][table_value="'+table_name+'"]input').prop("checked");
		permission_row["delete"] = $('[item_type="delete"][table_value="'+table_name+'"]input').prop("checked");
		permission_row["list"] = $('[item_type="list"][table_value="'+table_name+'"]input').prop("checked");
		permission_list.push(permission_row);

	});

	//console.log(active_form_values);
    $.ajax({
		type: "POST",
		cache: false,
		url: "api",
		dataType :"json",
		data: { rand_time: new Date().getTime(), api_object: object, api_action: action, object_id: object_id, permission_list: JSON.stringify(permission_list)} ,
		//url: "test_api.php",
		success: function(response){
		  //console.log(response);
		  //read data out
		  if(response.error === false)
		  {
		  	   //show success message
		  	   ShowSuccessMessage(response.error_msg);
			  //got to next page
			  ShowForm(next_action,object);
		  }
		  else
		  {
		      //show error message
			  ShowFailureMessage(response.error_msg)
		  }
		},
		error: function(data){
		console.log('failed to Load form:',data);
		//show error message
		ShowFailureMessage("Oops Server failed to complete this request");
		}
		});

}



function SubmitLoginForm()
{
	const object_id = null
	const active_form_values = {};
	active_form_values["user_name"] = $("#user_name").val();
	active_form_values["password"] = $("#password").val();

	//console.log(active_form_values);
    $.ajax({
		type: "POST",
		cache: false,
		url: "api",
		dataType :"json",
		data: { rand_time: new Date().getTime(), api_object: "security_action", api_action: "submit_login", object_id: object_id, form_values: JSON.stringify(active_form_values)} ,
		//url: "test_api.php",
		success: function(response){
		  //console.log(response);
		  //read data out
		  if(response.error === false)
		  {
		  	   //show success message
		  	   ShowSuccessMessage(response.error_msg);
			  //reload empty page
			  //window.location.reload();
			  //go to backend
			  window.location.href = 'backend';
		  }
		  else
		  {
		      //show error message
			  ShowFailureMessage(response.error_msg)
		  }
		},
		error: function(data){
		console.log('failed to Load form:',data);
		//show error message
		ShowFailureMessage("Oops Server failed to complete this request");
		}
		});

}


function SubmitLogOutForm()
{
	const object_id = null
	const active_form_values = {};

	//console.log(active_form_values);
    $.ajax({
		type: "POST",
		cache: false,
		url: "api",
		dataType :"json",
		data: { rand_time: new Date().getTime(), api_object: "security_action", api_action: "submit_logout", object_id: object_id, form_values: JSON.stringify(active_form_values)} ,
		//url: "test_api.php",
		success: function(response){
		  //console.log(response);
		  //read data out
		  if(response.error === false)
		  {
		  	   //show success message
		  	   ShowSuccessMessage(response.error_msg);
			  //got to next page
			  window.location.reload();
		  }
		  else
		  {
		      //show error message
			  ShowFailureMessage(response.error_msg)
		  }
		},
		error: function(data){
		console.log('failed to Load form:',data);
		//show error message
		ShowFailureMessage("Oops Server failed to complete this request");
		}
		});

}

function send_browser_notification(title, options)
{

	// Let's check if the browser supports notifications
  if (!("Notification" in window)) {
    console.log("This browser does not support desktop notification");
  }

  // Let's check whether notification permissions have already been granted
  else if (Notification.permission === "granted") {
    // If it's okay let's create a notification
    var notification = new Notification(title, options);
      //notification.renotify = true;
	  notification.onclick = function() {
	   window.open(EW.notification_click_url);
	  };
  }

  // Otherwise, we need to ask the user for permission
  else if (Notification.permission !== "denied") {
    console.log("Please enable notifications");
  }


}

function request_notification_permission()
{
	// request permission on page load
	 if (!Notification) {
	  ShowFailureMessage('Desktop notifications not available in your browser. Try Chrome.');
	  return;
	 }

	 if (Notification.permission !== 'granted')
	  Notification.requestPermission().then(function (permission) {
      // If the user accepts, let's create a notification
      if (permission === "granted") {
        send_browser_notification('Hey There',{
		requireInteraction: false,
		vibrate: [200, 100, 200],
		icon: EW.notification_icon,
		body: 'You will receive notification from now on',
	  })
      }
    });

}

function start_tempUpload(control_id){
	  $('#form_for_'+control_id).submit(); // Submit the form
}

function capture_selection_radio_button(control_id){
      parent_control_id = $(control_id).attr("data-radiofor");
      var my_value = $(control_id).val();
	  $('#'+parent_control_id).val(my_value); // save value
}


function draw_fusion_chart(chart_type,renderAt,api_object,api_action,parameters = {})
{
	$.ajax({
			type: "GET",
			cache: false,
			url: "api?api_object="+api_object+"&api_action="+api_action+"&rand_time="+new Date().getTime(),
			dataType :"json",
			data: parameters ,
			success: function(response){
			  //console.log(response);
			  //read data out
			  if(response.error === false)
			  {
				  var categories = response.chart_data.categories === undefined?[]:response.chart_data.categories;
				  var dataset = response.chart_data.dataset === undefined?[]:response.chart_data.dataset;
				  var data = response.chart_data.data === undefined?[]:response.chart_data.data;
				  if(categories != null && dataset != null){
					FusionCharts.ready(function() {
					  var revenueChart = new FusionCharts({
						type: chart_type,
						renderAt: renderAt,
						width: "100%",
						height: "100%",
						dataFormat: "json",
						dataSource: {
						  "chart": {
							"caption": response.chart_data.chart_options.caption,
							"subcaption": response.chart_data.chart_options.subcaption,
							"xaxisname": response.chart_data.chart_options.xaxisname,
							"yaxisname": response.chart_data.chart_options.yaxisname,
							"formatnumberscale": response.chart_data.chart_options.formatnumberscale,
							"plottooltext": response.chart_data.chart_options.plottooltext,
							"drawcrossline": response.chart_data.chart_options.drawcrossline,
							"exportEnabled": response.chart_data.chart_options.exportEnabled
						  },
						  "categories": categories,
						  "dataset": dataset,
						  "data": data
						}
					  }).render();

					});


				  }
			  }
			  else
			  {
				   //show error message
				  ShowFailureMessage(response.error_msg)
				  console.log('failed to pick '+api_action+' error:'+response.error_msg);
			  }
			},
			error: function(data){
				console.log('failed to Load form:',data);
				//show error message
				ShowFailureMessage("Oops Server failed to complete this request, Check your Internet Connection");
			}


	});

}


function get_draw_html(renderAt,api_object,api_action,parameters = {})
{
	$.ajax({
			type: "GET",
			cache: false,
			url: "api?api_object="+api_object+"&api_action="+api_action+"&rand_time="+new Date().getTime(),
			dataType :"json",
			data: parameters ,
			success: function(response){
			  //console.log(response);
			  //read data out
			  if(response.error === false)
			  {
				  var html = response.html;
				  $("#"+renderAt).html(html);
			  }
			  else
			  {
				   //show error message
				  ShowFailureMessage(response.error_msg)
				  console.log('failed to pick '+api_action+' error:'+response.error_msg);
			  }
			},
			error: function(data){
				console.log('failed to Load form:',data);
				//show error message
				ShowFailureMessage("Oops Server failed to complete this request, Check your Internet Connection");
			}


	});

}