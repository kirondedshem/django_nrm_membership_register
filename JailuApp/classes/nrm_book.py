from JailuApp.classes.base_structures import *
from JailuApp.models import NrmBook


class TableNrmBook(TableObjectBase):
    # table specific settings
    object_name = 'nrm_book'

    # field settings
    fields = {
        "id": TableFieldBase(object_name, 'id', InputTypes.TEXT.code, ValidationTypes.NONE.code
             , {Actions.Add.code: False, Actions.Edit.code: True, Actions.List.code: False}
             , {Actions.SubmitAdd.code: False, Actions.Edit.code: True
             , Actions.SubmitEdit.code: True, Actions.Delete.code: True}
             , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '', 'value2': ''})
        , "book_name": TableFieldBase(object_name, 'book_name', InputTypes.TEXT.code, ValidationTypes.NONE.code
           , {Actions.Add.code: True, Actions.Edit.code: True, Actions.List.code: True}
           , {Actions.Add.code: True, Actions.SubmitAdd.code: True, Actions.Edit.code: True,
              Actions.SubmitEdit.code: True, Actions.Delete.code: False}
           , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '', 'value2': ''})
        , "front_page": TableFieldBase(object_name, 'front_page', InputTypes.FILE.code, ValidationTypes.NONE.code
              , {Actions.Add.code: True, Actions.Edit.code: True, Actions.List.code: True}
              , {Actions.Add.code: True, Actions.SubmitAdd.code: True, Actions.Edit.code: True,
                 Actions.SubmitEdit.code: True, Actions.Delete.code: False}
              , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '', 'value2': ''})

        , "entry_date": TableFieldBase(object_name, 'entry_date', InputTypes.TEXT.code, ValidationTypes.NONE.code
           , {Actions.Add.code: False, Actions.Edit.code: False, Actions.List.code: False}
           , {Actions.Add.code: False, Actions.SubmitAdd.code: False, Actions.Edit.code: False
           , Actions.SubmitEdit.code: False, Actions.Delete.code: False}
           , {'show': False, 'filter_type': FilterTypes.BETWEEN.code, 'value': '','value2': ''})
        , "entered_by": TableFieldBase(object_name, 'entered_by', InputTypes.TEXT.code, ValidationTypes.NONE.code
             , {Actions.Add.code: False, Actions.Edit.code: False, Actions.List.code: False}
             , {Actions.Add.code: False, Actions.SubmitAdd.code: False, Actions.Edit.code: False,
                Actions.SubmitEdit.code: False, Actions.Delete.code: False}
             , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '', 'value2': ''})
        , "last_modified": TableFieldBase(object_name, 'last_modified', InputTypes.TEXT.code, ValidationTypes.NONE.code
             , {Actions.Add.code: False, Actions.Edit.code: False, Actions.List.code: False}
             , {Actions.Add.code: False, Actions.SubmitAdd.code: False, Actions.Edit.code: False,
                Actions.SubmitEdit.code: False, Actions.Delete.code: False}
             , {'show': False, 'filter_type': FilterTypes.BETWEEN.code, 'value': '', 'value2': ''})
        , "modified_by": TableFieldBase(object_name, 'modified_by', InputTypes.TEXT.code, ValidationTypes.NONE.code
             , {Actions.Add.code: False, Actions.Edit.code: False, Actions.List.code: False}
             , {Actions.Add.code: False, Actions.SubmitAdd.code: False, Actions.Edit.code: False,
                Actions.SubmitEdit.code: False, Actions.Delete.code: False}
             , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '', 'value2': ''})

    }

    def select_all_records(self):
        # put custom implementation for model
        sql = """select det.* 
        , concat_ws(' ',eb.first_name, eb.sur_name) as entered_by_name 
        , concat_ws(' ',mb.first_name, mb.sur_name) as modified_by_name
        from nrm_book det 
        left join user_account eb on eb.id = det.entered_by_id
        left join user_account mb on mb.id = det.modified_by_id
         where 1=1 """;

        # apply filters if available
        if is_empty(self.fields["book_name"].ex_search["value"]) is False:
            sql += " and INSTR(lower(det.book_name), lower('" + str(
                self.fields["book_name"].ex_search["value"]).lower() + "') ) > 0   "

        # apply sort
        sql += " order by det.entry_date DESC"

        # apply limit and offset
        return dict(list=self.apply_sql_limit(sql), count=self.count_sql_result(sql))

    # override for lookup information
    def get_list_data(self):
        data = self.select_all_records()
        data_list = list()
        for item in data["list"]:
            an_item = dict()
            an_item["id"] = TableFieldListItem("id", item.get("id"), item.get("id"))
            an_item["book_name"] = TableFieldListItem("book_name", item.get("book_name"), item.get("book_name"))

            import os
            file_url = os.path.join('media', 'nrm_book_front_page',
                                    str(item.get("front_page", "")))

            file_view = """
                                    <a target='_blank' href='""" + file_url + """' class="avatar-icon-wrapper avatar-icon-xl">
                                      <div class="avatar-icon rounded"><img style='image-orientation:from-image;width: 100%;' src='""" + file_url + """' alt=""></div>
                                    </a>
                                    """

            an_item["front_page"] = TableFieldListItem("front_page", item.get("front_page"), file_view)

            if self.current_action == Actions.ExcelExport.code:
                an_item["entry_date"] = TableFieldListItem("entry_date", item.get("entry_date"), item.get("entry_date"))
                an_item["entered_by"] = TableFieldListItem("entered_by", item.get("entered_by_id"), item.get("entered_by_name"))
                an_item["last_modified"] = TableFieldListItem("last_modified", item.get("last_modified"), item.get("last_modified"))
                an_item["modified_by"] = TableFieldListItem("modified_by", item.get("modified_by_id"), item.get("modified_by_name"))

            data_list.append(an_item)
        return {"list": data_list, "count": data["count"]}

    def select_a_record(self, object_id):
        # put custom implementation for model
        return NrmBook.objects.filter(id=object_id)  # get record to be edited

    def insert_row(self):
        api_response = {"error": False, "error_msg": "Insert completed successfully"}

        if is_empty(self.fields["front_page"].current_value) is False:
            from django_jailuapp.settings import MEDIA_ROOT
            import os
            source_file = os.path.join(MEDIA_ROOT, 'temp', self.fields["front_page"].current_value)
            destination_file = os.path.join(MEDIA_ROOT, 'nrm_book_front_page',
                                            self.fields["front_page"].current_value)
            #  save the temp file to destination
            ret = copy_file(source_file, destination_file)

            if ret["error"] is True:
                api_response["error"] = True
                api_response["error_msg"] = "Failed to save front_page file error:" + ret["error_msg"]
                return api_response
            else:
                # remove temp file
                delete_file(source_file)
                # get actual saved file_name
                self.fields["front_page"].current_value = ret["uploaded_file_name"]

        # populate model and perform db operation
        NrmBook(id=new_guid()
                    , book_name=self.fields["book_name"].current_value
                    , front_page=self.fields["front_page"].current_value
                    , entry_date=current_datetime()
                    , entered_by_id=current_user_id()
                    , last_modified=current_datetime()
                    , modified_by_id=current_user_id()
                    ).save()
        return api_response

    def update_row(self):
        api_response = {"error": False, "error_msg": "Insert completed successfully"}
        # populate model and perform db operation
        obj = NrmBook.objects.get(id=self.fields["id"].current_value)

        #  delete old file
        if is_empty(obj.front_page) is False:
            from django_jailuapp.settings import MEDIA_ROOT
            import os
            delete_file(os.path.join(MEDIA_ROOT, 'nrm_book_front_page', obj.front_page))

        if is_empty(self.fields["front_page"].current_value) is False:
            from django_jailuapp.settings import MEDIA_ROOT
            import os
            source_file = os.path.join(MEDIA_ROOT, 'temp', self.fields["front_page"].current_value)
            destination_file = os.path.join(MEDIA_ROOT, 'nrm_book_front_page',
                                            self.fields["front_page"].current_value)
            #  save the temp file to destination
            ret = copy_file(source_file, destination_file)

            if ret["error"] is True:
                api_response["error"] = True
                api_response["error_msg"] = "Failed to save front_page file error:" + ret["error_msg"]
                return api_response
            else:
                # remove temp file
                delete_file(source_file)
                # get actual saved file_name
                self.fields["front_page"].current_value = ret["uploaded_file_name"]
                # populate model and perform db operation

        # specify updates
        obj.book_name = self.fields["book_name"].current_value
        obj.front_page = self.fields["front_page"].current_value
        obj.last_modified = current_datetime()
        obj.modified_by_id = current_user_id()
        obj.save()
        return api_response

    def delete_row(self):
        api_response = {"error": False, "error_msg": "Delete completed successfully"}
        from django.db import IntegrityError
        try:
            # populate model and perform db operation
            obj = NrmBook.objects.get(id=self.fields["id"].current_value)
            # specify updates
            obj.delete()
        except IntegrityError as e:
            api_response = {"error": True,
                            "error_msg": "Can not delete " + self.caption + " because its used elsewhere"}

        return api_response

    def html_edit_form_page_load(self):

        # copy actual file into temp folder
        if is_empty(self.fields["front_page"].current_value) is False:
            from django_jailuapp.settings import MEDIA_ROOT
            import os
            temp_file = os.path.join(MEDIA_ROOT, 'temp', self.fields["front_page"].current_value)
            actual_file = os.path.join(MEDIA_ROOT, 'nrm_book_front_page',
                                       self.fields["front_page"].current_value)
            # remove existing temp file
            delete_file(temp_file)
            #  save the temp file to destination
            ret = copy_file(actual_file, temp_file)
            if ret["error"] is False:
                # get actual temp file_name
                self.fields["front_page"].current_value = ret["uploaded_file_name"]
            # populate model and perform db operation



