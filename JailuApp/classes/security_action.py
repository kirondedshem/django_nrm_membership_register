from JailuApp.classes.base_structures import *
from JailuApp.models import UserAccount


class TableSecurityActions(TableObjectBase):
    # table specific settings
    object_name = 'security_action'

    # field settings
    fields = {
        "user_name": TableFieldBase(object_name, 'user_name', InputTypes.TEXT.code, ValidationTypes.INT.code
                             , {Actions.Add.code: True, Actions.Edit.code: True, Actions.List.code: False}
                             , {Actions.SubmitAdd.code: False, Actions.Edit.code: True,
                                Actions.SubmitEdit.code: True, Actions.Delete.code: True}
                             , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '', 'value2': ''})
        , "password": TableFieldBase(object_name, 'password', InputTypes.TEXT.code, ValidationTypes.NONE.code
                                 , {Actions.Add.code: True, Actions.Edit.code: True, Actions.List.code: True}
                                 , {Actions.Add.code: True, Actions.SubmitAdd.code: True, Actions.Edit.code: True,
                                    Actions.SubmitEdit.code: True, Actions.Delete.code: False}
                             , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '', 'value2': ''})

    }

    def check_login(self):
        api_response = {"error": False, "error_msg": "Validation Failed"}

        if is_empty(self.fields["user_name"].current_value) is True:
            api_response["error"] = True
            api_response["error_msg"] = "Please Enter Required Field " + self.fields["user_name"].caption
        elif is_empty(self.fields["password"].current_value) is True:
            api_response["error"] = True
            api_response["error_msg"] = "Please Enter Required Field " + self.fields["password"].caption
        else:
            user = UserAccount.objects.filter(user_name=self.fields["user_name"].current_value
                                              , password= enrypt_passwordMd5(self.fields["password"].current_value)).first()
            if user is None:
                api_response["error"] = True
                api_response["error_msg"] = "Invalid Username or Password "
            else:
                Security.save_user_session(user.id)
                api_response["error"] = False
                api_response["error_msg"] = "Logged In successfully"
        return api_response

    def user_logged_in(self):
        api_response = {"error": False, "error_msg": "Logged In successfully"}
        # other after effects
        # perform logging
        return api_response

    def submit_login(self):
        api_response = self.check_login()
        if api_response["error"] is True:  # check insert worked
            return api_response
        api_response = self.user_logged_in()

        return api_response

    def log_out(self):
        api_response = {"error": False, "error_msg": "Logged Out successfully"}
        Security.clear_user_session()
        # other after effects
        # perform logging
        return api_response

    def user_logged_out(self):
        api_response = {"error": False, "error_msg": "Logged Out successfully"}
        # other after effects
        # perform logging
        return api_response

    def submit_logout(self):
        api_response = self.log_out()
        if api_response["error"] is True:  # check insert worked
            return api_response
        api_response = self.user_logged_out()

        return api_response

    def perform_action(self, data=None):
        # in case data was passed from a form
        if data is None:
            data = dict()
        self.extract_data(data)

        # perform desired action
        api_response = {"error": True, "error_msg": "Unsupported Operation"}
        if data["api_action"] == "submit_login":
            api_response = self.submit_login()
        elif data["api_action"] == "submit_logout":
            api_response = self.submit_logout()

        return api_response


