from JailuApp.classes.base_structures import *
from JailuApp.models import NrmMemeber


class TableNrmMember(TableObjectBase):
    # table specific settings
    object_name = 'nrm_member'

    # field settings
    fields = {
        "id": TableFieldBase(object_name, 'id', InputTypes.TEXT.code, ValidationTypes.NONE.code
             , {Actions.Add.code: False, Actions.Edit.code: True, Actions.List.code: False}
             , {Actions.SubmitAdd.code: False, Actions.Edit.code: True
             , Actions.SubmitEdit.code: True, Actions.Delete.code: True}
             , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '', 'value2': ''})
        , "entry_date": TableFieldBase(object_name, 'entry_date', InputTypes.DATETIME.code, ValidationTypes.NONE.code
                                       , {Actions.Add.code: False, Actions.Edit.code: False, Actions.List.code: True}
                                       ,
                                       {Actions.Add.code: False, Actions.SubmitAdd.code: False, Actions.Edit.code: False
                                           , Actions.SubmitEdit.code: False, Actions.Delete.code: False}
                                       , {'show': True, 'filter_type': FilterTypes.BETWEEN.code, 'value': '',
                                          'value2': ''})
        , "entered_by": TableFieldBase(object_name, 'entered_by', InputTypes.DROPDOWN.code, ValidationTypes.NONE.code
                                       , {Actions.Add.code: False, Actions.Edit.code: False, Actions.List.code: True}
                                       , {Actions.Add.code: False, Actions.SubmitAdd.code: False,
                                          Actions.Edit.code: False,
                                          Actions.SubmitEdit.code: False, Actions.Delete.code: False}
                                       ,
                                       {'show': True, 'filter_type': FilterTypes.EQUAL.code, 'value': '', 'value2': ''})
        , "nrm_book": TableFieldBase(object_name, 'nrm_book', InputTypes.DROPDOWN.code, ValidationTypes.NONE.code
                                     , {Actions.Add.code: False, Actions.Edit.code: False, Actions.List.code: True}
                                     ,
                                     {Actions.Add.code: False, Actions.SubmitAdd.code: False, Actions.Edit.code: False,
                                      Actions.SubmitEdit.code: False, Actions.Delete.code: False}
                                     , {'show': True, 'filter_type': FilterTypes.EQUAL.code, 'value': '', 'value2': ''})
        , "page_number": TableFieldBase(object_name, 'page_number', InputTypes.TEXT.code, ValidationTypes.INT.code
                                        , {Actions.Add.code: False, Actions.Edit.code: False, Actions.List.code: True}
                                        , {Actions.Add.code: False, Actions.SubmitAdd.code: False,
                                           Actions.Edit.code: False,
                                           Actions.SubmitEdit.code: False, Actions.Delete.code: False}
                                        , {'show': True, 'filter_type': FilterTypes.EQUAL.code, 'value': '',
                                           'value2': ''})
        , "nrm_book_page": TableFieldBase(object_name, 'nrm_book_page', InputTypes.DROPDOWN.code,
                                          ValidationTypes.NONE.code
                                          , {Actions.Add.code: True, Actions.Edit.code: True, Actions.List.code: False}
                                          , {Actions.Add.code: True, Actions.SubmitAdd.code: True,
                                             Actions.Edit.code: True,
                                             Actions.SubmitEdit.code: True, Actions.Delete.code: False}
                                          , {'show': False, 'filter_type': FilterTypes.EQUAL.code, 'value': '',
                                             'value2': ''})
        , "member_number": TableFieldBase(object_name, 'member_number', InputTypes.TEXT.code, ValidationTypes.NONE.code
           , {Actions.Add.code: True, Actions.Edit.code: True, Actions.List.code: True}
           , {Actions.Add.code: False, Actions.SubmitAdd.code: False, Actions.Edit.code: False,
              Actions.SubmitEdit.code: False, Actions.Delete.code: False}
           , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '', 'value2': ''})
        , "sur_name": TableFieldBase(object_name, 'sur_name', InputTypes.TEXT.code, ValidationTypes.NONE.code
                                          , {Actions.Add.code: True, Actions.Edit.code: True, Actions.List.code: True}
                                          , {Actions.Add.code: True, Actions.SubmitAdd.code: True,
                                             Actions.Edit.code: True,
                                             Actions.SubmitEdit.code: True, Actions.Delete.code: False}
                                          , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '',
                                             'value2': ''})
        , "other_name": TableFieldBase(object_name, 'other_name', InputTypes.TEXT.code, ValidationTypes.NONE.code
                                          , {Actions.Add.code: True, Actions.Edit.code: True, Actions.List.code: True}
                                          , {Actions.Add.code: True, Actions.SubmitAdd.code: True,
                                             Actions.Edit.code: True,
                                             Actions.SubmitEdit.code: True, Actions.Delete.code: False}
                                          , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '',
                                             'value2': ''})
        , "gender": TableFieldBase(object_name, 'gender', InputTypes.RADIOBUTTON.code, ValidationTypes.NONE.code
                                          , {Actions.Add.code: True, Actions.Edit.code: True, Actions.List.code: True}
                                          , {Actions.Add.code: True, Actions.SubmitAdd.code: True,
                                             Actions.Edit.code: True,
                                             Actions.SubmitEdit.code: True, Actions.Delete.code: False}
                                          , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '',
                                             'value2': ''})
        , "date_of_birth": TableFieldBase(object_name, 'date_of_birth', InputTypes.DATE.code, ValidationTypes.NONE.code
                                          , {Actions.Add.code: True, Actions.Edit.code: True, Actions.List.code: True}
                                          , {Actions.Add.code: False, Actions.SubmitAdd.code: False,
                                             Actions.Edit.code: False,
                                             Actions.SubmitEdit.code: False, Actions.Delete.code: False}
                                          , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '',
                                             'value2': ''})
        , "national_id_no": TableFieldBase(object_name, 'national_id_no', InputTypes.TEXT.code, ValidationTypes.NONE.code
                                          , {Actions.Add.code: True, Actions.Edit.code: True, Actions.List.code: True}
                                          , {Actions.Add.code: False, Actions.SubmitAdd.code: False,
                                             Actions.Edit.code: False,
                                             Actions.SubmitEdit.code: False, Actions.Delete.code: False}
                                          , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '',
                                             'value2': ''})
        , "telephone_number": TableFieldBase(object_name, 'telephone_number', InputTypes.TEXT.code, ValidationTypes.NONE.code
                                          , {Actions.Add.code: True, Actions.Edit.code: True, Actions.List.code: True}
                                          , {Actions.Add.code: False, Actions.SubmitAdd.code: False,
                                             Actions.Edit.code: False,
                                             Actions.SubmitEdit.code: False, Actions.Delete.code: False}
                                          , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '',
                                             'value2': ''})
        , "youth_mark": TableFieldBase(object_name, 'youth_mark', InputTypes.TEXT.code, ValidationTypes.NONE.code
                                          , {Actions.Add.code: True, Actions.Edit.code: True, Actions.List.code: True}
                                          , {Actions.Add.code: False, Actions.SubmitAdd.code: False,
                                             Actions.Edit.code: False,
                                             Actions.SubmitEdit.code: False, Actions.Delete.code: False}
                                          , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '',
                                             'value2': ''})
        , "elderly_mark": TableFieldBase(object_name, 'elderly_mark', InputTypes.TEXT.code, ValidationTypes.NONE.code
                                       , {Actions.Add.code: True, Actions.Edit.code: True, Actions.List.code: True}
                                       , {Actions.Add.code: False, Actions.SubmitAdd.code: False,
                                          Actions.Edit.code: False,
                                          Actions.SubmitEdit.code: False, Actions.Delete.code: False}
                                       , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '',
                                          'value2': ''})
        , "pwd_mark": TableFieldBase(object_name, 'pwd_mark', InputTypes.TEXT.code, ValidationTypes.NONE.code
                                       , {Actions.Add.code: True, Actions.Edit.code: True, Actions.List.code: True}
                                       , {Actions.Add.code: False, Actions.SubmitAdd.code: False,
                                          Actions.Edit.code: False,
                                          Actions.SubmitEdit.code: False, Actions.Delete.code: False}
                                       , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '',
                                          'value2': ''})
        , "remarks_code": TableFieldBase(object_name, 'remarks_code', InputTypes.TEXT.code, ValidationTypes.NONE.code
                                       , {Actions.Add.code: True, Actions.Edit.code: True, Actions.List.code: True}
                                       , {Actions.Add.code: False, Actions.SubmitAdd.code: False,
                                          Actions.Edit.code: False,
                                          Actions.SubmitEdit.code: False, Actions.Delete.code: False}
                                       , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '',
                                          'value2': ''})
        , "last_modified": TableFieldBase(object_name, 'last_modified', InputTypes.TEXT.code, ValidationTypes.NONE.code
             , {Actions.Add.code: False, Actions.Edit.code: False, Actions.List.code: False}
             , {Actions.Add.code: False, Actions.SubmitAdd.code: False, Actions.Edit.code: False,
                Actions.SubmitEdit.code: False, Actions.Delete.code: False}
             , {'show': False, 'filter_type': FilterTypes.BETWEEN.code, 'value': '', 'value2': ''})
        , "modified_by": TableFieldBase(object_name, 'modified_by', InputTypes.TEXT.code, ValidationTypes.NONE.code
             , {Actions.Add.code: False, Actions.Edit.code: False, Actions.List.code: False}
             , {Actions.Add.code: False, Actions.SubmitAdd.code: False, Actions.Edit.code: False,
                Actions.SubmitEdit.code: False, Actions.Delete.code: False}
             , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '', 'value2': ''})

    }

    def select_all_records(self):
        # put custom implementation for model
        sql = """select det.* 
        , concat_ws('-',nb.book_name,nbp.page_number) as nrm_book_page_name
        , nb.book_name, nbp.nrm_book_id, nbp.page_number
        , concat_ws(' ',eb.first_name, eb.sur_name) as entered_by_name 
        , concat_ws(' ',mb.first_name, mb.sur_name) as modified_by_name
        from nrm_member det 
        join nrm_book_page nbp on nbp.id = det.nrm_book_page_id
        join nrm_book nb on nb.id = nbp.nrm_book_id
        left join user_account eb on eb.id = det.entered_by_id
        left join user_account mb on mb.id = det.modified_by_id
         where 1=1 """;

        # apply filters if available
        if is_empty(self.fields["entry_date"].ex_search["value"]) is False and is_empty(self.fields["entry_date"].ex_search["value2"]) is False:
            sql += """ and ( det.entry_date >= '""" + str(self.fields["entry_date"].ex_search["value"]) + """' 
             and det.entry_date <= '""" +str(self.fields["entry_date"].ex_search["value2"])+ """') """
        if is_empty(self.fields["entered_by"].ex_search["value"]) is False:
            sql += " and det.entered_by_id = '" + str(self.fields["entered_by"].ex_search["value"]) + "' "
        if is_empty(self.fields["nrm_book"].ex_search["value"]) is False:
            sql += " and nbp.nrm_book_id = '" + str(self.fields["nrm_book"].ex_search["value"]) + "' "
        if is_empty(self.fields["page_number"].ex_search["value"]) is False:
            sql += " and nbp.page_number = '" + str(self.fields["page_number"].ex_search["value"]) + "' "
        if is_empty(self.fields["nrm_book_page"].ex_search["value"]) is False:
            sql += " and det.nrm_book_page_id = '" + str(self.fields["nrm_book_page"].ex_search["value"]) + "' "

        # apply default filter for agents to see only thier allocations
        if str(Security.user_group_id) == "1":
            sql += " and det.entered_by_id = '" + str(current_user_id()) + "' "

        # apply sort
        sql += " order by det.entry_date DESC"

        # apply limit and offset
        return dict(list=self.apply_sql_limit(sql), count=self.count_sql_result(sql))

    # override for lookup information
    def get_list_data(self):
        data = self.select_all_records()
        data_list = list()
        for item in data["list"]:
            an_item = dict()
            an_item["id"] = TableFieldListItem("id", item.get("id"), item.get("id"))
            an_item["nrm_book_page"] = TableFieldListItem("nrm_book_page", item.get("nrm_book_page_id"),
                                                          item.get("nrm_book_page_name"))
            an_item["nrm_book"] = TableFieldListItem("nrm_book", item.get("nrm_book_id"), item.get("book_name"))
            an_item["page_number"] = TableFieldListItem("page_number", item.get("page_number"), item.get("page_number"))
            an_item["member_number"] = TableFieldListItem("member_number", item.get("member_number"), item.get("member_number"))
            an_item["sur_name"] = TableFieldListItem("sur_name", item.get("sur_name"), item.get("sur_name"))
            an_item["other_name"] = TableFieldListItem("other_name", item.get("other_name"), item.get("other_name"))
            an_item["gender"] = TableFieldListItem("gender", item.get("gender"), item.get("gender"))
            an_item["date_of_birth"] = TableFieldListItem("date_of_birth", item.get("date_of_birth"), item.get("date_of_birth"))
            an_item["national_id_no"] = TableFieldListItem("national_id_no", item.get("national_id_no"), item.get("national_id_no"))
            an_item["telephone_number"] = TableFieldListItem("telephone_number", item.get("telephone_number"), item.get("telephone_number"))
            an_item["youth_mark"] = TableFieldListItem("youth_mark", item.get("youth_mark"), item.get("youth_mark"))
            an_item["elderly_mark"] = TableFieldListItem("elderly_mark", item.get("elderly_mark"), item.get("elderly_mark"))
            an_item["pwd_mark"] = TableFieldListItem("pwd_mark", item.get("pwd_mark"), item.get("pwd_mark"))
            an_item["remarks_code"] = TableFieldListItem("remarks_code", item.get("remarks_code"), item.get("remarks_code"))

            an_item["entry_date"] = TableFieldListItem("entry_date", item.get("entry_date"), item.get("entry_date"))
            an_item["entered_by"] = TableFieldListItem("entered_by", item.get("entered_by_id"),
                                                       item.get("entered_by_name"))

            if self.current_action == Actions.ExcelExport.code:

                an_item["last_modified"] = TableFieldListItem("last_modified", item.get("last_modified"), item.get("last_modified"))
                an_item["modified_by"] = TableFieldListItem("modified_by", item.get("modified_by_id"), item.get("modified_by_name"))

            data_list.append(an_item)
        return {"list": data_list, "count": data["count"]}

    def select_a_record(self, object_id):
        # put custom implementation for model
        return NrmMemeber.objects.filter(id=object_id)  # get record to be edited

    def get_record_data(self, object_id):
        obj = self.select_a_record(object_id)
        if obj.count() == 1:
            dict_obj = obj[0].__dict__
            for a_field in self.fields:
                if dict_obj.get(self.fields[a_field].object_name, None) is not None:
                    self.fields[a_field].current_value = dict_obj.get(self.fields[a_field].object_name)
                    self.fields[a_field].view_value = self.fields[a_field].current_value
            #  cater for foreign keys
            self.fields["nrm_book_page"].current_value = dict_obj.get("nrm_book_page_id")
            self.fields["nrm_book_page"].view_value = self.fields["nrm_book_page"].current_value


    def submit_add_form(self):
        api_response = self.validate_form_data()
        if api_response["error"] is True:  # check primary validations
            return api_response

        #  do extra validations
        if my_custom_sql("""select ifnull(count(id),0) as value from page_allocation where nrm_book_page_id = '"""+
                         str(self.fields["nrm_book_page"].current_value) +"""' and agent_id = '"""+
                         str(current_user_id()) +"""' """)[0]["value"] == 0:
            api_response["error"] = True
            api_response["error_msg"] = " The Book's Page you are refering to is NOT allocated to you, Please contact your administrator"
        elif my_custom_sql("""select (number_of_records - ifnull( (select count(nm.id) from nrm_member nm where nm.nrm_book_page_id = '"""+
                         str(self.fields["nrm_book_page"].current_value) +"""') ,0) ) as value from nrm_book_page where id = '"""+
                         str(self.fields["nrm_book_page"].current_value) +"""' LIMIT 1""")[0]["value"] <= 0:
            api_response["error"] = True
            api_response["error_msg"] = " This Book's Page has reached its maximum required records, Please contact your administrator"
        else:
            api_response = self.insert_row()  # run insert query
            if api_response["error"] is True:  # check insert worked
                return api_response
            api_response = self.row_inserted()

        return api_response

    def insert_row(self):
        api_response = {"error": False, "error_msg": "Insert completed successfully"}

        # cleanup sensitive types
        if is_empty(self.fields["date_of_birth"].current_value):
            self.fields["date_of_birth"].current_value = None
        # populate model and perform db operation
        NrmMemeber(id=new_guid()
                    , nrm_book_page_id=self.fields["nrm_book_page"].current_value
                    , member_number=self.fields["member_number"].current_value
                    , sur_name=self.fields["sur_name"].current_value
                    , other_name=self.fields["other_name"].current_value
                    , gender=self.fields["gender"].current_value
                    , date_of_birth=self.fields["date_of_birth"].current_value
                    , national_id_no=self.fields["national_id_no"].current_value
                    , telephone_number=self.fields["telephone_number"].current_value
                    , youth_mark=self.fields["youth_mark"].current_value
                    , elderly_mark=self.fields["elderly_mark"].current_value
                    , pwd_mark=self.fields["pwd_mark"].current_value
                    , remarks_code=self.fields["remarks_code"].current_value
                    , entry_date=current_datetime()
                    , entered_by_id=current_user_id()
                    , last_modified=current_datetime()
                    , modified_by_id=current_user_id()
                    ).save()
        return api_response

    def submit_edit_form(self):
        api_response = self.validate_form_data()
        if api_response["error"] is True:  # check primary validations
            return api_response

        #  do extra validations
        if my_custom_sql("""select ifnull(count(id),0) as value from page_allocation where nrm_book_page_id = '""" +
                         str(self.fields["nrm_book_page"].current_value) + """' and agent_id = '""" +
                         str(current_user_id()) + """' """)[0]["value"] == 0:
            api_response["error"] = True
            api_response[
                "error_msg"] = " The Book's Page you are refering to is NOT allocated to you, Please contact your administrator"
        else:
            api_response = self.update_row()  # run insert query
            if api_response["error"] is True:  # check insert worked
                return api_response
            api_response = self.row_updated()

        return api_response

    def update_row(self):
        api_response = {"error": False, "error_msg": "Insert completed successfully"}

        # cleanup sensitive types
        if is_empty(self.fields["date_of_birth"].current_value):
            self.fields["date_of_birth"].current_value = None

        # populate model and perform db operation
        obj = NrmMemeber.objects.get(id=self.fields["id"].current_value)
        # specify updates
        obj.nrm_book_page_id = self.fields["nrm_book_page"].current_value
        obj.member_number = self.fields["member_number"].current_value
        obj.sur_name = self.fields["sur_name"].current_value
        obj.other_name = self.fields["other_name"].current_value
        obj.gender = self.fields["gender"].current_value
        obj.date_of_birth = self.fields["date_of_birth"].current_value
        obj.national_id_no = self.fields["national_id_no"].current_value
        obj.telephone_number = self.fields["telephone_number"].current_value
        obj.youth_mark = self.fields["youth_mark"].current_value
        obj.elderly_mark = self.fields["elderly_mark"].current_value
        obj.pwd_mark = self.fields["pwd_mark"].current_value
        obj.remarks_code = self.fields["remarks_code"].current_value
        obj.last_modified = current_datetime()
        obj.modified_by_id = current_user_id()
        obj.save()
        return api_response

    def delete_row(self):
        api_response = {"error": False, "error_msg": "Delete completed successfully"}
        from django.db import IntegrityError
        try:
            # populate model and perform db operation
            obj = NrmMemeber.objects.get(id=self.fields["id"].current_value)
            # specify updates
            obj.delete()
        except IntegrityError as e:
            api_response = {"error": True,
                            "error_msg": "Can not delete " + self.caption + " because its used elsewhere"}

        return api_response


    def html_add_form(self):
        # call page load
        self.html_add_form_page_load()

        api_response = {"header_title": self.caption + "s", "content_title": self.caption + " Add",
                        "content_body": "content_body", "content_footer": "content_footer",
                        "error": True, "error_msg": "Unknown API Error"}
        html_text = """
        <div class="card card-info">
        <div class="card-body">
        <div class='col-lg-12 row'>
        """
        # add all add form fields
        for a_field in self.fields:
            if self.fields[a_field].show_on[self.current_action] is True:
                html_text += """
                        <div class='col-xs-6 col-sm-6 col-lg-6 row form-group' """ + ("hidden='true'" if self.object_name == "id" else "") + """ 
                        control_for='x_""" + self.object_name + """'>
                            """ + self.fields[a_field].html_caption(self.current_action) + """
                            """ + self.fields[a_field].html_input(self.current_action) + """
                        </div>
                        """
        #  show page preview
        html_text += """
                <div class='col-lg-12' style='overflow-x:scroll; white-space: nowrap;height: 150px;overflow-y: scroll;'
                 id="nrm_book_page_preview">
                <img style='image-orientation:from-image;width: 100%;' src="" alt="page preview"
                style="width: 120%;">
                </div>
                """

        html_text += """
        </div>
        </div>
        <div class="card-footer">
          <button onclick="SubmitForm('""" + Actions.SubmitAdd.code + """','""" + self.object_name \
                     + """')" class="btn btn-info">""" + Lang.phrase("btn_add") + """</button>
          <button onclick="ShowForm('""" + Actions.List.code + """','""" + self.object_name \
                     + """')" class="btn btn-default float-right">""" + Lang.phrase("btn_cancel") + """</button>
        </div>
        </div>
        """

        # add js
        html_text += """
                <script>
                //for firefox
                //fetch drop down items
                $('#x_nrm_book_page option').click(function(){
                    //get selected major
                    var nrm_book_page_id = $("#x_nrm_book_page").val();
                    get_draw_html('nrm_book_page_preview','""" + self.object_name + """','get_nrm_book_page_image',parameters = {nrm_book_page_id:nrm_book_page_id});

                });

                //for chrome compatibility
                //fetch drop down items
                $('#x_nrm_book_page').change(function(){
                    //get selected major
                    var nrm_book_page_id = $("#x_nrm_book_page").val();
                    get_draw_html('nrm_book_page_preview','""" + self.object_name + """','get_nrm_book_page_image',parameters = {nrm_book_page_id:nrm_book_page_id});

                });

                </script>
                """

        api_response["error"] = False
        api_response["error_msg"] = "Completed Successfully"
        api_response["content_body"] = html_text
        return api_response

    def get_nrm_book_page_image(self, data):
        api_response = {"error": True, "error_msg": "Unknown API error", "html": ""}
        nrm_book_page_id = data.get("nrm_book_page_id", "")
        file_attachment = my_custom_sql("select ifnull(file_attachment,'') as value from nrm_book_page where id = '"+nrm_book_page_id+"' LIMIT 1")[0]["value"]
        import os
        file_url = os.path.join('media', 'nrm_book_page_file_attachment',str(file_attachment))
        html = """<img style='image-orientation:from-image;width: 100%;' src='""" + file_url + """' alt=""
                        style="width: 120%;">
                        """

        api_response["html"] = html
        api_response["error"] = False
        api_response["error_msg"] = "Success"

        return api_response

    def html_add_form_page_load(self):
        self.fields["gender"].lookup_data = Lookups.genders()
        self.fields["entered_by"].lookup_data = None
        self.fields["nrm_book"].lookup_data = None
        # show only my allocated book pages
        # ignore complete pages
        self.fields["nrm_book_page"].lookup_data = Lookups.extract_lookup('id', 'full_name', my_custom_sql(
            """select nbp.id, concat_ws('',nb.book_name,'-',nbp.page_number
            , "(", (nbp.number_of_records - ifnull( (select count(nm.id) from nrm_member nm where nm.nrm_book_page_id = nbp.id) ,0) ) 
            , " left)") as full_name from nrm_book_page nbp
            join nrm_book nb on nb.id = nbp.nrm_book_id 
            where nbp.id in (select pa.nrm_book_page_id from 
            page_allocation pa where pa.agent_id = '"""+ str(current_user_id()) +"""') and 
            (nbp.number_of_records > ifnull( (select count(nm.id) from nrm_member nm where nm.nrm_book_page_id = nbp.id) ,0) )
            order by nb.book_name asc, nbp.page_number asc"""))

    def html_edit_form_page_load(self):
        self.fields["gender"].lookup_data = Lookups.genders()
        self.fields["entered_by"].lookup_data = None
        self.fields["nrm_book"].lookup_data = None

        # show only my allocated book pages
        self.fields["nrm_book_page"].lookup_data = Lookups.extract_lookup('id', 'full_name', my_custom_sql(
            """select nbp.id, concat_ws('-',nb.book_name,nbp.page_number) as full_name from nrm_book_page nbp
            join nrm_book nb on nb.id = nbp.nrm_book_id 
            where nbp.id in (select pa.nrm_book_page_id from 
            page_allocation pa where pa.agent_id = '""" + str(current_user_id()) + """') 
                    order by nb.book_name asc, nbp.page_number asc"""))

    def html_list_form_page_load(self):
        self.fields["gender"].lookup_data = Lookups.genders()

        if str(Security.user_group_id) == "1":
            #  disable some seracch fileds
            self.fields["entered_by"].ex_search["show"] = False
            self.fields["nrm_book"].ex_search["show"] = False
            self.fields["page_number"].ex_search["show"] = False

            #  enable some seracch fileds
            self.fields["nrm_book_page"].ex_search["show"] = True

            self.fields["nrm_book_page"].lookup_data = Lookups.extract_lookup('id', 'full_name', my_custom_sql(
                """select nbp.id, concat_ws('-',nb.book_name,nbp.page_number) as full_name from nrm_book_page nbp
                join nrm_book nb on nb.id = nbp.nrm_book_id 
                where nbp.id in (select pa.nrm_book_page_id from 
                page_allocation pa where pa.agent_id = '""" + str(current_user_id()) + """') 
                                order by nb.book_name asc, nbp.page_number asc"""))
        else:
            #  disable some seracch fileds
            self.fields["nrm_book_page"].ex_search["show"] = False

            #  enable some seracch fileds
            self.fields["entered_by"].ex_search["show"] = True
            self.fields["nrm_book"].ex_search["show"] = True
            self.fields["page_number"].ex_search["show"] = True

            self.fields["nrm_book_page"].lookup_data =  Lookups.nrm_book_pages()
            self.fields["entered_by"].lookup_data = Lookups.non_developer_users()
            self.fields["nrm_book"].lookup_data = Lookups.nrm_books()


        #  hide some fields for client users
        if str(Security.user_group_id) == "2":
            self.fields["entered_by"].show_on[Actions.List.code] = False
            self.fields["nrm_book"].show_on[Actions.List.code] = False
            self.fields["nrm_book_page"].show_on[Actions.List.code] = False
            self.fields["page_number"].show_on[Actions.List.code] = False
        else:
            self.fields["entered_by"].show_on[Actions.List.code] = True
            self.fields["nrm_book"].show_on[Actions.List.code] = True
            self.fields["nrm_book_page"].show_on[Actions.List.code] = True
            self.fields["page_number"].show_on[Actions.List.code] = True



    def perform_action(self, data=None):

        # in case data was passed from a form
        if data is None:
            data = dict()
        self.extract_data(data)

        # perform desired action
        api_response = {"error": True, "error_msg": "Unsupported Operation"}
        if Security.check_permission(self.object_name, data["api_action"]) is False:
            api_response["error"] = True
            api_response["error_msg"] = Lang.phrase("permission_denied")
        elif data["api_action"] == Actions.List.code:
            api_response = self.html_list_form()
        elif data["api_action"] == Actions.Add.code:
            api_response = self.html_add_form()
        elif data["api_action"] == Actions.SubmitAdd.code:
            api_response = self.submit_add_form()
        elif data["api_action"] == Actions.Edit.code:
            api_response = self.html_edit_form()
        elif data["api_action"] == Actions.SubmitEdit.code:
            api_response = self.submit_edit_form()
        elif data["api_action"] == Actions.Delete.code:
            api_response = self.submit_delete_form()
        elif data["api_action"] == Actions.ExcelExport.code and Security.is_admin() is False:
            api_response["error"] = True
            api_response["error_msg"] = "Export is disabled unless you are an adminsitrator"
        elif data["api_action"] == Actions.ExcelExport.code:
            api_response = self.html_list_export()
        elif data["api_action"] == "get_nrm_book_page_image":
            api_response = self.get_nrm_book_page_image(data)

        return api_response


