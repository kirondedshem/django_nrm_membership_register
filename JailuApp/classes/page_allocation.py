from JailuApp.classes.base_structures import *
from JailuApp.models import PageAllocation


class TablePageAllocation(TableObjectBase):
    # table specific settings
    object_name = 'page_allocation'

    # field settings
    fields = {
        "id": TableFieldBase(object_name, 'id', InputTypes.TEXT.code, ValidationTypes.NONE.code
             , {Actions.Add.code: False, Actions.Edit.code: True, Actions.List.code: False}
             , {Actions.SubmitAdd.code: False, Actions.Edit.code: True
             , Actions.SubmitEdit.code: True, Actions.Delete.code: True}
             , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '', 'value2': ''})
        , "nrm_book_page": TableFieldBase(object_name, 'nrm_book_page', InputTypes.DROPDOWN.code, ValidationTypes.NONE.code
                                     , {Actions.Add.code: True, Actions.Edit.code: True, Actions.List.code: True}
                                     , {Actions.Add.code: True, Actions.SubmitAdd.code: True, Actions.Edit.code: True,
                                        Actions.SubmitEdit.code: True, Actions.Delete.code: False}
                                     , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '', 'value2': ''})
        , "agent": TableFieldBase(object_name, 'agent', InputTypes.DROPDOWN.code, ValidationTypes.NONE.code
                                     , {Actions.Add.code: True, Actions.Edit.code: True, Actions.List.code: True}
                                     , {Actions.Add.code: True, Actions.SubmitAdd.code: True, Actions.Edit.code: True,
                                        Actions.SubmitEdit.code: True, Actions.Delete.code: False}
                                     , {'show': True, 'filter_type': FilterTypes.EQUAL.code, 'value': '', 'value2': ''})
        , "nrm_book": TableFieldBase(object_name, 'nrm_book', InputTypes.DROPDOWN.code, ValidationTypes.NONE.code
           , {Actions.Add.code: False, Actions.Edit.code: False, Actions.List.code: True}
           , {Actions.Add.code: False, Actions.SubmitAdd.code: False, Actions.Edit.code: False,
              Actions.SubmitEdit.code: False, Actions.Delete.code: False}
           , {'show': True, 'filter_type': FilterTypes.EQUAL.code, 'value': '', 'value2': ''})
        , "page_number": TableFieldBase(object_name, 'page_number', InputTypes.TEXT.code, ValidationTypes.INT.code
                                        , {Actions.Add.code: False, Actions.Edit.code: False, Actions.List.code: True}
                                        , {Actions.Add.code: False, Actions.SubmitAdd.code: False,
                                           Actions.Edit.code: False,
                                           Actions.SubmitEdit.code: False, Actions.Delete.code: False}
                                        , {'show': True, 'filter_type': FilterTypes.EQUAL.code, 'value': '',
                                           'value2': ''})
        , "number_of_records": TableFieldBase(object_name, 'number_of_records', InputTypes.TEXT.code, ValidationTypes.INT.code
                                              , {Actions.Add.code: False, Actions.Edit.code: False,
                                                 Actions.List.code: True}
                                              , {Actions.Add.code: False, Actions.SubmitAdd.code: False,
                                                 Actions.Edit.code: False,
                                                 Actions.SubmitEdit.code: False, Actions.Delete.code: False}
                                              , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '',
                                                 'value2': ''})
        , "actual_records": TableFieldBase(object_name, 'actual_records', InputTypes.TEXT.code,
                                              ValidationTypes.INT.code
                                              , {Actions.Add.code: False, Actions.Edit.code: False,
                                                 Actions.List.code: True}
                                              , {Actions.Add.code: False, Actions.SubmitAdd.code: False,
                                                 Actions.Edit.code: False,
                                                 Actions.SubmitEdit.code: False, Actions.Delete.code: False}
                                              , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '',
                                                 'value2': ''})
        , "file_attachment": TableFieldBase(object_name, 'file_attachment', InputTypes.FILE.code, ValidationTypes.NONE.code
                                            ,
                                            {Actions.Add.code: False, Actions.Edit.code: False, Actions.List.code: True}
                                            , {Actions.Add.code: False, Actions.SubmitAdd.code: False,
                                               Actions.Edit.code: False,
                                               Actions.SubmitEdit.code: False, Actions.Delete.code: False}
                                            , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '',
                                               'value2': ''})
        , "entry_date": TableFieldBase(object_name, 'entry_date', InputTypes.TEXT.code, ValidationTypes.NONE.code
           , {Actions.Add.code: False, Actions.Edit.code: False, Actions.List.code: False}
           , {Actions.Add.code: False, Actions.SubmitAdd.code: False, Actions.Edit.code: False
           , Actions.SubmitEdit.code: False, Actions.Delete.code: False}
           , {'show': False, 'filter_type': FilterTypes.BETWEEN.code, 'value': '','value2': ''})
        , "entered_by": TableFieldBase(object_name, 'entered_by', InputTypes.TEXT.code, ValidationTypes.NONE.code
             , {Actions.Add.code: False, Actions.Edit.code: False, Actions.List.code: False}
             , {Actions.Add.code: False, Actions.SubmitAdd.code: False, Actions.Edit.code: False,
                Actions.SubmitEdit.code: False, Actions.Delete.code: False}
             , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '', 'value2': ''})
        , "last_modified": TableFieldBase(object_name, 'last_modified', InputTypes.TEXT.code, ValidationTypes.NONE.code
             , {Actions.Add.code: False, Actions.Edit.code: False, Actions.List.code: False}
             , {Actions.Add.code: False, Actions.SubmitAdd.code: False, Actions.Edit.code: False,
                Actions.SubmitEdit.code: False, Actions.Delete.code: False}
             , {'show': False, 'filter_type': FilterTypes.BETWEEN.code, 'value': '', 'value2': ''})
        , "modified_by": TableFieldBase(object_name, 'modified_by', InputTypes.TEXT.code, ValidationTypes.NONE.code
             , {Actions.Add.code: False, Actions.Edit.code: False, Actions.List.code: False}
             , {Actions.Add.code: False, Actions.SubmitAdd.code: False, Actions.Edit.code: False,
                Actions.SubmitEdit.code: False, Actions.Delete.code: False}
             , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '', 'value2': ''})

    }

    def select_all_records(self):
        # put custom implementation for model
        sql = """select det.*, concat_ws(' ',agnt.first_name, agnt.sur_name) as agent_name
        , concat_ws('-',nb.book_name,nbp.page_number) as nrm_book_page_name
        , nb.book_name, nbp.nrm_book_id
        , nbp.page_number, nbp.number_of_records, nbp.file_attachment
        ,ifnull( (select count(nm.id) from nrm_member nm where nm.nrm_book_page_id = nbp.id) , 0) as actual_records
        , concat_ws(' ',eb.first_name, eb.sur_name) as entered_by_name 
        , concat_ws(' ',mb.first_name, mb.sur_name) as modified_by_name
        from page_allocation det 
        join nrm_book_page nbp on nbp.id = det.nrm_book_page_id
        join nrm_book nb on nb.id = nbp.nrm_book_id
        left join user_account agnt on agnt.id = det.agent_id
        left join user_account eb on eb.id = det.entered_by_id
        left join user_account mb on mb.id = det.modified_by_id
         where 1=1 """;

        # apply filters if available
        if is_empty(self.fields["agent"].ex_search["value"]) is False:
            sql += " and det.agent_id = '" + str(self.fields["agent"].ex_search["value"]) + "' "
        if is_empty(self.fields["nrm_book"].ex_search["value"]) is False:
            sql += " and nbp.nrm_book_id = '" + str(self.fields["nrm_book"].ex_search["value"]) + "' "
        if is_empty(self.fields["page_number"].ex_search["value"]) is False:
            sql += " and nbp.page_number = '" + str(self.fields["page_number"].ex_search["value"]) + "' "

        #apply default filter for agents to see only thier allocations
        if str(Security.user_group_id) == "1":
            sql += " and det.agent_id = '" + str(current_user_id()) + "' "

        # apply sort
        sql += " order by det.entry_date DESC"

        # apply limit and offset
        return dict(list=self.apply_sql_limit(sql), count=self.count_sql_result(sql))

    # override for lookup information
    def get_list_data(self):
        data = self.select_all_records()
        data_list = list()
        for item in data["list"]:
            an_item = dict()
            an_item["id"] = TableFieldListItem("id", item.get("id"), item.get("id"))
            an_item["nrm_book_page"] = TableFieldListItem("nrm_book_page", item.get("nrm_book_page_id"), item.get("nrm_book_page_name"))
            an_item["agent"] = TableFieldListItem("agent", item.get("agent_id"), item.get("agent_name"))
            an_item["nrm_book"] = TableFieldListItem("nrm_book", item.get("nrm_book_id"), item.get("book_name"))
            an_item["page_number"] = TableFieldListItem("page_number", item.get("page_number"), item.get("page_number"))
            an_item["number_of_records"] = TableFieldListItem("number_of_records", item.get("number_of_records"), item.get("number_of_records"))
            an_item["actual_records"] = TableFieldListItem("actual_records", item.get("actual_records"),
                                                              item.get("actual_records"))

            import os
            file_url = os.path.join('media', 'nrm_book_page_file_attachment',
                                    str(item.get("file_attachment", "")))

            file_view = """
                                                <a target='_blank' href='""" + file_url + """' class="avatar-icon-wrapper avatar-icon-xl">
                                                  <div class="avatar-icon rounded"><img style='image-orientation:from-image;width: 100%;' src='""" + file_url + """' alt=""></div>
                                                </a>
                                                """


            an_item["file_attachment"] = TableFieldListItem("file_attachment", item.get("file_attachment"), file_view)

            if self.current_action == Actions.ExcelExport.code:
                an_item["entry_date"] = TableFieldListItem("entry_date", item.get("entry_date"), item.get("entry_date"))
                an_item["entered_by"] = TableFieldListItem("entered_by", item.get("entered_by_id"), item.get("entered_by_name"))
                an_item["last_modified"] = TableFieldListItem("last_modified", item.get("last_modified"), item.get("last_modified"))
                an_item["modified_by"] = TableFieldListItem("modified_by", item.get("modified_by_id"), item.get("modified_by_name"))

            data_list.append(an_item)
        return {"list": data_list, "count": data["count"]}

    def select_a_record(self, object_id):
        # put custom implementation for model
        return PageAllocation.objects.filter(id=object_id)  # get record to be edited

    def get_record_data(self, object_id):
        obj = self.select_a_record(object_id)
        if obj.count() == 1:
            dict_obj = obj[0].__dict__
            for a_field in self.fields:
                if dict_obj.get(self.fields[a_field].object_name, None) is not None:
                    self.fields[a_field].current_value = dict_obj.get(self.fields[a_field].object_name)
                    self.fields[a_field].view_value = self.fields[a_field].current_value
            #  cater for foreign keys
            self.fields["nrm_book_page"].current_value = dict_obj.get("nrm_book_page_id")
            self.fields["nrm_book_page"].view_value = self.fields["nrm_book_page"].current_value
            self.fields["agent"].current_value = dict_obj.get("agent_id")
            self.fields["agent"].view_value = self.fields["agent"].current_value

    def submit_add_form(self):
        api_response = self.validate_form_data()
        if api_response["error"] is True:  # check primary validations
            return api_response

        #  do extra validations
        if my_custom_sql("""select ifnull(count(id),0) as value from page_allocation where nrm_book_page_id = '"""+
                         str(self.fields["nrm_book_page"].current_value) +"""' """)[0]["value"] > 0:
            api_response["error"] = True
            api_response["error_msg"] = " This Book's Page is already allocated to someone else"
        else:
            api_response = self.insert_row()  # run insert query
            if api_response["error"] is True:  # check insert worked
                return api_response
            api_response = self.row_inserted()

        return api_response

    def insert_row(self):
        api_response = {"error": False, "error_msg": "Insert completed successfully"}

        # populate model and perform db operation
        PageAllocation(id=new_guid()
                    , nrm_book_page_id=self.fields["nrm_book_page"].current_value
                    , agent_id=self.fields["agent"].current_value
                    , entry_date=current_datetime()
                    , entered_by_id=current_user_id()
                    , last_modified=current_datetime()
                    , modified_by_id=current_user_id()
                    ).save()
        return api_response

    def submit_edit_form(self):
        api_response = self.validate_form_data()
        if api_response["error"] is True:  # check primary validations
            return api_response

        #  do extra validations
        if my_custom_sql("""select ifnull(count(id),0) as value from page_allocation where nrm_book_page_id = '"""+
                         str(self.fields["nrm_book_page"].current_value) +"""' and id != '""" +
                         str(self.fields["id"].current_value) + """' """)[0]["value"] > 0:
            api_response["error"] = True
            api_response["error_msg"] = " This Book's Page is already allocated to someone else"
        else:
            api_response = self.update_row()  # run insert query
            if api_response["error"] is True:  # check insert worked
                return api_response
            api_response = self.row_updated()

        return api_response

    def update_row(self):
        api_response = {"error": False, "error_msg": "Insert completed successfully"}

        # populate model and perform db operation
        obj = PageAllocation.objects.get(id=self.fields["id"].current_value)

        # specify updates
        obj.nrm_book_page_id = self.fields["nrm_book_page"].current_value
        obj.agent_id = self.fields["agent"].current_value
        obj.last_modified = current_datetime()
        obj.modified_by_id = current_user_id()
        obj.save()
        return api_response

    def delete_row(self):
        api_response = {"error": False, "error_msg": "Delete completed successfully"}
        from django.db import IntegrityError
        try:
            # populate model and perform db operation
            obj = PageAllocation.objects.get(id=self.fields["id"].current_value)
            # specify updates
            obj.delete()
        except IntegrityError as e:
            api_response = {"error": True,
                            "error_msg": "Can not delete " + self.caption + " because its used elsewhere"}

        return api_response

    def html_add_form_page_load(self):
        #  show available pages only
        self.fields["nrm_book_page"].lookup_data = Lookups.extract_lookup('id', 'full_name', my_custom_sql(
            """select nbp.id, concat_ws('-',nb.book_name,nbp.page_number) as full_name from nrm_book_page nbp
            join nrm_book nb on nb.id = nbp.nrm_book_id 
            where nbp.id not in (select nrm_book_page_id from page_allocation) 
            order by nb.book_name asc, nbp.page_number asc"""))

        self.fields["agent"].lookup_data = Lookups.non_developer_users()

    def html_edit_form_page_load(self):

        self.fields["nrm_book_page"].lookup_data = Lookups.nrm_book_pages()
        self.fields["agent"].lookup_data = Lookups.non_developer_users()


    def html_list_form_page_load(self):
        self.fields["nrm_book_page"].lookup_data = None
        self.fields["agent"].lookup_data = Lookups.non_developer_users()

        self.fields["nrm_book"].lookup_data = Lookups.nrm_books()


