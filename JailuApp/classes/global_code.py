class Language:
    def __init__(self, m_lang_id="en"):
        self.lang_id = m_lang_id
        self.global_phrases = dict()
        self.table_phrases = dict()
        self.menu_phrases = dict()
        self.load_language(m_lang_id)

    def phrase(self, phrase_id):
        return str(self.global_phrases.get(phrase_id, phrase_id))

    def men_phrase(self, phrase_id):
        return str(self.menu_phrases.get(phrase_id, phrase_id))

    def tbl_phrase(self, tbl_id):
        return str(self.table_phrases.get(tbl_id, dict()).get("caption", tbl_id))

    def fld_phrase(self, tbl_id, fld_id):
        return str(self.table_phrases.get(tbl_id, dict()).get("fields", dict()).get(fld_id, fld_id))

    def load_language(self, m_lang_id):
        import xml.etree.ElementTree as ET
        from django_jailuapp.settings import BASE_DIR
        filename = BASE_DIR + '/JailuApp/lang/english.xml'
        tree = ET.parse(filename)
        root = tree.getroot()
        # read global phrases
        for node in root[0]:
            self.global_phrases[node.attrib["id"]] = node.attrib["caption"]
        # read table phrases
        for node in root[1]:
            fields = dict()  # capture all fields for said table
            for a_field in node:
                fields[a_field.attrib["id"]] = a_field.attrib["caption"]
            self.table_phrases[node.attrib["id"]] = dict(caption=node.attrib["caption"], fields=fields)
        # read menu phrases
        for node in root[2]:
            self.menu_phrases[node.attrib["id"]] = node.attrib["caption"]


Lang = Language()


class DefaultValues:
    from django_jailuapp.settings import TIME_ZONE
    records_per_page = 10
    session_logged_in_user_id = "session_logged_in_user_id"
    notification_icon = 'https://nftconsult.com/wp-content/uploads/2018/02/f65ccf_8b10605fb8ee412ca4f3e87d26dc1718_mv2.png'
    notification_click_url = "https://nftconsult.com"
    default_timezone = TIME_ZONE
    uptime_http_type = "Http_Request"
    uptime_ip_type = "IP_Request"
    uptime_imap_type = "Imap_Request"
    uptime_network_internet = "Internet"
    uptime_network_lan = "Local_Area"
    uptime_record_internet = "1"
    uptime_record_lan = "2"
    js_notification_timer = 60000


EW = DefaultValues()


class AnItem:
    def __init__(self, m_code, m_caption):
        self.code = m_code
        self.caption = m_caption

    def code(self):
        return str(self.code)

    def name(self):
        return str(self.caption)


class CSystemActions:
    List = AnItem('list', 'List')
    Add = AnItem('add', 'Add')
    Edit = AnItem('edit', 'Edit')
    View = AnItem('view', 'View')
    SubmitAdd = AnItem('submit_add', 'Submit Add')
    SubmitEdit = AnItem('submit_edit', 'Submit Edit')
    Delete = AnItem('submit_delete', 'Submit delete')
    ExcelExport = AnItem('excel', 'Excel Export')


Actions = CSystemActions()


class CInputTypes:
    TEXT = AnItem('text', 'text')
    DROPDOWN = AnItem('drop_down', 'drop_down')
    DATETIME = AnItem('date_time', 'date_time')
    DATE = AnItem('date', 'date')
    FILE = AnItem('file', 'file')
    TEXTAREA = AnItem('textarea', 'textarea')
    RADIOBUTTON = AnItem('radiobutton', 'radiobutton')


InputTypes = CInputTypes()


class CValidationTypes:
    INT = AnItem('int', 'int')
    NONE = AnItem('none', 'none')
    FLOAT = AnItem('float', 'float')
    DATETIME = AnItem('date_time', 'date_time')


ValidationTypes = CValidationTypes()


class CFilterTypes:
    EQUAL = AnItem('equal', 'equal')
    LIKE = AnItem('like', 'like')
    BETWEEN = AnItem('between', 'between')


FilterTypes = CFilterTypes()


class CPaginationLimits:
    EQUAL = AnItem('equal', 'equal')
    LIKE = AnItem('like', 'like')
    BETWEEN = AnItem('between', 'between')


PageSizes = [2, 5, 10, 25, 50, 100, 250, 500, 1000]


class CLookups:

    def __init__(self):
        from django.db import transaction
        transaction.commit()  # Whenever you want to see new data

    def extract_lookup(self, id_field, name_field, data):
        lookup_data = list()
        for ele in data:
            item = (ele if type(ele) is dict else ele.__dict__)
            lookup_data.append([item.get(id_field), item.get(name_field)])
        return lookup_data

    def user_groups(self):
        from JailuApp.models import UserGroup
        return self.extract_lookup('id', 'name', my_custom_sql("select id, name from user_group order by name asc"))

    def non_developer_users(self):
        return self.extract_lookup('id', 'full_name', my_custom_sql("select id, concat_ws(' ',first_name, sur_name) as full_name from user_account where id != 1 order by first_name asc"))


    def true_false_yn_status(self):
        return self.extract_lookup('id', 'name', [dict({'id': "True", 'name': "Yes"})
                                                  , dict({'id': "False", 'name': "No"})])
    def genders(self):
        return self.extract_lookup('id', 'name', [dict({'id': "Male", 'name': "Male"})
                                                  , dict({'id': "Female", 'name': "Female"})])

    def nrm_books(self):
        from JailuApp.models import UserGroup
        return self.extract_lookup('id', 'name', my_custom_sql("select id, book_name as name from nrm_book order by book_name asc"))

    def nrm_book_pages(self):
        from JailuApp.models import UserGroup
        return self.extract_lookup('id', 'name', my_custom_sql("""select nbp.id, concat_ws('-',nb.book_name,nbp.page_number) as name 
        from nrm_book_page nbp join nrm_book nb on nb.id = nbp.nrm_book_id order by nb.book_name asc, nbp.page_number asc"""))

    def payment_request_status(self):
        return self.extract_lookup('id', 'name', [dict({'id': "Pending", 'name': "Pending"})
                                                  , dict({'id': "Complete", 'name': "Complete"})
                                                  , dict({'id': "Rejected", 'name': "Rejected"})])

    def get_name_by_id(self, item_id, lookup_list):
        for ele in lookup_list:
            if str(ele[0]) == str(item_id):
                return ele[1]
        return item_id


Lookups = CLookups()


class MenuItem:
    def __init__(self, m_id, m_icon, m_target_object, m_target_action, m_target_object_id=''):
        self.id = str(m_id)
        self.icon = str(m_icon)
        self.caption = Lang.men_phrase(self.id)
        self.target_object = str(m_target_object)
        self.target_action = str(m_target_action)
        self.target_object_id = str(m_target_object_id)

    def code(self):
        return str(self.code)

    def name(self):
        return str(self.caption)

    def html_link(self):
        return """
        <li><a href="javascript:void(0);" 
         onclick="ShowForm('""" + self.target_action + """','""" + self.target_object + """','""" + \
               str(self.target_object_id) + """')">
              <i class="metismenu-icon """ + self.icon + """ "></i>""" + self.caption + """</a>
        </li>
        """ if Security.check_permission(self.target_object, self.target_action) is True else ""


class Menu:
    items = list()

    items.append(MenuItem(m_id='mi_performance_dashboard', m_icon='pe-7s-display2 icon-gradient bg-grow-early'
                          , m_target_object='performance_dashboard', m_target_action=Actions.List.code))

    items.append(
        MenuItem(m_id='mi_nrm_member', m_icon='pe-7s-ticket icon-gradient bg-mixed-hopes'
                 , m_target_object='nrm_member', m_target_action=Actions.List.code))
    items.append(MenuItem(m_id='mi_page_allocation', m_icon='fas fa-cog'
                          , m_target_object='page_allocation', m_target_action=Actions.List.code))
    items.append(MenuItem(m_id='mi_payment_request', m_icon='pe-7s-cash'
                          , m_target_object='payment_request', m_target_action=Actions.List.code))
    items.append(MenuItem(m_id='mi_nrm_book_page', m_icon='fas fa-cog'
                          , m_target_object='nrm_book_page', m_target_action=Actions.List.code))
    items.append(MenuItem(m_id='mi_nrm_book', m_icon='fas fa-cog'
                          , m_target_object='nrm_book', m_target_action=Actions.List.code))
    items.append(MenuItem(m_id='mi_user_group', m_icon='fas fa-cog'
                          , m_target_object='user_group', m_target_action=Actions.List.code))
    items.append(MenuItem(m_id='mi_user_account', m_icon='fas fa-cog'
                          , m_target_object='user_account', m_target_action=Actions.List.code))




    def add_item(self, m_item):
        self.items.append(m_item)

    def draw_menu(self):
        #html = """<ul class="nav nav-pills nav-sidebar flex-column"
        #data-widget="treeview" role="menu" data-accordion="false">"""
        html = """ <div class="app-sidebar-wrapper">
                <div class="app-sidebar sidebar-shadow bg-warning sidebar-text-dark">
                    <div class="app-header__logo">
                        <a href="#" data-toggle="tooltip" data-placement="bottom" title="KeroUI Admin Template" class="logo-src"></a>
                        <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                                <span class="hamburger-box">
                                    <span class="hamburger-inner"></span>
                                </span>
                        </button>
                    </div>
                    <div class="scrollbar-sidebar scrollbar-container">
                        <div class="app-sidebar__inner">
                            <ul class="vertical-nav-menu">
                                <li class="app-sidebar__heading">Menu</li>
                                <li><a href="#"><i class="metismenu-icon pe-7s-rocket"></i>Dashboards</a></li>
                                <li><a href="#"><i class="metismenu-icon pe-7s-rocket"></i>Dashboards</a></li>
                                <li><a href="#"><i class="metismenu-icon pe-7s-rocket"></i>Dashboards</a></li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div> """
        html = """ <div class="app-sidebar-wrapper">
                        <div class="app-sidebar sidebar-shadow bg-warning sidebar-text-dark">
                            <div class="app-header__logo">
                                <a href="#" data-toggle="tooltip" data-placement="bottom" title="KeroUI Admin Template" class="logo-src"></a>
                                <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                                        <span class="hamburger-box">
                                            <span class="hamburger-inner"></span>
                                        </span>
                                </button>
                            </div>
                            <div class="scrollbar-sidebar scrollbar-container">
                                <div class="app-sidebar__inner">
                                    <ul class="vertical-nav-menu">
                  """
        for x in self.items:
            html += x.html_link()
        html += """
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div> """
        return html


class CSecurity:
    permitted_actions = [Actions.Add.code, Actions.SubmitAdd.code, Actions.Edit.code
        , Actions.SubmitEdit.code, Actions.Delete.code, Actions.List.code, Actions.ExcelExport.code]
    super_admin_group_id = -1
    anonymous_group_id = -2
    table_objects = ["user_group", "user_account", "nrm_member", "nrm_book", "nrm_book_page", "page_allocation"
                     , "performance_dashboard", "payment_request"]
    is_logged_in = False
    user_profile = dict()
    user_group_id = -2
    user_account_id = None
    user_group = dict()
    permissions = dict()
    req = None

    def prepare_security(self):
        self.is_logged_in = False
        self.user_profile = dict()
        self.user_group_id = -2
        self.user_account_id = None
        self.user_group = dict()
        self.permissions = dict()
        self.req = None

    def save_user_session(self, user_id):
        # set sessions
        self.req.session[EW.session_logged_in_user_id] = user_id
        self.req.session.modified = True
        self.req.session.save()

    def clear_user_session(self):
        del self.req.session[EW.session_logged_in_user_id]
        self.req.session.modified = True
        self.req.session.save()

    def initialise(self, request):
        self.prepare_security()
        self.req = request
        if is_empty(self.req.session.get(EW.session_logged_in_user_id, '')) is False:
            # setup for saved user
            from JailuApp.models import UserAccount
            user = UserAccount.objects.filter(id=self.req.session.get(EW.session_logged_in_user_id))
            if user.count() == 1:  # make sure user was found
                self.user_profile = user[0].__dict__
                self.is_logged_in = True
                self.user_group_id = user[0].user_group_id_id
                self.user_account_id = user[0].id
                self.user_group = user[0].user_group_id.__dict__
        from JailuApp.models import GroupPermission
        permission_list = GroupPermission.objects.all().filter(user_group_id_id=self.user_group_id)
        for item in permission_list:
            self.permissions[item.table_name] = dict({Actions.Add.code: item.add, Actions.SubmitAdd.code: item.add
                                                         , Actions.Edit.code: item.edit,
                                                      Actions.SubmitEdit.code: item.edit
                                                         , Actions.Delete.code: item.delete,
                                                      Actions.List.code: item.list
                                                         , Actions.ExcelExport.code: item.list
                                                      , Actions.View.code: item.list})

    def initialise_api_user(self, user_id):
        self.prepare_security()
        if is_empty(user_id) is False:
            # setup for saved user
            from JailuApp.models import UserAccount
            user = UserAccount.objects.filter(id=user_id)
            if user.count() == 1:  # make sure user was found
                self.user_profile = user[0].__dict__
                self.is_logged_in = True
                self.user_group_id = user[0].user_group_id_id
                self.user_account_id = user[0].id
                self.user_group = user[0].user_group_id.__dict__
        from JailuApp.models import GroupPermission
        permission_list = GroupPermission.objects.all().filter(user_group_id_id=self.user_group_id)
        for item in permission_list:
            self.permissions[item.table_name] = dict({Actions.Add.code: item.add, Actions.SubmitAdd.code: item.add
                                                         , Actions.Edit.code: item.edit,
                                                      Actions.SubmitEdit.code: item.edit
                                                         , Actions.Delete.code: item.delete,
                                                      Actions.List.code: item.list
                                                         , Actions.ExcelExport.code: item.list})


    def user_full_names(self):
        return str(self.user_profile.get("first_name", "Unknown")) + " " + str(
            self.user_profile.get("sur_name", "Unknown"))

    def user_group_names(self):
        return str(self.user_group.get("name", "Unknown"))

    def get_user_info(self,param):
        return self.user_profile.get(param, None)

    def check_permission(self, table_name, action):
        if self.is_admin():  # allow all for super admins
            return True
        if action in self.permitted_actions:  # restrict known actions
            return bool(self.permissions.get(table_name, dict()).get(action, False))
        else:
            return True # allow unknown && actions

    def is_admin(self):
        return True if self.user_group_id == self.super_admin_group_id else False


Security = CSecurity()


def my_custom_sql(sql, params = []):
    from django.db import connection
    with connection.cursor() as cursor:
        cursor.execute(sql, params) # "SELECT foo FROM bar WHERE baz = %s", [self.baz]
        #  "Return all rows from a cursor as a dict"
        columns = [col[0] for col in cursor.description]
        return [
            dict(zip(columns, row))
            for row in cursor.fetchall()
        ]





def new_guid():
    import uuid
    return uuid.uuid4().hex

def enrypt_passwordMd5(str):
    import hashlib
    return hashlib.md5(str.encode()).hexdigest()


def add_link(object_name):
    return str("""
    <a class="btn btn-success btn-lg btn-wide" href="javascript:void(0);" 
     onclick="ShowForm('""" + Actions.Add.code + """','""" + str(object_name) + """')">
      <i class='fas fa-plus'></i>""" + Lang.phrase("link_add") + """
  </a>
    """) if Security.check_permission(object_name, Actions.Add.code) is True else ""


def edit_link(object_name, object_id):
    return str("""
        <a class="btn btn-info btn-sm" href="javascript:void(0);" 
        onclick="ShowForm('""" + Actions.Edit.code + """','""" + str(
        object_name) + """','""" + str(
        object_id) + """')">
          <i class='fas fa-pencil-alt'></i>""" + Lang.phrase("link_edit") + """
      </a>
        """) if Security.check_permission(object_name, Actions.Edit.code) is True else ""


def delete_link(object_name, object_id, message="This record will be deleted, you will not be able to revert this!"):
    return str("""
            <a class="btn btn-danger btn-sm" href="javascript:void(0);" 
            onclick="ConfirmDelete('""" + str(
        object_name) + """','""" + str(object_id) + """','""" + str(message) + """')">
              <i class='fas fa-trash'></i>""" + Lang.phrase("link_delete") + """
          </a>
            """) if Security.check_permission(object_name, Actions.Delete.code) is True else ""


def ex_filter_link(object_name):
    return str("""
    <a class="btn btn-primary btn-md" href="javascript:void(0);" 
     onclick="ShowForm(EW.action.List,'""" + object_name + """',null,1);" >
      <i class='fas fa-search'></i>""" + Lang.phrase("link_ex_filter") + """
  </a>
    """)


def export_excel_link(object_name):
    return str("""
    <a class="btn-icon btn btn-light" href="javascript:void(0);" 
     onclick="ShowForm(EW.action.ExcelExport,'""" + object_name + """',null,1);" >
      <i class='fas fa-file-excel'></i>""" + Lang.phrase("link_export_excel") + """
  </a>
    """) if Security.check_permission(object_name, Actions.ExcelExport.code) is True else ""


def pagination(object_name, total_records, current_page, records_per_page):
    from math import ceil
    total_pages = ceil(total_records / records_per_page)
    html = """<ul class='pagination pagination-md' >"""
    # draw page size
    html += """<li class="page-item">
               <select id="page_size" class="form-control" style="padding-right: 5px;" onchange="ShowForm(EW.action.List,'""" + object_name + """',null,1);">
               """
    for x in PageSizes:
        html += """
           <option value='""" + str(x) + """' """ + str(
            'selected' if str(x) == str(records_per_page) else '') + """ >""" + str(x) + """</option>
           """
    html += """</select></li>"""
    # only show if not first page
    if current_page != 1:
        html += """
        <li class="page-item"><a class="page-link" href='javascript:void(0);' 
        onclick="ShowForm(EW.action.List,'""" + object_name + """',null,1);" >""" + Lang.phrase("pagination_first") + """</a></li>
        """
    start = (current_page - 4) if (current_page - 4) > 0 else 1
    end = (current_page + 4) if ((current_page + 4) < total_pages) else total_pages
    for i in range(start, end):
        if current_page == i:
            html += """<li class="page-item active"><a class='page-link' href='javascript:void(0);' 
            onclick="ShowForm(EW.action.List,'""" + object_name + """',null,""" + str(i) + """);" >""" + str(
                i) + """</a></li>"""
        else:
            html += """<li class="page-item"><a class='page-link active' href='javascript:void(0);' 
            onclick="ShowForm(EW.action.List,'""" + object_name + """',null,""" + str(i) + """)" >""" + str(
                i) + """</a></li>"""
        i += 1
    # only show if not on the last page
    if current_page != total_pages:
        html += """
        <li class="page-item"><a class="page-link" href='javascript:void(0);' 
        onclick="ShowForm(EW.action.List,'""" + object_name + """',null,""" + str(
            total_pages) + """);" >""" + Lang.phrase("pagination_last") + """</a></li>
        """
    html += """<li class="page-item">
                   <label class="btn btn-md bg-info">Showing """ + str((current_page - 1) * records_per_page) \
            + """ to """ + str(current_page * records_per_page) + """ of """ + str(total_records) + """ </label>
                   """
    html += "</ul>"
    return html


def is_empty(x):
    try:
        if x is None or str(x) == "":
            return True
        else:
            return False
    except:
        return True


def is_integer(x):
    try:
        return float(x).is_integer()
    except:
        return False


def is_float(x):
    try:
        float(x)
        return True
    except:
        return False


def is_datetime(x):
    try:
        from datetime import datetime
        datetime.strptime(str(x), '%Y-%m-%d %H:%M:%S')
        return True
    except:
        return False

def is_date(x):
    try:
        from datetime import datetime
        datetime.strptime(str(x), '%Y-%m-%d')
        return True
    except:
        return False


def is_date(x):
    try:
        from datetime import datetime
        datetime.strptime(str(x), '%Y-%m-%d')
        return True
    except:
        return False


def convert_to_bool(value):
    if str(value).lower() in ["yes", "true", "t", "1"]:
        return True
    elif str(value).lower() in ["no", "false", "f", "0"]:
        return False
    else:
        return None

def convert_to_int(value):
    if is_integer(value)is True:
        return int(float(value))
    else:
        return None

def convert_to_datetime(value,date_format = ""):
    try:
        import datetime
        import re
        # if its already a datetime format
        if type(value) is datetime.date or type(value) is datetime.datetime:
            return value
        #  if a desired format is provided then use it
        elif type(value) is str and is_empty(date_format) is False:
            return datetime.datetime.strptime(value, date_format)
        # if not format passed then try default known formats
        elif type(value) is str and re.compile('.*-.*-.* .*:.*:.*').match(value) is not None:
            return datetime.datetime.strptime(value, '%Y-%m-%d %H:%M:%S')
        elif type(value) is str and re.compile('.*-.*-.*').match(value) is not None:
            return datetime.datetime.strptime(value, '%Y-%m-%d')
    except:
        return None


def excel_export(data, file_name='excel_export.xls'):
    import pyexcel
    # data = [{'name': 'shem', 'Age': 30, 'Sex': 'Male'}, {'name': 'James', 'Age': 40, 'Sex': 'Female'}]
    from django_jailuapp.settings import MEDIA_ROOT
    import os
    filename = os.path.join(MEDIA_ROOT, 'temp', file_name)
    #  filename = BASE_DIR + '/JailuApp/uploads/temp/' + file_name
    pyexcel.save_as(records=data, dest_file_name=filename)
    return file_name

def delete_file(source_file_url):
    response = {"error": True, "error_msg": "Failed to delete"}
    try:
        from django.core.files.storage import FileSystemStorage
        fs = FileSystemStorage()
        filename = fs.delete(source_file_url)
        response["error"] = False
        response["error_msg"] = "Success"
    except Exception as x:
        response["error"] = True
        response["error_msg"] = "Failed to save file eroor:"+str(x)
    return response

def copy_file(source_file_url, destination_file_url):
    response = {"error": True, "error_msg": "Failed to upload"}
    try:
        from django.core.files.storage import FileSystemStorage
        fs = FileSystemStorage()
        filename = fs.save(destination_file_url, fs.open(source_file_url))
        uploaded_file_url = fs.url(filename)
        # from shutil import copy
        # uploaded_file_url = copy(source_file_url, destination_file_url)
        response["error"] = False
        response["error_msg"] = "Success"
        response["uploaded_file_url"] = filename
        response["uploaded_file_name"] = filename[filename.rfind("/")+1:]
    except Exception as x:
        response["error"] = True
        response["error_msg"] = "Failed to save file eroor:"+str(x)
    return response

def upload_temp_file(myfile, suggested_file_name):
    response = {"error": True, "error_msg": "Failed to upload"}
    try:
        from django_jailuapp.settings import MEDIA_ROOT
        import os
        saved_filename = os.path.join(MEDIA_ROOT, 'temp', suggested_file_name)
        from django.core.files.storage import FileSystemStorage
        fs = FileSystemStorage()
        filename = fs.save(saved_filename, myfile)
        uploaded_file_url = fs.url(filename)
        response["error"] = False
        response["error_msg"] = "Success"
        response["uploaded_file_url"] = filename
        response["uploaded_file_name"] = filename[filename.rfind("/")+1:]
    except Exception as x:
        response["error"] = True
        response["error_msg"] = "Failed to save file eroor:"+str(x)
    return response


def open_cvs_file(source_file_url):
    response = {"error": True, "error_msg": "Failed to upload", "headers":None, "data":None}
    try:
        import csv
        file = open(source_file_url, "r")
        csv_reader = csv.DictReader(file)
        headers = csv_reader.fieldnames
        data = list()
        for row in csv_reader:
            # collect actual data as a dicionary of the heading
            data.append(dict(row))
        #compile response
        response["error"] = False
        response["headers"] = headers
        response["data"] = data
        response["error_msg"] = "Success"
    except Exception as x:
        response["error"] = True
        response["error_msg"] = "Failed to open file error:"+str(x)
    return response


def current_datetime():
    from datetime import datetime
    #return datetime.today()
    import pytz
    return datetime.now(pytz.timezone(EW.default_timezone))


def current_user_id():
    return Security.user_account_id


def get_short_month(monthinteger):
    try:
        import datetime
        return datetime.date(1900, int(monthinteger), 1).strftime("%b")
    except:
        return monthinteger


def get_date_only_str(value):
    try:
        import datetime
        #datetime.strptime(str(x), '%Y-%m-%d %H:%M:%S')
        if type(value) is datetime.date or type(value) is datetime.datetime:
            return value.strftime("%a-%d-%b")
        elif type(value) is str:
            date = datetime.datetime.strptime(value, '%Y-%m-%d')
            return date.strftime("%a-%d-%b")
    except:
        return str(value)


def time_elapsed_str(past_time,full = True,current_time = None):
    try:
        import datetime
        # auto set now_time  to now if not specified
        if current_time is None:
            current_time = datetime.datetime.now()
        # convert to datetime if string
        import re
        if type(past_time) is str and re.compile('.*-.*-.* .*:.*:.*').match(past_time) is not None:
            past_time = datetime.datetime.strptime(past_time, '%Y-%m-%d %H:%M:%S')
        elif type(past_time) is str and re.compile('.*-.*-.*').match(past_time) is not None:
            past_time = datetime.datetime.strptime(past_time, '%Y-%m-%d')

        ela = (current_time - past_time)
        total_seconds = ela.total_seconds()
        elapsed_str = ""
        if total_seconds >= (60 * 60 * 24) and (full is False or (full is True and elapsed_str == "")):
            elapsed_str += " "+ str(int(total_seconds // (60 * 60 * 24))) + "Days"
            total_seconds -= ((total_seconds // (60 * 60 * 24)) * (60 * 60 * 24))
        if total_seconds >= (60 * 60) and (full is False or (full is True and elapsed_str == "")):
            elapsed_str += " "+ str(int(total_seconds // (60 * 60))) + "Hrs"
            total_seconds -= ((total_seconds // (60 * 60)) * (60 * 60))
        if total_seconds >= (60) and (full is False or (full is True and elapsed_str == "")):
            elapsed_str += " "+ str(int(total_seconds // (60))) + "mins"
            total_seconds -= ((total_seconds // (60)) * (60))
        if total_seconds >= 0 and (full is False or (full is True and elapsed_str == "")):
            elapsed_str += " "+ str(int(total_seconds)) + "secs"
        return str(elapsed_str)
    except:
        return "Unknown"




