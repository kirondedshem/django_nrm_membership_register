from JailuApp.classes.base_structures import *
from JailuApp.models import UserGroup, GroupPermission, InsuranceRecord


class TableFrontEnd(TableObjectBase):
    # table specific settings
    object_name = 'front_end'
    js_execution_timer = 10000
    js_idle_execution_timer = 120000

    # field settings
    fields = {
        "id": TableFieldBase(object_name, 'id', InputTypes.TEXT.code, ValidationTypes.NONE.code
                             , {Actions.Add.code: False, Actions.Edit.code: True, Actions.List.code: False}
                             , {Actions.SubmitAdd.code: False, Actions.Edit.code: True
                                 , Actions.SubmitEdit.code: True, Actions.Delete.code: True}
                             , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '', 'value2': ''})
        ,
        "setup_date": TableFieldBase(object_name, 'setup_date', InputTypes.DATETIME.code, ValidationTypes.DATETIME.code
                                     , {Actions.Add.code: False, Actions.Edit.code: False, Actions.List.code: False}
                                     , {Actions.Add.code: False, Actions.SubmitAdd.code: False, Actions.Edit.code: False,
                                        Actions.SubmitEdit.code: True, Actions.Delete.code: False}
                                     , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '', 'value2': ''})
        , "agent": TableFieldBase(object_name, 'agent', InputTypes.DROPDOWN.code, ValidationTypes.NONE.code
                                  , {Actions.Add.code: False, Actions.Edit.code: False, Actions.List.code: False}
                                  , {Actions.Add.code: False, Actions.SubmitAdd.code: False, Actions.Edit.code: False
                                      , Actions.SubmitEdit.code: False, Actions.Delete.code: False}
                                  , {'show': True, 'filter_type': FilterTypes.EQUAL.code, 'value': '', 'value2': ''},
                                  Lookups.non_developer_users())
        , "full_name": TableFieldBase(object_name, 'full_name', InputTypes.TEXT.code, ValidationTypes.NONE.code
                                      , {Actions.Add.code: True, Actions.Edit.code: True, Actions.List.code: True}
                                      , {Actions.Add.code: True, Actions.SubmitAdd.code: True, Actions.Edit.code: True,
                                         Actions.SubmitEdit.code: True, Actions.Delete.code: False}
                                      ,
                                      {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '', 'value2': ''})
        , "primary_phone": TableFieldBase(object_name, 'primary_phone', InputTypes.TEXT.code, ValidationTypes.NONE.code
                                          , {Actions.Add.code: True, Actions.Edit.code: True, Actions.List.code: True}
                                          , {Actions.Add.code: True, Actions.SubmitAdd.code: True,
                                             Actions.Edit.code: True,
                                             Actions.SubmitEdit.code: True, Actions.Delete.code: False}
                                          , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '',
                                             'value2': ''})
        , "primary_email": TableFieldBase(object_name, 'primary_email', InputTypes.TEXT.code, ValidationTypes.NONE.code
                                          , {Actions.Add.code: True, Actions.Edit.code: True, Actions.List.code: True}
                                          , {Actions.Add.code: True, Actions.SubmitAdd.code: True,
                                             Actions.Edit.code: True,
                                             Actions.SubmitEdit.code: True, Actions.Delete.code: False}
                                          , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '',
                                             'value2': ''})
        , "file_national_id": TableFieldBase(object_name, 'file_national_id', InputTypes.FILE.code,
                                             ValidationTypes.NONE.code
                                             ,
                                             {Actions.Add.code: True, Actions.Edit.code: True, Actions.List.code: True}
                                             , {Actions.Add.code: True, Actions.SubmitAdd.code: True,
                                                Actions.Edit.code: True,
                                                Actions.SubmitEdit.code: True, Actions.Delete.code: False}
                                             , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '',
                                                'value2': ''})
        , "phone_model": TableFieldBase(object_name, 'phone_model', InputTypes.DROPDOWN.code, ValidationTypes.NONE.code
                                        , {Actions.Add.code: True, Actions.Edit.code: True, Actions.List.code: True}
                                        , {Actions.Add.code: True, Actions.SubmitAdd.code: True, Actions.Edit.code: True
                                            , Actions.SubmitEdit.code: True, Actions.Delete.code: False}
                                        , {'show': True, 'filter_type': FilterTypes.EQUAL.code, 'value': '',
                                           'value2': ''})
        , "phone_imei": TableFieldBase(object_name, 'phone_imei', InputTypes.TEXT.code, ValidationTypes.NONE.code
                                       , {Actions.Add.code: True, Actions.Edit.code: True, Actions.List.code: True}
                                       , {Actions.Add.code: True, Actions.SubmitAdd.code: True, Actions.Edit.code: True,
                                          Actions.SubmitEdit.code: True, Actions.Delete.code: False}
                                       ,
                                       {'show': True, 'filter_type': FilterTypes.LIKE.code, 'value': '', 'value2': ''})
        , "bought_on": TableFieldBase(object_name, 'bought_on', InputTypes.DATETIME.code, ValidationTypes.DATETIME.code
                                      , {Actions.Add.code: True, Actions.Edit.code: True, Actions.List.code: True}
                                      , {Actions.Add.code: True, Actions.SubmitAdd.code: True, Actions.Edit.code: True,
                                         Actions.SubmitEdit.code: True, Actions.Delete.code: False}
                                      ,
                                      {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '', 'value2': ''})
        , "amount_worth": TableFieldBase(object_name, 'amount_worth', InputTypes.TEXT.code, ValidationTypes.INT.code
                                         , {Actions.Add.code: True, Actions.Edit.code: True, Actions.List.code: True}
                                         ,
                                         {Actions.Add.code: True, Actions.SubmitAdd.code: True, Actions.Edit.code: True,
                                          Actions.SubmitEdit.code: True, Actions.Delete.code: False}
                                         , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '',
                                            'value2': ''})
        , "insurance_body": TableFieldBase(object_name, 'insurance_body', InputTypes.DROPDOWN.code,
                                           ValidationTypes.NONE.code
                                           , {Actions.Add.code: False, Actions.Edit.code: False, Actions.List.code: False}
                                           , {Actions.Add.code: False, Actions.SubmitAdd.code: False, Actions.Edit.code: False
                                               , Actions.SubmitEdit.code: False, Actions.Delete.code: False}
                                           , {'show': True, 'filter_type': FilterTypes.EQUAL.code, 'value': '',
                                              'value2': ''}, Lookups.insurance_bodies())
        , "insurance_number": TableFieldBase(object_name, 'insurance_number', InputTypes.TEXT.code,
                                             ValidationTypes.NONE.code
                                             , {Actions.Add.code: False, Actions.Edit.code: False, Actions.List.code: False}
                                             , {Actions.Add.code: False, Actions.SubmitAdd.code: False, Actions.Edit.code: False,
                                                Actions.SubmitEdit.code: False, Actions.Delete.code: False}
                                             , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '',
                                                'value2': ''})
        , "entry_date": TableFieldBase(object_name, 'entry_date', InputTypes.TEXT.code, ValidationTypes.NONE.code
                                       , {Actions.Add.code: False, Actions.Edit.code: False, Actions.List.code: False}
                                       , {Actions.Add.code: False, Actions.SubmitAdd.code: False, Actions.Edit.code: False
                                           , Actions.SubmitEdit.code: False, Actions.Delete.code: False}
                                       , {'show': False, 'filter_type': FilterTypes.BETWEEN.code, 'value': '',
                                          'value2': ''})
        , "entered_by": TableFieldBase(object_name, 'entered_by', InputTypes.TEXT.code, ValidationTypes.NONE.code
                                       , {Actions.Add.code: False, Actions.Edit.code: False, Actions.List.code: False}
                                       , {Actions.Add.code: False, Actions.SubmitAdd.code: False,
                                          Actions.Edit.code: False,
                                          Actions.SubmitEdit.code: False, Actions.Delete.code: False}
                                       ,
                                       {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '', 'value2': ''})
        , "last_modified": TableFieldBase(object_name, 'last_modified', InputTypes.TEXT.code, ValidationTypes.NONE.code
                                          ,
                                          {Actions.Add.code: False, Actions.Edit.code: False, Actions.List.code: False}
                                          , {Actions.Add.code: False, Actions.SubmitAdd.code: False,
                                             Actions.Edit.code: False,
                                             Actions.SubmitEdit.code: False, Actions.Delete.code: False}
                                          , {'show': False, 'filter_type': FilterTypes.BETWEEN.code, 'value': '',
                                             'value2': ''})
        , "modified_by": TableFieldBase(object_name, 'modified_by', InputTypes.TEXT.code, ValidationTypes.NONE.code
                                        , {Actions.Add.code: False, Actions.Edit.code: False, Actions.List.code: False}
                                        , {Actions.Add.code: False, Actions.SubmitAdd.code: False,
                                           Actions.Edit.code: False,
                                           Actions.SubmitEdit.code: False, Actions.Delete.code: False}
                                        , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '',
                                           'value2': ''})

    }

    def submit_insurance_form(self, data):
        api_response = {"error": True, "error_msg": "Unknown API error", "html": ""}
        #  indicate add operation
        self.current_action = Actions.SubmitAdd.code
        api_response = self.validate_form_data()
        if api_response["error"] is True:  # check primary validations
            return api_response
        else:


            if is_empty(self.fields["file_national_id"].current_value) is False:
                from django_jailuapp.settings import MEDIA_ROOT
                import os
                source_file = os.path.join(MEDIA_ROOT, 'temp', self.fields["file_national_id"].current_value)
                destination_file = os.path.join(MEDIA_ROOT, 'insurance_record_file_national_id',
                                                self.fields["file_national_id"].current_value)
                #  save the temp file to destination
                ret = copy_file(source_file, destination_file)

                if ret["error"] is True:
                    api_response["error"] = True
                    api_response["error_msg"] = "Failed to save file_national_id file error:" + ret["error_msg"]
                    return api_response
                else:
                    # remove temp file
                    delete_file(source_file)
                    # get actual saved file_name
                    self.fields["file_national_id"].current_value = ret["uploaded_file_name"]
                    # populate model and perform db operation

                    InsuranceRecord(id=new_guid()
                                    , phone_model_id=self.fields["phone_model"].current_value
                                    , setup_date=current_datetime()
                                    , full_name=self.fields["full_name"].current_value
                                    , primary_phone=self.fields["primary_phone"].current_value
                                    , primary_email=self.fields["primary_email"].current_value
                                    , file_national_id=self.fields["file_national_id"].current_value
                                    , phone_imei=self.fields["phone_imei"].current_value
                                    , bought_on=self.fields["bought_on"].current_value
                                    , amount_worth=self.fields["amount_worth"].current_value
                                    , entry_date=current_datetime()
                                    , modified_by_id=current_user_id()
                                    ).save()

                    api_response = {"error": False, "error_msg": Lang.fld_phrase(self.object_name, "submit_insurance_form_success")}

        return api_response


    def register_insurance_form(self, data):
        api_response = {"error": True, "error_msg": "Unknown API error", "html": ""}

        #  update lookup
        self.fields["phone_model"].lookup_data = Lookups.phone_models();

        # draw js
        html = """
            <script>
            
            var wizard_current_step = 1;
            
    
            function LoadStep1()
            {
                if(wizard_current_step == 2)
                {
                //update nav
                $('[nav-item="step-1"]').removeClass("active").removeClass("done").addClass("active");
                $('[nav-item="step-2"]').removeClass("active").removeClass("done").addClass("done");
                $('[nav-item="step-3"]').removeClass("active").removeClass("done");
                
                //show content
                $("#step-1").show();
                $("#step-2").hide();
                $("#step-3").hide();
                
                //show buttons
                $("#prev-btn").hide();
                $("#next-btn").show();
                $("#submit-btn").hide();
                
                wizard_current_step = 1;
                }
                else
                {
                console.log('you must be on step 2');
                }
            
            }
            
            function LoadStep2()
            {
                if(wizard_current_step == 1)
                {
                //update nav
                $('[nav-item="step-1"]').removeClass("active").removeClass("done").addClass("done");
                $('[nav-item="step-2"]').removeClass("active").removeClass("done").addClass("active");
                $('[nav-item="step-3"]').removeClass("active").removeClass("done");
                
                //show content
                $("#step-1").hide();
                $("#step-2").show();
                $("#step-3").hide();
                
                //show buttons
                $("#prev-btn").show();
                $("#next-btn").hide();
                $("#submit-btn").show();
                
                
                wizard_current_step = 2;
                }
                else
                {
                console.log('you must be on step 1');
                }
            }
            
            function LoadLastStep()
            {
                if(wizard_current_step == 2)
                {
                //update nav
                $('[nav-item="step-1"]').removeClass("active").removeClass("done").addClass("done");
                $('[nav-item="step-2"]').removeClass("active").removeClass("done").addClass("done");
                $('[nav-item="step-3"]').removeClass("active").removeClass("done").addClass("done");
                
                //show content
                $("#step-1").hide();
                $("#step-2").hide();
                $("#step-3").show();
                
                //show buttons
                $("#prev-btn").hide();
                $("#next-btn").hide();
                $("#submit-btn").hide();
                
                wizard_current_step = 3;
                }
                else
                {
                console.log('you must be on step 2');
                }
            
            }
            
            
            function submit_insurance_form()
            {
                var action = 'submit_insurance_form';
                var object = 'front_end';
                const active_form_values = CollectFormParameters(object);
                var object_id = null;
                    $.ajax({
                    type: "POST",
                    cache: false,
                    url: "api",
                    dataType :"json",
                    data: { rand_time: new Date().getTime(), api_object: object, api_action: action, object_id: object_id, form_values: JSON.stringify(active_form_values)} ,
                    success: function(response){
                      //console.log(response);
                      //read data out
                      if(response.error === false)
                      {
                        //show success message
                        ShowSuccessMessage(response.error_msg);
                        //update wizard
                        LoadLastStep();
            
                      }
                      else
                      {
                           //show error message
                          ShowFailureMessage(response.error_msg)
                      }
                    },
                    error: function(data){
                    console.log('failed to Load form:',data);
                    //show error message
                    ShowFailureMessage("Oops Server failed to complete this request, Check your Internet Connection");
            
                    }
                    });
            
            
            }
    
            
    
            </script>
           """

        html += """
        
        <div class="app-container app-theme-white body-tabs-shadow">
            <div class="app-container">
                <div class="h-100">
                    <div class="h-100 no-gutters row">
                        <div class="justify-content-center align-items-center col-md-12 col-lg-7">
                            <div class="mx-auto app-login-box col-sm-12 col-md-10 col-lg-9">
                                <div class="app-logo"></div>
                                <h4>
                                    <div>Welcome to Mobile Phone Insurance,</div>
                                    <span>Tired of always worrying that your phone will get lost one day and you will have to buy a new one all over again. It only takes a <span class="text-success">minute</span> to insure your phone</span></h4>
                                <div>
                                    <div >
        
                                        <div class="col-lg-12 col-md-12">
                                            <div class="main-card mb-3 card">
                                                <div class="card-body">
                                        
                                                    <div id="smartwizard">
                                                        <ul class="forms-wizard">
                                                            <li nav-item="step-1" class="nav-item active">
                                                                <a href="javascript:void(0);">
                                                                    <em>1</em><span>Personal Information</span>
                                                                </a>
                                                            </li>
                                                            <li nav-item="step-2" class="nav-item done">
                                                                <a href="javascript:void(0);">
                                                                    <em>2</em><span>Phone Details</span>
                                                                </a>
                                                            </li>
                                                            <li nav-item="step-3" class="nav-item">
                                                                <a href="javascript:void(0);">
                                                                    <em>3</em><span>Submit</span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                        
                                                        <div class="form-wizard-content">
                                                            <div id="step-1" style="display: block;">
                                                                <div class="form-row">
                                                                    <div class="col-md-12">
                                                                        """+ self.fields["full_name"].html_control(Actions.Add.code) +"""
                                                                    </div>
                                                                    <div class="col-md-12">
                                                                        """+ self.fields["primary_phone"].html_control(Actions.Add.code) +"""
                                                                    </div>
                                                                    <div class="col-md-12">
                                                                        """+ self.fields["primary_email"].html_control(Actions.Add.code) +"""
                                                                    </div>
                                                                    <div class="col-md-12">
                                                                        """+ self.fields["file_national_id"].html_control(Actions.Add.code) +"""
                                                                    </div>
                                                                    
                                                                </div>
                                                                
                                                                <div class='form-row form-check'>
                                                                    <input name='tot_check' id='tot_check' type='checkbox' class='form-check-input'>
                                                                    <label for='exampleCheck' class='form-check-label'>Accept our <a href='javascript:void(0);'>Terms and Conditions</a>.
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div id="step-2" style="display: none;">
                                                                <div class="form-row">
                                                                    <div class="col-md-12">
                                                                        """+ self.fields["phone_model"].html_control(Actions.Add.code) +"""
                                                                    </div>
                                                                    <div class="col-md-12">
                                                                        """+ self.fields["phone_imei"].html_control(Actions.Add.code) +"""
                                                                    </div>
                                                                    <div class="col-md-12">
                                                                        """+ self.fields["bought_on"].html_control(Actions.Add.code) +"""
                                                                    </div>
                                                                    <div class="col-md-12">
                                                                        """+ self.fields["amount_worth"].html_control(Actions.Add.code) +"""
                                                                    </div>
                                                                    
                                                                </div>
                                                                
                                                            </div>
                                                            <div id="step-3" style="display: none;">
                                                                <div class="no-results">
                                                                    <div class="swal2-icon swal2-success swal2-animate-success-icon">
                                                                        <div class="swal2-success-circular-line-left"
                                                                             style="background-color: rgb(255, 255, 255);"></div>
                                                                        <span class="swal2-success-line-tip"></span>
                                                                        <span class="swal2-success-line-long"></span>
                                                                        <div class="swal2-success-ring"></div>
                                                                        <div class="swal2-success-fix"
                                                                             style="background-color: rgb(255, 255, 255);"></div>
                                                                        <div class="swal2-success-circular-line-right"
                                                                             style="background-color: rgb(255, 255, 255);"></div>
                                                                    </div>
                                                                    <div class="results-subtitle mt-4">Finished!</div>
                                                                    <div class="results-title">Registration completed successfully. We shall contact you for followup!
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="divider"></div>
                                                    <div class="clearfix">
                                                        <button onclick="front_end_showForm('register_insurance_form')" type="button" id="reset-btn"
                                                                class="btn-shadow float-left btn btn-link">Reset
                                                        </button>
                                                        <button id="submit-btn" style='display: none;' onclick="submit_insurance_form()" type="button" class="btn-shadow btn-wide float-right btn-pill btn-hover-shine btn btn-success btn-lg">
                                                            Submit
                                                        </button>
                                                        <button onclick="LoadStep2()" type="button" id="next-btn"
                                                                class="btn-shadow btn-wide float-right btn-pill btn-hover-shine btn btn-info">
                                                            Next
                                                        </button>
                                                        <button style='display: none;' onclick="LoadStep1()" type="button" id="prev-btn"
                                                                class="btn-shadow float-right btn-wide btn-pill mr-3 btn btn-outline-secondary">
                                                            Previous
                                                        </button>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="justify-content-center align-items-center col-md-12 col-lg-5">
                            <div class="slider-light">
                                <div class="slick-slider slick-initialized">
                                    <div>
                                        <div class="h-100 d-flex justify-content-center align-items-center bg-premium-dark" tabindex="-1">
                                            <div class="slide-img-bg" style="background-image: url('assets/images/originals/citynights.jpg');"></div>
                                            <div class="slider-content"><h3>Quick, Reliable, Trustworthy</h3>
                                                <p>Insure your phone today by submitting the form. 
                                                You will be required to pay a small premium. 
                                                Your phone will be insured against loss and damage.</p></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>

        """

        api_response = {"error": False, "error_msg": "Success", "html": html}
        return api_response


    def perform_action(self, data=None):
        # in case data was passed from a form
        # in case data was passed from a form
        if data is None:
            data = dict()
        self.extract_data(data)

        # perform desired action
        api_response = {"error": True, "error_msg": "Unsupported Operation"}
        if Security.check_permission(self.object_name, data["api_action"]) is False:
            api_response["error"] = True
            api_response["error_msg"] = Lang.phrase("permission_denied")
        elif data["api_action"] == Actions.List.code:
            api_response = self.html_list_form()
        elif data["api_action"] == "register_insurance_form":
            api_response = self.register_insurance_form(data)
        elif data["api_action"] == "submit_insurance_form":
            api_response = self.submit_insurance_form(data)

        return api_response



