from JailuApp.classes.base_structures import *
from JailuApp.models import UserAccount


class TableUserAccount(TableObjectBase):
    # table specific settings
    object_name = 'user_account'

    # field settings
    fields = {
        "id": TableFieldBase(object_name, 'id', InputTypes.TEXT.code, ValidationTypes.NONE.code
             , {Actions.Add.code: False, Actions.Edit.code: True, Actions.List.code: False}
             , {Actions.SubmitAdd.code: False, Actions.Edit.code: True,
                Actions.SubmitEdit.code: True, Actions.Delete.code: True}
             , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '', 'value2': ''})
        , "first_name": TableFieldBase(object_name, 'first_name', InputTypes.TEXT.code, ValidationTypes.NONE.code
                 , {Actions.Add.code: True, Actions.Edit.code: True, Actions.List.code: True}
                 , {Actions.Add.code: True, Actions.SubmitAdd.code: True, Actions.Edit.code: True,
                    Actions.SubmitEdit.code: True, Actions.Delete.code: False}
             , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '', 'value2': ''})
        , "sur_name": TableFieldBase(object_name, 'sur_name', InputTypes.TEXT.code, ValidationTypes.NONE.code
           , {Actions.Add.code: True, Actions.Edit.code: True, Actions.List.code: True}
           , {Actions.Add.code: True, Actions.SubmitAdd.code: True, Actions.Edit.code: True,
              Actions.SubmitEdit.code: True, Actions.Delete.code: False}
           , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '', 'value2': ''})
        , "primary_phone": TableFieldBase(object_name, 'primary_phone', InputTypes.TEXT.code, ValidationTypes.NONE.code
             , {Actions.Add.code: True, Actions.Edit.code: True, Actions.List.code: True}
             , {Actions.Add.code: True, Actions.SubmitAdd.code: True, Actions.Edit.code: True,
                Actions.SubmitEdit.code: True, Actions.Delete.code: False}
             , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '', 'value2': ''})
        , "email": TableFieldBase(object_name, 'email', InputTypes.TEXT.code, ValidationTypes.NONE.code
                                  , {Actions.Add.code: True, Actions.Edit.code: True, Actions.List.code: True}
                                  , {Actions.Add.code: True, Actions.SubmitAdd.code: True, Actions.Edit.code: True,
                                     Actions.SubmitEdit.code: True, Actions.Delete.code: False}
                                  , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '', 'value2': ''})
        , "user_group_id": TableFieldBase(object_name, 'user_group_id', InputTypes.DROPDOWN.code, ValidationTypes.INT.code
               , {Actions.Add.code: True, Actions.Edit.code: True, Actions.List.code: True}
               , {Actions.Add.code: True, Actions.SubmitAdd.code: True, Actions.Edit.code: True,
                  Actions.SubmitEdit.code: True, Actions.Delete.code: False}
               , {'show': True, 'filter_type': FilterTypes.EQUAL.code, 'value': '', 'value2': ''})
        , "user_name": TableFieldBase(object_name, 'user_name', InputTypes.TEXT.code, ValidationTypes.NONE.code
               , {Actions.Add.code: True, Actions.Edit.code: True, Actions.List.code: True}
               , {Actions.Add.code: True, Actions.SubmitAdd.code: True, Actions.Edit.code: True,
                  Actions.SubmitEdit.code: True, Actions.Delete.code: False}
               , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '', 'value2': ''})
        , "password": TableFieldBase(object_name, 'password', InputTypes.TEXT.code, ValidationTypes.NONE.code
               , {Actions.Add.code: True, Actions.Edit.code: False, Actions.List.code: False}
               , {Actions.Add.code: True, Actions.SubmitAdd.code: True, Actions.Edit.code: False,
                  Actions.SubmitEdit.code: False, Actions.Delete.code: False}
               , {'show': False, 'filter_type': FilterTypes.BETWEEN.code, 'value': '', 'value2': ''})
        , "entry_date": TableFieldBase(object_name, 'entry_date', InputTypes.TEXT.code, ValidationTypes.NONE.code
           , {Actions.Add.code: False, Actions.Edit.code: False, Actions.List.code: False}
           , {Actions.Add.code: False, Actions.SubmitAdd.code: False, Actions.Edit.code: False
           , Actions.SubmitEdit.code: False, Actions.Delete.code: False}
           , {'show': False, 'filter_type': FilterTypes.BETWEEN.code, 'value': '','value2': ''})
        , "entered_by": TableFieldBase(object_name, 'entered_by', InputTypes.TEXT.code, ValidationTypes.NONE.code
             , {Actions.Add.code: False, Actions.Edit.code: False, Actions.List.code: False}
             , {Actions.Add.code: False, Actions.SubmitAdd.code: False, Actions.Edit.code: False,
                Actions.SubmitEdit.code: False, Actions.Delete.code: False}
             , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '', 'value2': ''})
        , "last_modified": TableFieldBase(object_name, 'last_modified', InputTypes.TEXT.code, ValidationTypes.NONE.code
             , {Actions.Add.code: False, Actions.Edit.code: False, Actions.List.code: False}
             , {Actions.Add.code: False, Actions.SubmitAdd.code: False, Actions.Edit.code: False,
                Actions.SubmitEdit.code: False, Actions.Delete.code: False}
             , {'show': False, 'filter_type': FilterTypes.BETWEEN.code, 'value': '', 'value2': ''})
        , "modified_by": TableFieldBase(object_name, 'modified_by', InputTypes.TEXT.code, ValidationTypes.NONE.code
             , {Actions.Add.code: False, Actions.Edit.code: False, Actions.List.code: False}
             , {Actions.Add.code: False, Actions.SubmitAdd.code: False, Actions.Edit.code: False,
                Actions.SubmitEdit.code: False, Actions.Delete.code: False}
             , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '', 'value2': ''})

    }

    def select_all_records(self):
        # put custom implementation for model
        sql = """select det.*, ug.name as group_name 
        , concat_ws(' ',eb.first_name, eb.sur_name) as entered_by_name 
        , concat_ws(' ',mb.first_name, mb.sur_name) as modified_by_name 
        from user_account det join user_group ug on ug.id = det.user_group_id_id 
        left join user_account eb on eb.id = det.entered_by
        left join user_account mb on mb.id = det.modified_by
        where 1=1 """;


        # apply filters if available
        if is_empty(self.fields["user_group_id"].ex_search["value"]) is False:
            sql += " and det.user_group_id_id = '" + str(self.fields["user_group_id"].ex_search["value"]).lower() + "' "
        # apply sort
        sql += " order by det.entry_date DESC"
        # apply limit and offset
        return dict(list=self.apply_sql_limit(sql), count=self.count_sql_result(sql))

    # override for lookup information
    def get_list_data(self):
        data = self.select_all_records()
        data_list = list()
        for item in data["list"]:
            an_item = dict()
            an_item["id"] = TableFieldListItem("id", item.get("id"), item.get("id"))
            an_item["first_name"] = TableFieldListItem("first_name", item.get("first_name"), item.get("first_name"))
            an_item["sur_name"] = TableFieldListItem("sur_name", item.get("sur_name"), item.get("sur_name"))
            an_item["email"] = TableFieldListItem("email", item.get("email"), item.get("email"))
            an_item["primary_phone"] = TableFieldListItem("primary_phone", item.get("primary_phone"), item.get("primary_phone"))
            an_item["user_group_id"] = TableFieldListItem("user_group_id", item.get("user_group_id_id"), item.get("group_name"))
            an_item["user_name"] = TableFieldListItem("user_name", item.get("user_name"), item.get("user_name"))
            if self.current_action == Actions.ExcelExport.code:
                an_item["entry_date"] = TableFieldListItem("entry_date", item.get("entry_date"), item.get("entry_date"))
                an_item["entered_by"] = TableFieldListItem("entered_by", item.get("entered_by"), item.get("entered_by_name"))
                an_item["last_modified"] = TableFieldListItem("last_modified", item.get("last_modified"), item.get("last_modified"))
                an_item["modified_by"] = TableFieldListItem("modified_by", item.get("modified_by"), item.get("modified_by_name"))
            data_list.append(an_item)
        return {"list": data_list, "count": data["count"]}

    def select_a_record(self, object_id):
        # put custom implementation for model
        return UserAccount.objects.filter(id=object_id)  # get record to be edited

    def get_record_data(self, object_id):
        obj = self.select_a_record(object_id)
        if obj.count() == 1:
            dict_obj = obj[0].__dict__
            for a_field in self.fields:
                if dict_obj.get(self.fields[a_field].object_name, None) is not None:
                    self.fields[a_field].current_value = dict_obj.get(self.fields[a_field].object_name)
                    self.fields[a_field].view_value = self.fields[a_field].current_value
            #  cater for foreign keys
            self.fields["user_group_id"].current_value = dict_obj.get("user_group_id_id")
            self.fields["user_group_id"].view_value = self.fields["user_group_id"].current_value

    def insert_row(self):
        api_response = {"error": False, "error_msg": "Insert completed successfully"}
        # populate model and perform db operation
        UserAccount(id=new_guid(), first_name=self.fields["first_name"].current_value
                    , sur_name=self.fields["sur_name"].current_value
                    , email=self.fields["email"].current_value
                    , primary_phone=self.fields["primary_phone"].current_value
                    , user_group_id_id=self.fields["user_group_id"].current_value
                    , user_name=self.fields["user_name"].current_value
                    , password=enrypt_passwordMd5(self.fields["password"].current_value)
                    , entry_date=current_datetime()
                    , entered_by=current_user_id()
                    , last_modified=current_datetime()
                    , modified_by=current_user_id()
                    ).save()
        return api_response

    def update_row(self):
        api_response = {"error": False, "error_msg": "Insert completed successfully"}
        # populate model and perform db operation
        obj = UserAccount.objects.get(id=self.fields["id"].current_value)
        # specify updates
        obj.first_name = self.fields["first_name"].current_value
        obj.sur_name = self.fields["sur_name"].current_value
        obj.email = self.fields["email"].current_value
        obj.primary_phone = self.fields["primary_phone"].current_value
        obj.user_group_id_id = self.fields["user_group_id"].current_value
        obj.user_name = self.fields["user_name"].current_value
        obj.last_modified = current_datetime()
        obj.modified_by = current_user_id()
        obj.save()
        return api_response

    def delete_row(self):
        api_response = {"error": False, "error_msg": "Delete completed successfully"}
        from django.db import IntegrityError
        try:
            # populate model and perform db operation
            obj = UserAccount.objects.get(id=self.fields["id"].current_value)
            # specify updates
            obj.delete()
        except IntegrityError as e:
            api_response = {"error": True,
                            "error_msg": "Can not delete " + self.caption + " because its used elsewhere"}

        return api_response

    def html_add_form_page_load(self):
        self.fields["user_group_id"].lookup_data = Lookups.user_groups()

    def html_edit_form_page_load(self):
        self.fields["user_group_id"].lookup_data = Lookups.user_groups()

    def html_list_form_page_load(self):
        self.fields["user_group_id"].lookup_data = Lookups.user_groups()


