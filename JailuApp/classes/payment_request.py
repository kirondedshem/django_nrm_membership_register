from JailuApp.classes.base_structures import *
from JailuApp.models import PaymentRequest, PaymentRequestDetail


class TablePaymentRequest(TableObjectBase):
    # table specific settings
    object_name = 'payment_request'

    # add other variables
    rate_per_record = 50

    # field settings
    fields = {
        "id": TableFieldBase(object_name, 'id', InputTypes.TEXT.code, ValidationTypes.NONE.code
             , {Actions.Add.code: False, Actions.Edit.code: True, Actions.List.code: False}
             , {Actions.SubmitAdd.code: False, Actions.Edit.code: True
             , Actions.SubmitEdit.code: True, Actions.Delete.code: True}
             , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '', 'value2': ''})
        , "entry_date": TableFieldBase(object_name, 'entry_date', InputTypes.DATETIME.code, ValidationTypes.NONE.code
                                       , {Actions.Add.code: False, Actions.Edit.code: False, Actions.List.code: True}
                                       ,
                                       {Actions.Add.code: False, Actions.SubmitAdd.code: False, Actions.Edit.code: False
                                           , Actions.SubmitEdit.code: False, Actions.Delete.code: False}
                                       , {'show': True, 'filter_type': FilterTypes.BETWEEN.code, 'value': '',
                                          'value2': ''})
        , "agent": TableFieldBase(object_name, 'agent', InputTypes.DROPDOWN.code,
                                          ValidationTypes.NONE.code
                                          , {Actions.Add.code: True, Actions.Edit.code: True, Actions.List.code: True}
                                          , {Actions.Add.code: True, Actions.SubmitAdd.code: True,
                                             Actions.Edit.code: False,
                                             Actions.SubmitEdit.code: False, Actions.Delete.code: False}
                                          , {'show': True, 'filter_type': FilterTypes.EQUAL.code, 'value': '',
                                             'value2': ''})
        , "number_of_records": TableFieldBase(object_name, 'number_of_records', InputTypes.TEXT.code, ValidationTypes.INT.code
           , {Actions.Add.code: True, Actions.Edit.code: True, Actions.List.code: True}
           , {Actions.Add.code: True, Actions.SubmitAdd.code: True, Actions.Edit.code: True,
              Actions.SubmitEdit.code: False, Actions.Delete.code: False}
           , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '', 'value2': ''})
        , "rate": TableFieldBase(object_name, 'rate', InputTypes.TEXT.code,
                                              ValidationTypes.INT.code
                                              , {Actions.Add.code: False, Actions.Edit.code: True,
                                                 Actions.List.code: True}
                                              , {Actions.Add.code: False, Actions.SubmitAdd.code: False,
                                                 Actions.Edit.code: False,
                                                 Actions.SubmitEdit.code: False, Actions.Delete.code: False}
                                              , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '',
                                                 'value2': ''})
        , "amount": TableFieldBase(object_name, 'amount', InputTypes.TEXT.code,
                                              ValidationTypes.INT.code
                                              , {Actions.Add.code: False, Actions.Edit.code: True,
                                                 Actions.List.code: True}
                                              , {Actions.Add.code: False, Actions.SubmitAdd.code: False,
                                                 Actions.Edit.code: False,
                                                 Actions.SubmitEdit.code: False, Actions.Delete.code: False}
                                              , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '',
                                                 'value2': ''})
        , "status": TableFieldBase(object_name, 'status', InputTypes.DROPDOWN.code,
                                   ValidationTypes.NONE.code
                                   , {Actions.Add.code: False, Actions.Edit.code: True,
                                      Actions.List.code: True}
                                   , {Actions.Add.code: False, Actions.SubmitAdd.code: False,
                                      Actions.Edit.code: True,
                                      Actions.SubmitEdit.code: True, Actions.Delete.code: False}
                                   , {'show': True, 'filter_type': FilterTypes.LIKE.code, 'value': '',
                                      'value2': ''})
        , "status_note": TableFieldBase(object_name, 'status_note', InputTypes.TEXTAREA.code,
                                   ValidationTypes.NONE.code
                                   , {Actions.Add.code: False, Actions.Edit.code: True,
                                      Actions.List.code: True}
                                   , {Actions.Add.code: False, Actions.SubmitAdd.code: False,
                                      Actions.Edit.code: False,
                                      Actions.SubmitEdit.code: False, Actions.Delete.code: False}
                                   , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '',
                                      'value2': ''})
        , "evidence_attachment": TableFieldBase(object_name, 'evidence_attachment', InputTypes.FILE.code,
                                   ValidationTypes.NONE.code
                                   , {Actions.Add.code: False, Actions.Edit.code: True,
                                      Actions.List.code: True}
                                   , {Actions.Add.code: False, Actions.SubmitAdd.code: False,
                                      Actions.Edit.code: False,
                                      Actions.SubmitEdit.code: False, Actions.Delete.code: False}
                                   , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '',
                                      'value2': ''})
        , "entered_by": TableFieldBase(object_name, 'entered_by', InputTypes.DROPDOWN.code, ValidationTypes.NONE.code
                                       , {Actions.Add.code: False, Actions.Edit.code: False, Actions.List.code: False}
                                       , {Actions.Add.code: False, Actions.SubmitAdd.code: False,
                                          Actions.Edit.code: False,
                                          Actions.SubmitEdit.code: False, Actions.Delete.code: False}
                                       ,
                                       {'show': True, 'filter_type': FilterTypes.EQUAL.code, 'value': '', 'value2': ''})
        , "last_modified": TableFieldBase(object_name, 'last_modified', InputTypes.TEXT.code, ValidationTypes.NONE.code
             , {Actions.Add.code: False, Actions.Edit.code: False, Actions.List.code: False}
             , {Actions.Add.code: False, Actions.SubmitAdd.code: False, Actions.Edit.code: False,
                Actions.SubmitEdit.code: False, Actions.Delete.code: False}
             , {'show': False, 'filter_type': FilterTypes.BETWEEN.code, 'value': '', 'value2': ''})
        , "modified_by": TableFieldBase(object_name, 'modified_by', InputTypes.TEXT.code, ValidationTypes.NONE.code
             , {Actions.Add.code: False, Actions.Edit.code: False, Actions.List.code: False}
             , {Actions.Add.code: False, Actions.SubmitAdd.code: False, Actions.Edit.code: False,
                Actions.SubmitEdit.code: False, Actions.Delete.code: False}
             , {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '', 'value2': ''})

    }

    def select_all_records(self):
        # put custom implementation for model
        sql = """select det.* 
        , concat_ws(' ',agt.first_name, agt.sur_name) as agent_name
        , concat_ws(' ',eb.first_name, eb.sur_name) as entered_by_name 
        , concat_ws(' ',mb.first_name, mb.sur_name) as modified_by_name
        from payment_request det 
        join user_account agt on agt.id = det.agent_id
        left join user_account eb on eb.id = det.entered_by_id
        left join user_account mb on mb.id = det.modified_by_id
         where 1=1 """;

        # apply filters if available
        if is_empty(self.fields["entry_date"].ex_search["value"]) is False and is_empty(self.fields["entry_date"].ex_search["value2"]) is False:
            sql += """ and ( det.entry_date >= '""" + str(self.fields["entry_date"].ex_search["value"]) + """' 
             and det.entry_date <= '""" +str(self.fields["entry_date"].ex_search["value2"])+ """') """
        if is_empty(self.fields["agent"].ex_search["value"]) is False:
            sql += " and det.agent_id = '" + str(self.fields["agent"].ex_search["value"]) + "' "
        if is_empty(self.fields["status"].ex_search["value"]) is False:
            sql += " and nbp.status = '" + str(self.fields["status"].ex_search["value"]) + "' "


        # apply default filter for agents to see only thier allocations
        if str(Security.user_group_id) == "1":
            sql += " and det.agent_id = '" + str(current_user_id()) + "' "

        # apply sort
        sql += " order by det.entry_date DESC"

        # apply limit and offset
        return dict(list=self.apply_sql_limit(sql), count=self.count_sql_result(sql))

    # override for lookup information
    def get_list_data(self):
        data = self.select_all_records()
        data_list = list()
        for item in data["list"]:
            an_item = dict()
            an_item["id"] = TableFieldListItem("id", item.get("id"), item.get("id"))
            an_item["agent"] = TableFieldListItem("agent", item.get("agent_id"),
                                                          item.get("agent_name"))
            an_item["number_of_records"] = TableFieldListItem("number_of_records", item.get("number_of_records"), item.get("number_of_records"))
            an_item["rate"] = TableFieldListItem("rate", item.get("rate"), item.get("rate"))
            an_item["amount"] = TableFieldListItem("amount", item.get("amount"), item.get("amount"))
            an_item["status"] = TableFieldListItem("status", item.get("status"), item.get("status"))
            an_item["status_note"] = TableFieldListItem("status_note", item.get("status_note"), item.get("status_note"))

            import os
            file_url = os.path.join('media', 'payment_request_evidence_attachment',
                                    str(item.get("evidence_attachment", "")))

            file_view = """
                                                <a target='_blank' href='""" + file_url + """' class="avatar-icon-wrapper avatar-icon-xl">
                                                  <div class="avatar-icon rounded"><img style='image-orientation:from-image;width: 100%;' src='""" + file_url + """' alt=""></div>
                                                </a>
                                                """

            an_item["evidence_attachment"] = TableFieldListItem("evidence_attachment", item.get("evidence_attachment"), file_view)

            an_item["entry_date"] = TableFieldListItem("entry_date", item.get("entry_date"), item.get("entry_date"))


            if self.current_action == Actions.ExcelExport.code:
                an_item["entered_by"] = TableFieldListItem("entered_by", item.get("entered_by_id"),
                                                           item.get("entered_by_name"))
                an_item["last_modified"] = TableFieldListItem("last_modified", item.get("last_modified"), item.get("last_modified"))
                an_item["modified_by"] = TableFieldListItem("modified_by", item.get("modified_by_id"), item.get("modified_by_name"))

            data_list.append(an_item)
        return {"list": data_list, "count": data["count"]}

    def select_a_record(self, object_id):
        # put custom implementation for model
        return PaymentRequest.objects.filter(id=object_id)  # get record to be edited

    def get_record_data(self, object_id):
        obj = self.select_a_record(object_id)
        if obj.count() == 1:
            dict_obj = obj[0].__dict__
            for a_field in self.fields:
                if dict_obj.get(self.fields[a_field].object_name, None) is not None:
                    self.fields[a_field].current_value = dict_obj.get(self.fields[a_field].object_name)
                    self.fields[a_field].view_value = self.fields[a_field].current_value
            #  cater for foreign keys
            self.fields["agent"].current_value = dict_obj.get("agent_id")
            self.fields["agent"].view_value = self.fields["agent"].current_value


    def submit_add_form(self):
        api_response = self.validate_form_data()
        if api_response["error"] is True:  # check primary validations
            return api_response

        minimum_limit = 211

        sql = """select ifnull(count(nm.id),0) as value from nrm_member nm 
        where nm.entered_by_id = '"""+ str(self.fields["agent"].current_value) +"""' 
                         and nm.id not in (select prd.nrm_member_id from payment_request_detail prd 
                          join payment_request pr on pr.id = prd.payment_request_id and pr.status != 'Rejected' ) """
        billable_records = my_custom_sql(sql)[0]["value"]
        #  do extra validations
        if billable_records < int(self.fields["number_of_records"].current_value):
            api_response["error"] = True
            api_response["error_msg"] = " You can only be paid for records not exceeding " +str(billable_records)+ ", you only get paid for what you input, Please contact your administrator"
        elif int(self.fields["number_of_records"].current_value) < minimum_limit:
            api_response["error"] = True
            api_response["error_msg"] = " You can only request payment after you hit or exceed your weekly target of " +str(minimum_limit)+ " records, Please contact your administrator"
        else:

            self.fields["rate"].current_value = self.rate_per_record
            self.fields["rate"].view_value = self.rate_per_record

            self.fields["amount"].current_value = self.rate_per_record * int(self.fields["number_of_records"].current_value)
            self.fields["amount"].view_value = self.rate_per_record * int(self.fields["number_of_records"].current_value)

            api_response = self.insert_row()  # run insert query
            if api_response["error"] is True:  # check insert worked
                return api_response
            api_response = self.row_inserted()

        return api_response

    def insert_row(self):
        api_response = {"error": False, "error_msg": "Insert completed successfully"}

        payment_request_id = new_guid()
        # populate model and perform db operation
        PaymentRequest(id=payment_request_id
                    , agent_id=self.fields["agent"].current_value
                    , number_of_records=self.fields["number_of_records"].current_value
                    , rate=self.fields["rate"].current_value
                    , amount=self.fields["amount"].current_value
                    , status="Pending"
                    , entry_date=current_datetime()
                    , entered_by_id=current_user_id()
                    , last_modified=current_datetime()
                    , modified_by_id=current_user_id()
                    ).save()

        # save all records billed sofar
        sql = """select nm.id from nrm_member nm 
                where nm.entered_by_id = '""" + str(self.fields["agent"].current_value) + """' 
                                 and nm.id not in (select prd.nrm_member_id from payment_request_detail prd 
                                  join payment_request pr on pr.id = prd.payment_request_id 
                                  and pr.status != 'Rejected' )  
                                  LIMIT """ + str(self.fields["number_of_records"].current_value) + """ """
        records = my_custom_sql(sql)

        bulk_detail_separator = list()
        bulk_detail_objects = list()
        max_bulk_size = 1000

        for a_row in records:


            # see if bulk has reached peak and paginate it
            if bulk_detail_objects.__len__() == max_bulk_size:
                # add bulk to heap
                bulk_detail_separator.append(bulk_detail_objects)
                # empty bulk to create new one
                bulk_detail_objects = list()

            # append to group insert
            bulk_detail_objects.append(
                PaymentRequestDetail(id=new_guid()
                         , payment_request_id=payment_request_id
                         , nrm_member_id=str(a_row["id"])
                         , entry_date=current_datetime()
                         , entered_by_id=current_user_id()
                         , last_modified=current_datetime()
                         , modified_by_id=current_user_id()

                         ))

        # add last alert bulk to heap
        # see if last bulk is not empty and add it
        if bulk_detail_objects.__len__() > 0:
            # add bulk to heap
            bulk_detail_separator.append(bulk_detail_objects)
            # empty bulk to create new one
            bulk_detail_objects = list()

        #perform insert
        for a_batch in bulk_detail_separator:
            PaymentRequestDetail.objects.bulk_create(a_batch)


        return api_response

    def submit_edit_form(self):
        api_response = self.validate_form_data()
        if api_response["error"] is True:  # check primary validations
            return api_response

        obj = PaymentRequest.objects.get(id=self.fields["id"].current_value)
        #  do extra validations
        if obj.status != "Pending":
            api_response["error"] = True
            api_response["error_msg"] = " This payment request was already completed or rejected"
        elif str(self.fields["status"].current_value) == "Rejected" and is_empty(self.fields["status_note"].current_value):
            api_response["error"] = True
            api_response["error_msg"] = self.fields["status_note"].caption+ " is required if its rejected"
        elif str(self.fields["status"].current_value) == "Complete" and is_empty(self.fields["evidence_attachment"].current_value):
            api_response["error"] = True
            api_response[
                "error_msg"] = self.fields["evidence_attachment"].caption+ " is required if its payment is Complete"
        else:
            api_response = self.update_row()  # run insert query
            if api_response["error"] is True:  # check insert worked
                return api_response
            api_response = self.row_updated()

        return api_response

    def update_row(self):
        api_response = {"error": False, "error_msg": "Insert completed successfully"}


        # populate model and perform db operation
        obj = PaymentRequest.objects.get(id=self.fields["id"].current_value)

        #  delete old file
        if is_empty(obj.evidence_attachment) is False:
            from django_jailuapp.settings import MEDIA_ROOT
            import os
            delete_file(os.path.join(MEDIA_ROOT, 'payment_request_evidence_attachment', obj.evidence_attachment))

        if is_empty(self.fields["evidence_attachment"].current_value) is False:
            from django_jailuapp.settings import MEDIA_ROOT
            import os
            source_file = os.path.join(MEDIA_ROOT, 'temp', self.fields["evidence_attachment"].current_value)
            destination_file = os.path.join(MEDIA_ROOT, 'payment_request_evidence_attachment',
                                            self.fields["evidence_attachment"].current_value)
            #  save the temp file to destination
            ret = copy_file(source_file, destination_file)

            if ret["error"] is True:
                api_response["error"] = True
                api_response["error_msg"] = "Failed to save front_page file error:" + ret["error_msg"]
                return api_response
            else:
                # remove temp file
                delete_file(source_file)
                # get actual saved file_name
                self.fields["evidence_attachment"].current_value = ret["uploaded_file_name"]
                # populate model and perform db operation


        # specify updates
        obj.status = self.fields["status"].current_value
        obj.status_note = self.fields["status_note"].current_value
        obj.evidence_attachment = self.fields["evidence_attachment"].current_value
        obj.last_modified = current_datetime()
        obj.modified_by_id = current_user_id()
        obj.save()
        return api_response



    def html_add_form(self):
        # call page load
        self.html_add_form_page_load()

        api_response = {"header_title": self.caption + "s", "content_title": self.caption + " Add",
                        "content_body": "content_body", "content_footer": "content_footer",
                        "error": True, "error_msg": "Unknown API Error"}
        html_text = """
        <div class="card card-info">
        <div class="card-body">
        <div class='col-lg-12 row'>
        """
        # add all add form fields
        for a_field in self.fields:
            if self.fields[a_field].show_on[self.current_action] is True:
                html_text += """
                        <div class='col-xs-6 col-sm-6 col-lg-6 row form-group' """ + ("hidden='true'" if self.object_name == "id" else "") + """ 
                        control_for='x_""" + self.object_name + """'>
                            """ + self.fields[a_field].html_caption(self.current_action) + """
                            """ + self.fields[a_field].html_input(self.current_action) + """
                        </div>
                        """
        #  show page preview
        html_text += """
                <div class='col-lg-12' id="amount_preview">
                Amount:XXXX
                </div>
                """

        html_text += """
        </div>
        </div>
        <div class="card-footer">
          <button onclick="SubmitForm('""" + Actions.SubmitAdd.code + """','""" + self.object_name \
                     + """')" class="btn btn-info">""" + Lang.phrase("btn_add") + """</button>
          <button onclick="ShowForm('""" + Actions.List.code + """','""" + self.object_name \
                     + """')" class="btn btn-default float-right">""" + Lang.phrase("btn_cancel") + """</button>
        </div>
        </div>
        """

        # add js
        html_text += """
                <script>
                //for firefox
                //fetch drop down items
                $('#x_agent option').click(function(){
                    //get parameter values
                    var agent_id = $("#x_agent").val();
                    var number_of_records = $("#x_number_of_records").val();
                    get_draw_html('amount_preview','""" + self.object_name + """','get_amount_preview',parameters = {agent_id:agent_id, number_of_records:number_of_records });

                });

                //for chrome compatibility
                //fetch drop down items
                $('#x_agent').change(function(){
                    //get parameter values
                    var agent_id = $("#x_agent").val();
                    var number_of_records = $("#x_number_of_records").val();
                    get_draw_html('amount_preview','""" + self.object_name + """','get_amount_preview',parameters = {agent_id:agent_id, number_of_records:number_of_records });

                });
                
               
                $('#x_number_of_records').keyup(function(){
                    //get parameter values
                    var agent_id = $("#x_agent").val();
                    var number_of_records = $("#x_number_of_records").val();
                    get_draw_html('amount_preview','""" + self.object_name + """','get_amount_preview',parameters = {agent_id:agent_id, number_of_records:number_of_records });

                });
                
                //do initial calculation
                $('#x_number_of_records').trigger('keyup');

                </script>
                """

        api_response["error"] = False
        api_response["error_msg"] = "Completed Successfully"
        api_response["content_body"] = html_text
        return api_response

    def html_edit_form(self):
        self.html_edit_form_page_load()
        api_response = {"header_title": self.caption + "s", "content_title": self.caption + " Edit",
                        "content_body": "content_body", "content_footer": "content_footer",
                        "error": True, "error_msg": "Unknown API Error"}
        html_text = """
        <div class="card card-info">
        <div class="card-body">
        <div class='col-lg-12'>
        """
        # add all add form fields
        for a_field in self.fields:
            html_text += self.fields[a_field].html_control(Actions.Edit.code)
        html_text += """
        </div>
        </div>
        <div class="card-footer">
          <button onclick="SubmitForm('""" + Actions.SubmitEdit.code + """','""" + self.object_name \
                     + """')" class="btn btn-info">""" + Lang.phrase("btn_save") + """</button>
          <button onclick="ShowForm('""" + Actions.List.code + """','""" + self.object_name \
                     + """')" class="btn btn-default float-right">""" + Lang.phrase("btn_cancel") + """</button>
        </div>
        </div>
        """

        api_response["error"] = False
        api_response["error_msg"] = "Completed Successfully"
        api_response["content_body"] = html_text

        return api_response

    def get_amount_preview(self, data):
        api_response = {"error": True, "error_msg": "Unknown API error", "html": ""}
        agent_id = data.get("agent_id", "")
        number_of_records = data.get("number_of_records", 0)

        #cleanup amount
        if is_integer(number_of_records) is False:
            number_of_records = 0

        amount = self.rate_per_record * int(number_of_records)
        html = """<h1>Total Payment Amount = """ + str(amount) + """</h1>"""

        api_response["html"] = html
        api_response["error"] = False
        api_response["error_msg"] = "Success"

        return api_response

    def html_add_form_page_load(self):
        if str(Security.user_group_id) == "1":
            self.fields["agent"].default_add = current_user_id()
            self.fields["agent"].lookup_data = Lookups.extract_lookup('id', 'full_name', my_custom_sql(
                "select id, concat_ws(' ',first_name, sur_name) as full_name from user_account where id = '" + current_user_id() + "' order by first_name asc"))
        else:
            self.fields["agent"].lookup_data = Lookups.non_developer_users()

        # set default payable records
        sql = """select ifnull(count(nm.id),0) as value from nrm_member nm 
                where nm.entered_by_id = '""" + str(current_user_id()) + """' 
                                 and nm.id not in (select prd.nrm_member_id from payment_request_detail prd 
                                  join payment_request pr on pr.id = prd.payment_request_id and pr.status != 'Rejected' ) """
        billable_records = my_custom_sql(sql)[0]["value"]
        self.fields["number_of_records"].default_add = billable_records

        self.fields["status"].lookup_data = Lookups.payment_request_status()

    def html_edit_form_page_load(self):

        if str(Security.user_group_id) == "1":
            self.fields["agent"].default_add = current_user_id()
            self.fields["agent"].lookup_data = Lookups.extract_lookup('id', 'full_name', my_custom_sql(
                "select id, concat_ws(' ',first_name, sur_name) as full_name from user_account where id = '" + current_user_id() + "' order by first_name asc"))
        else:
            self.fields["agent"].lookup_data = Lookups.non_developer_users()
        self.fields["status"].lookup_data = Lookups.payment_request_status()

    def html_list_form_page_load(self):
        if str(Security.user_group_id) == "1":
            self.fields["agent"].default_add = current_user_id()
            self.fields["agent"].lookup_data = Lookups.extract_lookup('id', 'full_name', my_custom_sql(
                "select id, concat_ws(' ',first_name, sur_name) as full_name from user_account where id = '" + current_user_id() + "' order by first_name asc"))
        else:
            self.fields["agent"].lookup_data = Lookups.non_developer_users()
        self.fields["status"].lookup_data = Lookups.payment_request_status()


    def perform_action(self, data=None):

        # in case data was passed from a form
        if data is None:
            data = dict()
        self.extract_data(data)

        # perform desired action
        api_response = {"error": True, "error_msg": "Unsupported Operation"}
        if Security.check_permission(self.object_name, data["api_action"]) is False:
            api_response["error"] = True
            api_response["error_msg"] = Lang.phrase("permission_denied")
        elif data["api_action"] == Actions.List.code:
            api_response = self.html_list_form()
        elif data["api_action"] == Actions.Add.code:
            api_response = self.html_add_form()
        elif data["api_action"] == Actions.SubmitAdd.code:
            api_response = self.submit_add_form()
        elif data["api_action"] == Actions.Edit.code:
            api_response = self.html_edit_form()
        elif data["api_action"] == Actions.SubmitEdit.code:
            api_response = self.submit_edit_form()
        elif data["api_action"] == Actions.Delete.code:
            api_response = self.submit_delete_form()
        elif data["api_action"] == Actions.ExcelExport.code:
            api_response = self.html_list_export()
        elif data["api_action"] == "get_amount_preview":
            api_response = self.get_amount_preview(data)

        return api_response


