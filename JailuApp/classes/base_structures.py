from JailuApp.classes.global_code import *


class TableFieldBase:
    table_object_name = 'unknown'
    object_name = 'unknown'
    caption = 'unknown'
    field_type = InputTypes.TEXT.code
    required = {Actions.Add.code: True, Actions.Edit.code: True}
    show_on = {Actions.Add.code: True, Actions.Edit.code: True, Actions.List.code: True}
    ex_search = {'show': False, 'filter_type': FilterTypes.LIKE.code, 'value': '', 'value2': ''}
    validation_type = ValidationTypes.NONE.code
    default_add = None
    current_value = ''
    view_value = ''
    lookup_data = None

    def __init__(self, m_table_object_name, m_object_name, m_field_type, m_validation_type, m_show_on
                 , m_required, m_ex_search, m_lookup_data=None, m_default_add=None):
        self.table_object_name = str(m_table_object_name)
        self.object_name = str(m_object_name)
        self.field_type = str(m_field_type)
        self.validation_type = str(m_validation_type)
        self.show_on = m_show_on
        self.required = m_required
        self.caption = Lang.fld_phrase(self.table_object_name,self.object_name)
        self.default_add = m_default_add
        self.ex_search = m_ex_search
        self.lookup_data = m_lookup_data

    def str_default_add(self):
        try:
            return '' if self.default_add is None else str(self.default_add)
        except:
            return ''

    def html_control(self, action):
        if self.show_on[action] is True:
            return str("""
            <div class='col-lg-12 row form-group' """ + ("hidden='true'" if self.object_name == "id" else "") + """ 
            control_for='x_""" + self.object_name + """'>
                """ + self.html_caption(action) + """
                """ + self.html_input(action) + """
            </div>
            """)
        else:
            return ""

    def html_ex_search(self, action):
        if self.ex_search["show"] is True:
            return str("""
            <div class='text-center form-group' control_for='xf_""" + self.object_name + """'>
                """ + self.html_caption(action) + """
                """ + self.html_input(action) + """
            </div>
            """)
        else:
            return ""

    def html_input(self, action):
        if action == Actions.Add.code and self.field_type == InputTypes.TEXT.code:
            return str("""
                <div class='col-lg-8'>
                <input class='form-control' table_object_name='""" + self.table_object_name
                       + """' field_object_name='""" + self.object_name + """' placeholder='"""
                       + self.caption + """' id='x_""" + self.object_name + """' name='x_"""
                       + self.object_name + """' value='""" + self.str_default_add() + """'></input>
                </div>
            """)
        elif action == Actions.Edit.code and self.field_type == InputTypes.TEXT.code:
            return ("""
                <div class='col-lg-8'>
                <input """ + (
                "hidden='true' disabled='true'" if self.object_name == "id" else "")
                    + """ class='form-control' table_object_name='""" + self.table_object_name
                    + """' field_object_name='""" + self.object_name + """' placeholder='"""
                    + self.caption + """' id='x_""" + self.object_name + """' name='x_"""
                    + self.object_name + """' value='"""
                    + ( "" if is_empty(self.current_value) else str(self.current_value) ) + """'></input>
                </div>
            """)
        elif action == Actions.List.code and self.field_type == InputTypes.TEXT.code:
            return str("""
                <div >
                <input class='form-control' table_object_name='""" + self.table_object_name
                       + """' field_object_name='""" + self.object_name + """' placeholder='"""
                       + self.caption + """' id='xf_""" + self.object_name + """' name='xf_"""
                       + self.object_name + """' value='""" + str(
                        self.ex_search["value"]) + """'></input>
                </div>
            """)
        elif action == Actions.Add.code and self.field_type == InputTypes.DROPDOWN.code:
            html = """
                <div class='col-lg-8'>
                <select class='form-control' table_object_name='""" + self.table_object_name + """' field_object_name
                ='""" + self.object_name + """' placeholder='""" + self.caption + """' id='x_""" + self.object_name +\
                   """' name='x_""" + self.object_name + """' > 
                   <option value=''>""" + Lang.phrase('please_select') + """</option>
                """
            # fill with lookup data
            if self.lookup_data is not None:  # only loop when you have values
                for ele in self.lookup_data:
                    html += """<option """ + ('selected' if str(self.str_default_add()) == str(ele[0]) else '') +\
                            """ value='""" + str(ele[0]) + """'>""" + str(ele[1]) + """</option> """

            html += """</select></div>"""
            return html
        elif action == Actions.Edit.code and self.field_type == InputTypes.DROPDOWN.code:
            html = """
                <div class='col-lg-8'>
                <select class='form-control' table_object_name='""" + self.table_object_name + """' field_object_name
                ='""" + self.object_name + """' placeholder='""" + self.caption + """' id='x_""" + self.object_name +\
                   """' name='x_""" + self.object_name + """' > 
                   <option value=''>""" + Lang.phrase('please_select') + """</option>
                """
            # fill with lookup data
            if self.lookup_data is not None:  # only loop when you have values
                for ele in self.lookup_data:
                    html += """<option """ + ('selected' if str(self.current_value) == str(ele[0]) else '') +\
                            """ value='""" + str(ele[0]) + """'>""" + str(ele[1]) + """</option> """

            html += """</select></div>"""
            return html
        elif action == Actions.List.code and self.field_type == InputTypes.DROPDOWN.code:
            html = """
                <div class='col-lg-8'>
                <select class='form-control' table_object_name='""" + self.table_object_name + """' field_object_name
                ='""" + self.object_name + """' placeholder='""" + self.caption + """' id='x_""" + self.object_name +\
                   """' name='x_""" + self.object_name + """' > 
                   <option value=''>""" + Lang.phrase('please_select') + """</option>
                """
            # fill with lookup data
            if self.lookup_data is not None:  # only loop when you have values
                for ele in self.lookup_data:
                    html += """<option """ + ('selected' if str(self.ex_search["value"]) == str(ele[0]) else '') +\
                            """ value='""" + str(ele[0]) + """'>""" + str(ele[1]) + """</option> """

            html += """</select></div>"""
            return html
        elif action == Actions.Add.code and self.field_type == InputTypes.DATETIME.code:
            return str("""
                <div class='col-lg-8'>
                <input class='form-control' table_object_name='""" + self.table_object_name
                       + """' field_object_name='""" + self.object_name + """' placeholder='"""
                       + self.caption + """' id='x_""" + self.object_name + """' name='x_"""
                       + self.object_name + """' value='""" + self.str_default_add() + """'></input>
                <script>
                $('#x_""" + self.object_name + """').datetimepicker({format: 'YYYY-MM-DD H:mm:ss',icons: {
                    time: "pe-7s-clock",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                }});
                </script>
                </div>
            """)
        elif action == Actions.Edit.code and self.field_type == InputTypes.DATETIME.code:
            return ("""
                <div class='col-lg-8'>
                <input class='form-control' table_object_name='""" + self.table_object_name
                    + """' field_object_name='""" + self.object_name + """' placeholder='"""
                    + self.caption + """' id='x_""" + self.object_name + """' name='x_"""
                    + self.object_name + """' value='""" + str(
                        self.current_value) + """'></input>
                <script>
                $('#x_""" + self.object_name + """').datetimepicker({format: 'YYYY-MM-DD H:mm:ss',icons: {
                    time: "pe-7s-clock",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                }});
                </script>
                </div>
            """)
        elif action == Actions.List.code and self.field_type == InputTypes.DATETIME.code:
            html = """
                <div >
                <input class='form-control' table_object_name='""" + self.table_object_name + """' 
                 field_object_name='""" + self.object_name + """' placeholder='""" + self.caption + """' 
                  id='x_""" + self.object_name + """' name='x_""" + self.object_name + """' 
                  value='""" + str(self.ex_search["value"]) + """'></input>
                <script>
                $('#x_""" + self.object_name + """').datetimepicker({format: 'YYYY-MM-DD H:mm:ss',icons: {
                    time: "pe-7s-clock",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                }});
                </script>
                """
            # if between draw a second one
            if self.ex_search["filter_type"] == FilterTypes.BETWEEN.code:
                html += """
                <span class="badge bg-info">AND</span>
                <input class='form-control' table_object_name='""" + self.table_object_name + """' 
                 field_object_name='y_""" + self.object_name + """' placeholder='""" + self.caption + """2' 
                  id='y_""" + self.object_name + """' name='y_""" + self.object_name + """' 
                  value='""" + str(self.ex_search["value2"]) + """'></input>
                <script>
                $('#y_""" + self.object_name + """').datetimepicker({format: 'YYYY-MM-DD H:mm:ss', icons: {
                    time: "pe-7s-clock",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                }});
                </script>
                """

            html += """ </div>  """
            return html
        elif action == Actions.Add.code and self.field_type == InputTypes.DATE.code:
            return str("""
                <div class='col-lg-8'>
                <input class='form-control' table_object_name='""" + self.table_object_name
                       + """' field_object_name='""" + self.object_name + """' placeholder='"""
                       + self.caption + """' id='x_""" + self.object_name + """' name='x_"""
                       + self.object_name + """' value='""" + self.str_default_add() + """'></input>
                <script>
                $('#x_""" + self.object_name + """').datetimepicker({format: 'YYYY-MM-DD',icons: {
                    time: "pe-7s-clock",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                }});
                </script>
                </div>
            """)
        elif action == Actions.Edit.code and self.field_type == InputTypes.DATE.code:
            return ("""
                <div class='col-lg-8'>
                <input class='form-control' table_object_name='""" + self.table_object_name
                    + """' field_object_name='""" + self.object_name + """' placeholder='"""
                    + self.caption + """' id='x_""" + self.object_name + """' name='x_"""
                    + self.object_name + """' value='""" + str(
                        self.current_value) + """'></input>
                <script>
                $('#x_""" + self.object_name + """').datetimepicker({format: 'YYYY-MM-DD',icons: {
                    time: "pe-7s-clock",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                }});
                </script>
                </div>
            """)
        elif action == Actions.List.code and self.field_type == InputTypes.DATE.code:
            html = """
                <div >
                <input class='form-control' table_object_name='""" + self.table_object_name + """' 
                 field_object_name='""" + self.object_name + """' placeholder='""" + self.caption + """' 
                  id='x_""" + self.object_name + """' name='x_""" + self.object_name + """' 
                  value='""" + str(self.ex_search["value"]) + """'></input>
                <script>
                $('#x_""" + self.object_name + """').datetimepicker({format: 'YYYY-MM-DD',icons: {
                    time: "pe-7s-clock",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                }});
                </script>
                """
            # if between draw a second one
            if self.ex_search["filter_type"] == FilterTypes.BETWEEN.code:
                html += """
                <span class="badge bg-info">AND</span>
                <input class='form-control' table_object_name='""" + self.table_object_name + """' 
                 field_object_name='y_""" + self.object_name + """' placeholder='""" + self.caption + """2' 
                  id='y_""" + self.object_name + """' name='y_""" + self.object_name + """' 
                  value='""" + str(self.ex_search["value2"]) + """'></input>
                <script>
                $('#y_""" + self.object_name + """').datetimepicker({format: 'YYYY-MM-DD', icons: {
                    time: "pe-7s-clock",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                }});
                </script>
                """

            html += """ </div>  """
            return html

        elif action == Actions.Add.code and self.field_type == InputTypes.FILE.code:
            return str("""
                <div class='col-lg-8'>
                <input hidden='true' disabled='true' class='form-control' table_object_name='""" + self.table_object_name
                    + """' field_object_name='""" + self.object_name + """' placeholder='"""
                    + self.caption + """' id='x_""" + self.object_name + """' name='x_"""
                    + self.object_name + """' value='""" + str(
                        self.current_value) + """'></input>
                <form id='form_for_x_""" + self.object_name + """' action="upload_file" method="post" 
                enctype="multipart/form-data" target='iframe_for_x_""" + self.object_name + """'  >
                    <input onchange="start_tempUpload('x_""" + self.object_name + """');" class='form-control' type="file" fileuploaderfor='x_"""
                       + self.object_name + """' id='x_""" + self.object_name + """_uploader' name='x_"""
                       + self.object_name + """_uploader' size="30" />
                     
                     <iframe onchange="temp_uploadCallback('x_""" + self.object_name + """');" 
                     id='iframe_for_x_""" + self.object_name + """' name='iframe_for_x_""" + self.object_name + """' 
                     src="#" style="width:0;height:0;border:0px solid #fff;"></iframe>
                 </form>
                </div>
            """)
        elif action == Actions.Edit.code and self.field_type == InputTypes.FILE.code:
            return str("""
                        <div class='col-lg-8'>
                        <input hidden='true' disabled='true' class='form-control' table_object_name='""" + self.table_object_name
                   + """' field_object_name='""" + self.object_name + """' placeholder='"""
                   + self.caption + """' id='x_""" + self.object_name + """' name='x_"""
                   + self.object_name + """' value='""" + str(
            self.current_value) + """'></input>
                        <form id='form_for_x_""" + self.object_name + """' action="upload_file" method="post" 
                        enctype="multipart/form-data" target='iframe_for_x_""" + self.object_name + """'  >
                            <input onchange="start_tempUpload('x_""" + self.object_name + """');" class='form-control' type="file" fileuploaderfor='x_"""
                   + self.object_name + """' id='x_""" + self.object_name + """_uploader' name='x_"""
                   + self.object_name + """_uploader' value='""" + str(
            self.current_value) + """' size="30" />

                             <iframe onchange="temp_uploadCallback('x_""" + self.object_name + """');" 
                             id='iframe_for_x_""" + self.object_name + """' name='iframe_for_x_""" + self.object_name + """' 
                             src="#" style="width:0;height:0;border:0px solid #fff;"></iframe>
                         </form>
                        </div>
                    """)
        elif action == Actions.Add.code and self.field_type == InputTypes.TEXTAREA.code:
            return str("""
                <div class='col-lg-8'>
                <textarea class='form-control' table_object_name='""" + self.table_object_name
                       + """' field_object_name='""" + self.object_name + """' placeholder='"""
                       + self.caption + """' id='x_""" + self.object_name + """' name='x_"""
                       + self.object_name + """' >""" + self.str_default_add() + """</textarea>
                </div>
            """)
        elif action == Actions.Edit.code and self.field_type == InputTypes.TEXTAREA.code:
            return ("""
                <div class='col-lg-8'>
                <textarea """ + (
                "hidden='true' disabled='true'" if self.object_name == "id" else "")
                    + """ class='form-control' table_object_name='""" + self.table_object_name
                    + """' field_object_name='""" + self.object_name + """' placeholder='"""
                    + self.caption + """' id='x_""" + self.object_name + """' name='x_"""
                    + self.object_name + """' >""" + ( "" if is_empty(self.current_value) else str(self.current_value) )  + """</textarea>
                </div>
            """)
        elif action == Actions.List.code and self.field_type == InputTypes.TEXT.code:
            return str("""
                <div >
                <input class='form-control' table_object_name='""" + self.table_object_name
                       + """' field_object_name='""" + self.object_name + """' placeholder='"""
                       + self.caption + """' id='xf_""" + self.object_name + """' name='xf_"""
                       + self.object_name + """' value='""" + str(
                        self.ex_search["value"]) + """'></input>
                </div>
            """)
        elif action == Actions.Add.code and self.field_type == InputTypes.RADIOBUTTON.code:
            html = """
                <div class='col-lg-8'>
                <input hidden class='form-control' table_object_name='""" + self.table_object_name + """' 
                   field_object_name='""" + self.object_name + """' placeholder='""" + self.caption + """' 
                   id='x_""" + self.object_name + """' name='x_""" + self.object_name + """' 
                   value='""" + self.str_default_add() + """'></input>
                """
            # fill with lookup data
            if self.lookup_data is not None:  # only loop when you have values
                int_ind = 1 #  use for creating ids for radio buttons
                for ele in self.lookup_data:
                    html += """
                                <div class='form-check form-check-inline'>
                                  <input class='form-check-input' type='radio'  name='x_""" + self.object_name + """' 
                                    data-radiofor='x_""" + self.object_name + """'
                                  id='x_""" + self.object_name + str(int_ind) + """' value='""" + str(ele[0]) + """'
                                  """ + ('checked' if str(self.str_default_add()) == str(ele[0]) else '') + """
                                  onchange='capture_selection_radio_button(this)'>
                                  <label class='form-check-label' for='x_""" + self.object_name + str(int_ind) + """'>
                                  """ + str(ele[1]) + """
                                  </label>
                                </div>"""

                    int_ind += 1 # got to next

            html += """</div>"""
            return html
        elif action == Actions.Edit.code and self.field_type == InputTypes.RADIOBUTTON.code:
            html = """
                <div class='col-lg-8'>
                <input hidden class='form-control' table_object_name='""" + self.table_object_name + """' 
                   field_object_name='""" + self.object_name + """' placeholder='""" + self.caption + """' 
                   id='x_""" + self.object_name + """' name='x_""" + self.object_name + """' 
                   value='""" + ( "" if is_empty(self.current_value) else str(self.current_value) ) + """'></input>
                """
            # fill with lookup data
            if self.lookup_data is not None:  # only loop when you have values
                int_ind = 1 #  use for creating ids for radio buttons
                for ele in self.lookup_data:
                    html += """
                                <div class='form-check form-check-inline'>
                                  <input class='form-check-input' type='radio'  name='x_""" + self.object_name + """' 
                                    data-radiofor='x_""" + self.object_name + """'
                                  id='x_""" + self.object_name + str(int_ind) + """' value='""" + str(ele[0]) + """'
                                  """ + ('checked' if str(self.current_value) == str(ele[0]) else '') + """
                                  onchange='capture_selection_radio_button(this)'>
                                  <label class='form-check-label' for='x_""" + self.object_name + str(int_ind) + """'>
                                  """ + str(ele[1]) + """
                                  </label>
                                </div>"""

                    int_ind += 1 # got to next

            html += """</div>"""
            return html
        elif action == Actions.List.code and self.field_type == InputTypes.RADIOBUTTON.code:
            html = """
                <div class='col-lg-8'>
                <input hidden class='form-control' table_object_name='""" + self.table_object_name + """' 
                   field_object_name='""" + self.object_name + """' placeholder='""" + self.caption + """' 
                   id='xf_""" + self.object_name + """' name='xf_""" + self.object_name + """' 
                   value='""" + self.ex_search["value"] + """'></input>
                """
            # fill with lookup data
            if self.lookup_data is not None:  # only loop when you have values
                int_ind = 1 #  use for creating ids for radio buttons
                for ele in self.lookup_data:
                    html += """
                                <div class='form-check form-check-inline'>
                                  <input class='form-check-input' type='radio'  name='xf_""" + self.object_name + """' 
                                    data-radiofor='xf_""" + self.object_name + """'
                                  id='xf_""" + self.object_name + str(int_ind) + """' value='""" + str(ele[0]) + """'
                                  """ + ('checked' if str(self.ex_search["value"]) == str(ele[0]) else '') + """
                                  onchange='capture_selection_radio_button(this)'>
                                  <label class='form-check-label' for='xf_""" + self.object_name + str(int_ind) + """'>
                                  """ + str(ele[1]) + """
                                  </label>
                                </div>"""

                    int_ind += 1 # got to next

            html += """</div>"""
            return html
        elif action == Actions.View.code :
            html = """
                <div class='col-lg-8'>
                <label class='col-lg-12'>
                """ + str(self.view_value) + """
                </label>
                </div> 
                """
            return html
        else:
            return """<div class='alert alert-danger'>
              <h5><i class='icon fa fa-ban'></i> Unsupported Control!</h5>
              This input control is not yet supported.
            </div>"""

    def html_caption(self, action):
        if action == Actions.Add.code or action == Actions.Edit.code:
            return """
                <label class='col-lg-4'>
                """ + self.caption + (
                "<i class='fa fa-asterisk ew-required' ></i>" if (self.required[action] is True) else "") + """
                </label>
                """
        elif action == Actions.List.code:
            return """ <label >""" + self.caption + """ <span class="badge bg-info">""" \
                   + self.ex_search["filter_type"] + """</span>
                </label>
                """
        else:
            return """
                    <label class='col-lg-4'>
                    """ + self.caption + """
                    </label>
                    """

    def is_valid_type(self):
        if is_empty(self.current_value) is False:  # test only if it has a value
            if self.validation_type == ValidationTypes.NONE.code:
                return True
            elif self.validation_type == ValidationTypes.INT.code:
                return is_integer(self.current_value)
            elif self.validation_type == ValidationTypes.FLOAT.code:
                return is_float(self.current_value)
            elif self.validation_type == ValidationTypes.DATETIME.code:
                return is_datetime(self.current_value)
            elif self.validation_type == ValidationTypes.DATE.code:
                return is_date(self.current_value)
            else:
                return False
        else:
            return True

    def is_required_valid(self, action):
        return False if (self.required[action] is True and is_empty(self.current_value) is True) else True


class TableFieldListItem:
    object_name = 'unknown'
    current_value = None
    view_value = None

    def __init__(self, m_object_name, m_current_value=None, m_view_value=None):
        self.object_name = str(m_object_name)
        self.current_value = m_current_value
        self.view_value = str(m_view_value)


class TableObjectBase:
    object_name = 'unknown'
    caption = 'unknown'
    current_action = 'unknown'
    fields = {}
    pagination = {"records_per_page": EW.records_per_page, 'current_page': 1,
                  'start_limit': 0, 'end_limit': EW.records_per_page}

    def __init__(self):
        self.caption = Lang.tbl_phrase(self.object_name)

    def select_all_records(self):
        # put custom implementation for model
        return {"list": list(), "count": 0}

    def apply_sql_limit(self,query):
        try:
            #if not exporting to excel then limit output
            if self.current_action != Actions.ExcelExport.code:
                from django.db.models import QuerySet
                if type(query) is str:
                    limited_sql = query + " LIMIT  " + str(self.pagination["start_limit"]) + ", " + str(self.pagination["records_per_page"])
                    return my_custom_sql(limited_sql)
                elif type(query) is QuerySet:
                    return query[self.pagination["start_limit"]:self.pagination["end_limit"]]
            else:
                from django.db.models import QuerySet
                if type(query) is str:
                    return my_custom_sql(query)
                elif type(query) is QuerySet:
                    return query
        except:
            return list()

    def count_sql_result(self,query):
        try:
            from django.db.models import QuerySet
            if type(query) is str:
                count_sql = """select ifnull(count(count_list.id),0) as value from ( """ + query + """ ) as count_list """
                return int(my_custom_sql(count_sql)[0]["value"])
            elif type(query) is QuerySet:
                return query.__len__()

        except:
            return 0

    # list data
    def get_list_data(self):
        data = self.select_all_records()
        data_list = list()
        for item in data["list"]:
            dict_obj = item.__dict__
            an_item = dict()
            for a_field in self.fields:
                an_item[a_field] = TableFieldListItem(a_field, dict_obj[a_field], dict_obj[a_field])
            data_list.append(an_item)
        return {"list": data_list, "count": data["count"]}

    def html_add_form_page_load(self):
        return None

    def html_add_form(self):
        # call page load
        self.html_add_form_page_load()

        api_response = {"header_title": self.caption + "s", "content_title": self.caption + " Add",
                        "content_body": "content_body", "content_footer": "content_footer",
                        "error": True, "error_msg": "Unknown API Error"}
        html_text = """
        <div class="card card-info">
        <div class="card-body">
        <div class='col-lg-12'>
        """
        # add all add form fields
        for a_field in self.fields:
            html_text += self.fields[a_field].html_control(Actions.Add.code)
        html_text += """
        </div>
        </div>
        <div class="card-footer">
          <button onclick="SubmitForm('""" + Actions.SubmitAdd.code + """','""" + self.object_name \
                     + """')" class="btn btn-info">""" + Lang.phrase("btn_add") + """</button>
          <button onclick="ShowForm('""" + Actions.List.code + """','""" + self.object_name \
                     + """')" class="btn btn-default float-right">""" + Lang.phrase("btn_cancel") + """</button>
        </div>
        </div>
        """

        api_response["error"] = False
        api_response["error_msg"] = "Completed Successfully"
        api_response["content_body"] = html_text
        return api_response

    def html_edit_form_page_load(self):
        return None

    def html_edit_form(self):
        self.html_edit_form_page_load()
        api_response = {"header_title": self.caption + "s", "content_title": self.caption + " Edit",
                        "content_body": "content_body", "content_footer": "content_footer",
                        "error": True, "error_msg": "Unknown API Error"}
        html_text = """
        <div class="card card-info">
        <div class="card-body">
        <div class='col-lg-12'>
        """
        # add all add form fields
        for a_field in self.fields:
            html_text += self.fields[a_field].html_control(Actions.Edit.code)
        html_text += """
        </div>
        </div>
        <div class="card-footer">
          <button onclick="SubmitForm('""" + Actions.SubmitEdit.code + """','""" + self.object_name \
                     + """')" class="btn btn-info">""" + Lang.phrase("btn_save") + """</button>
          <button onclick="ShowForm('""" + Actions.List.code + """','""" + self.object_name \
                     + """')" class="btn btn-default float-right">""" + Lang.phrase("btn_cancel") + """</button>
        </div>
        </div>
        """

        api_response["error"] = False
        api_response["error_msg"] = "Completed Successfully"
        api_response["content_body"] = html_text

        return api_response

    def clear_field_data(self):
        for a_field in self.fields:
            # clear any cache
            self.fields[a_field].current_value = ""
            self.fields[a_field].view_value = ""
            self.fields[a_field].ex_search["value"] = ""
            self.fields[a_field].ex_search["value2"] = ""

    def extract_data(self, posted_data):
        #clear all cached data
        self.clear_field_data()

        import json
        if posted_data.get("api_action", None) is not None:  # if there is data from add / edit/ delete from
            self.current_action = str(posted_data["api_action"])
        if posted_data.get("form_values", "{}") != "{}":  # if there is data from add / edit/ delete from
            self.extract_form_data(json.loads(posted_data["form_values"]))
        if posted_data.get("ex_filter_values", "{}") != "{}":  # if there is data from add / edit/ delete from
            self.extract_ex_filter(json.loads(posted_data["ex_filter_values"]))
        if is_empty(posted_data.get("object_id", None)) is False:  # if there is data from add / edit/ delete from
            self.get_record_data(posted_data["object_id"])
        if posted_data.get("pagination", "{}") != "{}":  # if there is pagination data
            self.extract_pagination_data(json.loads(posted_data["pagination"]))

    def extract_form_data(self, data):

        for a_field in self.fields:
            if data.get(self.fields[a_field].object_name, None) is not None:
                self.fields[a_field].current_value = data.get(self.fields[a_field].object_name)
                self.fields[a_field].view_value = self.fields[a_field].current_value

    def extract_ex_filter(self, data):
        for a_field in self.fields:
            if self.fields[a_field].ex_search["show"] is True:
                # get value 1
                if data.get(self.fields[a_field].object_name, None) is not None:
                    self.fields[a_field].ex_search["value"] = data.get(self.fields[a_field].object_name)
                # get value 1
                if data.get(('y_' + self.fields[a_field].object_name), None) is not None:
                    self.fields[a_field].ex_search["value2"] = data.get(('y_' + self.fields[a_field].object_name))

    def extract_pagination_data(self, data):
        self.pagination["records_per_page"] = int(data.get("records_per_page", self.pagination["records_per_page"]))
        self.pagination["current_page"] = data.get("current_page", self.pagination["current_page"])
        self.pagination["start_limit"] = (self.pagination["current_page"] - 1) * self.pagination["records_per_page"]
        self.pagination["end_limit"] = self.pagination["current_page"] * self.pagination["records_per_page"]

    def select_a_record(self, object_id):
        # put custom implementation for model
        return list()

    def get_record_data(self, object_id):
        obj = self.select_a_record(object_id)
        if obj.count() == 1:
            dict_obj = obj[0].__dict__
            for a_field in self.fields:
                if dict_obj.get(self.fields[a_field].object_name, None) is not None:
                    self.fields[a_field].current_value = dict_obj.get(self.fields[a_field].object_name)
                    self.fields[a_field].view_value = self.fields[a_field].current_value

    def validate_form_data(self):
        api_response = {"error": False, "error_msg": "Validation Passed Successfully"}

        # perform required primary validations for form data
        for a_field in self.fields:
            if self.fields[a_field].is_required_valid(self.current_action) is False:  # validate required
                api_response["error"] = True
                api_response["error_msg"] = "Please Enter Required Field " + self.fields[a_field].caption
                break  # stop looping
            if self.fields[a_field].is_valid_type() is False:  # validate data type
                api_response["error"] = True
                api_response["error_msg"] = self.fields[a_field].caption + " must be a valid " + self.fields[
                    a_field].validation_type
                break  # stop looping
        return api_response

    def row_inserted(self):
        api_response = {"error": False, "error_msg": self.caption + " Added Successfully"}
        # other after effects
        # perform logging
        return api_response

    def insert_row(self):
        api_response = {"error": True, "error_msg": "No Implimentation found"}
        return api_response

    def submit_add_form(self):
        api_response = self.validate_form_data()
        if api_response["error"] is True:  # check primary validations
            return api_response
        api_response = self.insert_row()  # run insert query
        if api_response["error"] is True:  # check insert worked
            return api_response
        api_response = self.row_inserted()

        return api_response

    def update_row(self):
        api_response = {"error": True, "error_msg": "No Implimentation found"}
        return api_response

    def row_updated(self):
        api_response = {"error": False, "error_msg": self.caption + " Updated Successfully"}
        # other after effects
        # perform logging
        return api_response

    def submit_edit_form(self):
        api_response = self.validate_form_data()
        if api_response["error"] is True:  # check primary validations
            return api_response
        api_response = self.update_row()  # run insert query
        if api_response["error"] is True:  # check insert worked
            return api_response
        api_response = self.row_updated()

        return api_response

    def delete_row(self):
        api_response = {"error": True, "error_msg": "No Implimentation found"}
        return api_response

    def row_deleted(self):
        api_response = {"error": False, "error_msg": self.caption + " Deleted Successfully"}
        # other after effects
        # perform logging
        return api_response

    def submit_delete_form(self):

        api_response = {"error": False, "error_msg": "Validation Passed Successfully"}
        #  only validate id value
        # perform required primary validations for form data
        if self.fields["id"].is_required_valid(self.current_action) is False:  # validate required
            api_response["error"] = True
            api_response["error_msg"] = "Please Enter Required Field " + self.fields["id"].caption
            return api_response
        elif self.fields["id"].is_valid_type() is False:  # validate data type
            api_response["error"] = True
            api_response["error_msg"] = self.fields["id"].caption + " must be a valid " + self.fields["id"].field_type
            return api_response
        else:
            api_response = self.delete_row()  # run insert query
            if api_response["error"] is True:  # check insert worked
                return api_response
            api_response = self.row_deleted()

        return api_response

    def draw_ex_search_panel(self):
        html = """"""
        # draw all fields that have extended search
        for a_field in self.fields:
            if self.fields[a_field].ex_search["show"] is True:
                html += self.fields[a_field].html_ex_search(Actions.List.code)
        # add button
        if html != """""":
            html = """
                        <div id='list_ex_filter_content' class='row text-center callout callout-info'>
                        """ + html

            html += """<div class="col-lg-12 text-left">
                    """ + ex_filter_link(self.object_name) + """
                    </div>
                    """
            html += """</div>"""
        return html

    def html_list_export(self):
        api_response = { "download_url": "export_url", "error": True, "error_msg": "Failed to export"}

        # draw the rows
        list_data = self.get_list_data()["list"]
        # organise for export
        # add the captions
        export_dict = list()
        # reformat all the data
        for ele in list_data:
            an_item = dict()
            for a_field in self.fields:
                #  ignore fields not passed 
                if ele.get(self.fields[a_field].object_name,None) is not None:
                    an_item[self.fields[a_field].caption] = ele[self.fields[a_field].object_name].view_value
            export_dict.append(an_item)
        try:
            api_response["download_file_name"] = excel_export(export_dict, self.caption + " export.xls")
            api_response["error"] = False
            api_response["error_msg"] = "Exported successfully"
        except:
            api_response["error"] = True
            api_response["error_msg"] = "Error while exporting"
        return api_response

    def html_list_form_page_load(self):
        return None

    def html_list_form(self):
        self.html_list_form_page_load()
        api_response = {"header_title": self.caption + "s", "content_title": self.caption + " List",
                        "content_body": "content_body", "content_footer": "content_footer",
                        "error": True, "error_msg": "Unknown API Error"}

        # draw the rows
        list_data = self.get_list_data()

        # draw search panel
        html_text = self.draw_ex_search_panel()

        # draw list content
        html_text += """
        <div id='list_data_content'>
        <div class='row text-center'>
        <div class="col-xs-6 col-sm-4 col-md-3">
        """ + export_excel_link(self.object_name) + """
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3">
        """ + add_link(self.object_name) + """
        </div>
        <div class="col-xs-12 col-sm-4 col-md-6">
        """ + pagination(self.object_name, list_data["count"], self.pagination["current_page"], self.pagination["records_per_page"]) + """
        </div>
        </div>
        <div class='card table-responsive col-lg-12' style='padding:10px;width:96%;margin-left: 2%;margin-right: 2%;'>
        <table class='table-bordered table-hover table-striped text-center' style='width: 100%;'>
        <thead>
        <tr>
        """

        # draw all fields
        for a_field in self.fields:
            if self.fields[a_field].show_on[Actions.List.code] is True:
                html_text += """<td>""" + self.fields[a_field].caption + """</td>"""
        # draw options area
        html_text += """<td>&nbsp</td>
        </tr>
        </thead>
        <tbody>
        """

        if list_data["count"] == 0:
            html_text += """<tr><td colspan='30'>
            <div class='alert alert-danger'>
              <h5><i class='icon fa fa-ban'></i> No Data!</h5>
              No """ + self.caption + """ records found.
            </div>
            </td></tr>
            """
        else:
            # select full list
            for item in list_data["list"]:
                html_text += """<tr>"""
                for a_field in self.fields:
                    if self.fields[a_field].show_on[Actions.List.code] is True:
                        html_text += """<td>""" + str(item[a_field].view_value) + """</td>"""
                # add list options
                html_text += """<td >
                          """ + edit_link(self.object_name, item[self.fields["id"].object_name].current_value) + """
                          """ + delete_link(self.object_name, item[self.fields["id"].object_name].current_value) + """
                                </td>
                                """

                html_text += """</tr>"""

        html_text += """
        </tbody>
        </table>
        </div>
        </div>
        """

        api_response["error"] = False
        api_response["error_msg"] = "Completed Successfully"
        api_response["content_body"] = html_text

        return api_response

    def perform_action(self, data=None):

        # in case data was passed from a form
        if data is None:
            data = dict()
        self.extract_data(data)

        # perform desired action
        api_response = {"error": True, "error_msg": "Unsupported Operation"}
        if Security.check_permission(self.object_name, data["api_action"]) is False:
            api_response["error"] = True
            api_response["error_msg"] = Lang.phrase("permission_denied")
        elif data["api_action"] == Actions.List.code:
            api_response = self.html_list_form()
        elif data["api_action"] == Actions.Add.code:
            api_response = self.html_add_form()
        elif data["api_action"] == Actions.SubmitAdd.code:
            api_response = self.submit_add_form()
        elif data["api_action"] == Actions.Edit.code:
            api_response = self.html_edit_form()
        elif data["api_action"] == Actions.SubmitEdit.code:
            api_response = self.submit_edit_form()
        elif data["api_action"] == Actions.Delete.code:
            api_response = self.submit_delete_form()
        elif data["api_action"] == Actions.ExcelExport.code:
            api_response = self.html_list_export()

        return api_response
