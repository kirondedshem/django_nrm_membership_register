from JailuApp.classes.base_structures import *

class TablePerformanceDashBoard(TableObjectBase):
    # table specific settings
    object_name = 'performance_dashboard'
    js_execution_timer = 10000
    js_idle_execution_timer = 120000

    # field settings
    fields = {

    }

    def html_list_form(self):
        api_response = {"header_title": self.caption + "s", "content_title": self.caption + " List",
                        "content_body": "content_body", "content_footer": "content_footer",
                        "error": True, "error_msg": "Unknown API Error"}

        # draw js
        html_text = """
                        <script>

                        function get_entry_current_status()
                            {
                                var action = "get_entry_current_status"
                                var object = '""" + self.object_name + """'
                                $.ajax({
                                    type: "POST",
                                    cache: false,
                                    url: "api",
                                    dataType :"json",
                                    data: { rand_time: new Date().getTime(), api_object: object, api_action: action} ,
                                    success: function(response){
                                      //console.log(response);
                                      //read data out
                                      if(response.error === false)
                                      { 
                                            $("#performance_dashboard_member_status").html(response.member_status_html);
                                            $("#performance_dashboard_page_status").html(response.page_status_html);
                                            $("#performance_dashboard_book_status").html(response.book_status_html);
                                      }
                                      else
                                      {
                                           //show error message
                                          ShowFailureMessage(response.error_msg)
                                          console.log('failed to pick get_map_activity_route error:'+response.error_msg);
                                      }
                                    },
                                    error: function(data){
                                    console.log('failed to Load form:',data);
                                    //show error message
                                    ShowFailureMessage("Oops Server failed to complete this request, Check your Internet Connection");

                                    }
                                    });

                            }

                        get_entry_current_status();


                        draw_fusion_chart('mscolumn2d','daily_entry_vs_agent','""" + self.object_name + """','entry_vs_agent',parameters = {chart_filter:'daily'});
                        draw_fusion_chart('mscolumn2d','last_7_days_entry_vs_agent','""" + self.object_name + """','entry_vs_agent',parameters = {chart_filter:'last_7_days'});
                        draw_fusion_chart('mscolumn2d','monthly_entry_vs_agent','""" + self.object_name + """','entry_vs_agent',parameters = {chart_filter:'monthly'});
                        draw_fusion_chart('mscolumn2d','overall_entry_vs_agent','""" + self.object_name + """','entry_vs_agent',parameters = {chart_filter:'overall'});
                        
                        get_draw_html('page_allocation_performance','"""+self.object_name+"""','page_allocation_performance',parameters = {});
                        

                        

                        </script>
                       """

        # draw list content
        html_text += """
        <div class='col-lg-12 row'>
        """

        html_text += """
                <div class='col-lg-12 row'>


                    <div class='card-shadow-primary border mb-3 card border-primary col-xs-12 col-sm-6 col-lg-4'>
                        <div class="card-header"><i class="header-icon pe-7s-users icon-gradient bg-grow-early"> </i>
                        Member Entry Status
                        </div>
                        <div class='card-body col-lg-12 row' id='performance_dashboard_member_status' style="padding: 0.5rem;">
                        performance_dashboard_member_status
                        </div>

                    </div>

                    <div class='card-shadow-primary border mb-3 card border-primary col-xs-12 col-sm-6 col-lg-4'>
                        <div class="card-header"><i class="header-icon pe-7s-note2 icon-gradient bg-grow-early"> </i>
                        Pages Completion Status
                        </div>
                        <div class='card-body col-lg-12 row' id='performance_dashboard_page_status' style="padding: 0.5rem;">
                        performance_dashboard_page_status
                        </div>

                    </div>

                    <div class='card-shadow-primary border mb-3 card border-primary col-xs-12 col-sm-6 col-lg-4'>
                        <div class="card-header"><i class="header-icon pe-7s-notebook icon-gradient bg-grow-early"> </i>
                        Book Completion Status
                        </div>
                        <div class='card-body col-lg-12 row' id='performance_dashboard_book_status' style="padding: 0.5rem;">
                        performance_dashboard_book_status
                        </div>

                    </div>

                </div>

                <div class="main-card mb-3 card col-lg-12" >

                    <div class="card-header" style="height: auto;"><i class="header-icon lnr-screen icon-gradient bg-grow-early"> </i>
                            Today Only Recap
                            <ul  class=" body-tabs body-tabs-layout tabs-animated body-tabs-animated nav pt-0 pb-3">
                                <li class="nav-item" style="max-height: 25px;">
                                    <a role="tab" class="nav-link  active" data-toggle="tab" href="#daily_entry_vs_agent" aria-selected="true"
                                    style="padding: .1rem .3rem;">
                                        <span>Entry Per Agent</span>
                                    </a>
                                </li>
                                <li class="nav-item" style="max-height: 25px;">
                                    <a role="tab" class="nav-link" data-toggle="tab" href="#page_allocation_performance" aria-selected="false"
                                    style="padding: .1rem .3rem;">
                                        <span>Page Allocation Performance</span>
                                    </a>
                                </li>
                                

                            </ul>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">
                            <div style="height: 300px;" class="tab-pane active" id="daily_entry_vs_agent">
                            <p>daily_entry_vs_agent</p>
                            </div>
                            <div style="height: auto;" class="tab-pane" id="page_allocation_performance">
                            <p>page_allocation_performance</p>
                            </div>
                            

                        </div>
                    </div>
                </div>



                <div class="main-card mb-3 card col-lg-12" >

                    <div class="card-header" style="height: auto;"><i class="header-icon lnr-screen icon-gradient bg-grow-early"> </i>
                            Last 7 Days Recap
                            <ul  class=" body-tabs body-tabs-layout tabs-animated body-tabs-animated nav pt-0 pb-3">
                                <li class="nav-item" style="max-height: 25px;">
                                    <a role="tab" class="nav-link  active" data-toggle="tab" href="#last_7_days_entry_vs_agent" aria-selected="true"
                                    style="padding: .1rem .3rem;">
                                        <span>Entry Per Agent</span>
                                    </a>
                                </li>
                                

                            </ul>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">
                            <div style="height: 300px;" class="tab-pane active" id="last_7_days_entry_vs_agent">
                            <p>last_7_days_entry_vs_agent</p>
                            </div>
                            

                        </div>
                    </div>
                </div>


                <div class="main-card mb-3 card col-lg-12">
                    <div class="card-header" style="height: auto;"><i class="header-icon lnr-screen icon-gradient bg-grow-early"> </i>
                            Monthly Recap

                            <ul  class=" body-tabs body-tabs-layout tabs-animated body-tabs-animated nav pt-0 pb-3">
                                <li class="nav-item" style="max-height: 25px;">
                                    <a role="tab" class="nav-link  active" data-toggle="tab" href="#monthly_entry_vs_agent" aria-selected="true" 
                                     style="padding: .1rem .3rem;">
                                        <span>Entry Per Agent</span>
                                    </a>
                                </li>
                                

                            </ul>
                    </div>

                    <div class="card-body">
                        <div class="tab-content">
                            <div style="height: 300px;" class="tab-pane active" id="monthly_entry_vs_agent">
                            <p>monthly_entry_vs_agent</p>
                            </div>
                            

                        </div>
                    </div>
                </div>



                <div class="main-card mb-3 card col-lg-12">
                    <div class="card-header" style="height: auto;"><i class="header-icon lnr-screen icon-gradient bg-grow-early"> </i>
                            Overall Recap

                            <ul  class=" body-tabs body-tabs-layout tabs-animated body-tabs-animated nav pt-0 pb-3">
                                <li class="nav-item" style="max-height: 25px;">
                                    <a role="tab" class="nav-link  active" data-toggle="tab" href="#overall_entry_vs_agent" aria-selected="true" 
                                     style="padding: .1rem .3rem;">
                                        <span>Entry Per Agent</span>
                                    </a>
                                </li>
                                

                            </ul>
                    </div>


                    <div class="card-body">
                        <div class="tab-content">
                            <div style="height: 300px;" class="tab-pane active" id="overall_entry_vs_agent">
                            overall_entry_vs_agent
                            </div>
                            

                        </div>
                    </div>
                </div>
                        """

        html_text += """
                </div>
                """

        api_response["error"] = False
        api_response["error_msg"] = "Completed Successfully"
        api_response["content_body"] = html_text

        return api_response

    def entry_vs_agent(self, data):
        api_response = {"error": True, "error_msg": "Unknown API error", "html": ""}
        chart_options = dict()
        chart_options["caption"] = "Overall Entry Per Agent"
        chart_options["subcaption"] = "All Time"
        chart_options["xaxisname"] = "Years"
        chart_options["yaxisname"] = "Calls"
        chart_options["formatnumberscale"] = 1
        chart_options["plottooltext"] = "<b>$dataValue</b> entry(s) by <b>$seriesName</b> for $label"
        chart_options["drawcrossline"] = 1
        chart_options["exportEnabled"] = 1

        chart_filter = data.get("chart_filter", "")
        date_filter = " 1=1 "
        label_category = " year(det.entry_date) "
        if chart_filter == "overall":
            label_category = " year(det.entry_date) "
            date_filter = " 1=1 "
            chart_options["caption"] = "Overall Entry Per Agent"
            chart_options["subcaption"] = "All Time"
            chart_options["xaxisname"] = "Years"
        elif chart_filter == "monthly":
            label_category = " month(det.entry_date) "
            date_filter = " year(det.entry_date) = year(now()) "
            chart_options["caption"] = "Monthly Entry Per Agent"
            chart_options["subcaption"] = "This Year"
            chart_options["xaxisname"] = "Months"
        elif chart_filter == "last_7_days":
            label_category = " DATE(det.entry_date) "
            date_filter = " DATE(det.entry_date) >= DATE(NOW() + INTERVAL -7 DAY) "
            chart_options["caption"] = "Weekly Entry Per Agent"
            chart_options["subcaption"] = "Last 7 Days"
            chart_options["xaxisname"] = "Days"
        elif chart_filter == "daily":
            label_category = " DATE(det.entry_date) "
            date_filter = " DATE(det.entry_date) = DATE(now()) "
            chart_options["caption"] = "Daily Entry Per Agent"
            chart_options["subcaption"] = "Today"
            chart_options["xaxisname"] = "Days"
        if is_empty(chart_filter):  #
            api_response["error"] = True
            api_response["error_msg"] = "Please specify chart_filter"
        else:  # get new html
            from django.db import connection
            sql = """select """+label_category+""" as label_category,  ua.first_name as seriesname , count(det.id) AS value 
            from nrm_member det  join user_account ua on ua.id = det.entered_by_id  
            where """+date_filter+""" GROUP BY """+label_category+""", ua.first_name ORDER BY """+label_category+""" DESC """
            result = my_custom_sql(sql)

            category1 = list()  # define the various categories

            for ele in result:
                # collect all categories
                # format
                cat_formated_str = str(ele["label_category"])
                if chart_filter == "monthly":
                    cat_formated_str = get_short_month(ele["label_category"])
                elif chart_filter == "last_7_days":
                    cat_formated_str = get_date_only_str(ele["label_category"])
                elif chart_filter == "daily":
                    cat_formated_str = get_date_only_str(ele["label_category"])
                # check if its not already considered
                if category1.__len__() == 0 or cat_formated_str not in [x["label"] for x in category1]:
                    category1.append({"label": cat_formated_str})

            #get all dataset & seriesnames
            dataset = list()
            serienames = dict()
            for ele in result:
                # format
                ser_formated_str = str(ele["seriesname"])
                #check if its not already considered
                if serienames.__len__() == 0 or ser_formated_str not in [x for x in serienames]:
                    # intialise each serie with its values
                    #dataset.append({"seriesname": formated_str, "data": [{"value": 0} for x in category1]})
                    serienames[ser_formated_str] = {"seriesname": ser_formated_str, "data": [{"value": 0} for x in category1]}

                # get the category anme
                cat_formated_str = str(ele["label_category"])
                if chart_filter == "monthly":
                    cat_formated_str = get_short_month(ele["label_category"])
                elif chart_filter == "last_7_days":
                    cat_formated_str = get_date_only_str(ele["label_category"])
                elif chart_filter == "daily":
                    cat_formated_str = get_date_only_str(ele["label_category"])
                # check if its not already considered
                #set value if available
                serienames[ser_formated_str]["data"][category1.index({"label": cat_formated_str})]["value"] = ele["value"]

            #  add categories to the list
            categories = list()
            categories.append({"category": category1})

            api_response["chart_data"] = {"categories": categories, "dataset": [serienames[k] for k in serienames]
                                          , "chart_options": chart_options}
            api_response["error"] = False
            api_response["error_msg"] = "Success"

        return api_response


    def get_entry_current_status(self, data):
        api_response = {"error": True, "error_msg": "Unknown API error", "html": ""}

        # project = UptimeMonitor.objects.filter(id=data.get("object_id", ""))


        expected = my_custom_sql(""" select ifnull(sum(number_of_records),0) as value from nrm_book_page """)[0]["value"]
        actual = my_custom_sql(""" select ifnull(count(id),0) as value from nrm_member """)[0]["value"]
        percentage = int((actual / expected) * 100)
        member_status_html = """
        <div class="widget-content col-12">
            <div class="widget-content-outer">
                <div class="widget-content-wrapper">
                    <div class="widget-content-left">
                        <div class="widget-numbers fsize-3 text-muted">
                            """ + str(percentage) + """%
                        </div>
                    </div>
                    <div class="widget-content-right">
                        <div class="text-muted opacity-6">
                            """ + str(actual) + """ / """ + str(expected) + """ Members
                        </div>
                    </div>
                </div>
                <div class="widget-progress-wrapper mt-1">
                    <div class="progress-bar-sm progress-bar-animated-alt progress">
                        <div class="progress-bar bg-success" role="progressbar" 
                        aria-valuenow='""" + str(percentage) + """' aria-valuemin="0" aria-valuemax="100" 
                        style='width: """ + str(percentage) + """%;'></div>
                    </div>
                </div>
            </div>
        </div>
        """

        api_response["member_status_html"] = member_status_html



        expected = my_custom_sql(""" select ifnull(count(id),0) as value from nrm_book_page """)[0][
            "value"]
        actual = my_custom_sql(""" select ifnull(count(nbp.id),0) as value from nrm_book_page nbp 
        where nbp.number_of_records <= ifnull( (select count(nm.id) from nrm_member nm where nm.nrm_book_page_id = nbp.id) ,0)  """)[0]["value"]
        percentage = int((actual / expected) * 100)
        ff =8
        page_status_html = """
                <div class="widget-content col-12">
                    <div class="widget-content-outer">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left">
                                <div class="widget-numbers fsize-3 text-muted">
                                    """ + str(percentage) + """%
                                </div>
                            </div>
                            <div class="widget-content-right">
                                <div class="text-muted opacity-6">
                                    """ + str(actual) + """ / """ + str(expected) + """ Pages
                                </div>
                            </div>
                        </div>
                        <div class="widget-progress-wrapper mt-1">
                            <div class="progress-bar-sm progress-bar-animated-alt progress">
                                <div class="progress-bar bg-success" role="progressbar" 
                                aria-valuenow='""" + str(percentage) + """' aria-valuemin="0" aria-valuemax="100" 
                                style='width: """ + str(percentage) + """%;'></div>
                            </div>
                        </div>
                    </div>
                </div>
                """

        api_response["page_status_html"] = page_status_html

        expected = my_custom_sql(""" select ifnull(count(id),0) as value from nrm_book """)[0][
            "value"]
        actual = my_custom_sql(""" select ifnull(count(nb.id),0) as value from nrm_book nb
                where 
                ifnull( (select ifnull(sum(nbp.number_of_records),0) from nrm_book_page nbp where nbp.nrm_book_id = nb.id) ,0)
                 <= ifnull( (select count(nm.id) from nrm_member nm 
                 join nrm_book_page nbp on nm.nrm_book_page_id = nbp.id 
                 where nbp.nrm_book_id = nb.id ) ,0)  """)[
            0]["value"]
        percentage = int((actual / expected) * 100)
        ff = 8
        book_status_html = """
                        <div class="widget-content col-12">
                            <div class="widget-content-outer">
                                <div class="widget-content-wrapper">
                                    <div class="widget-content-left">
                                        <div class="widget-numbers fsize-3 text-muted">
                                            """ + str(percentage) + """%
                                        </div>
                                    </div>
                                    <div class="widget-content-right">
                                        <div class="text-muted opacity-6">
                                            """ + str(actual) + """ / """ + str(expected) + """ Books
                                        </div>
                                    </div>
                                </div>
                                <div class="widget-progress-wrapper mt-1">
                                    <div class="progress-bar-sm progress-bar-animated-alt progress">
                                        <div class="progress-bar bg-success" role="progressbar" 
                                        aria-valuenow='""" + str(percentage) + """' aria-valuemin="0" aria-valuemax="100" 
                                        style='width: """ + str(percentage) + """%;'></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        """

        api_response["book_status_html"] = book_status_html




        api_response["error"] = False
        api_response["error_msg"] = "Success"

        return api_response


    def page_allocation_performance(self, data):
        api_response = {"error": True, "error_msg": "Unknown API error", "html": ""}

        # project = UptimeMonitor.objects.filter(id=data.get("object_id", ""))

        html = """
                <div class='card table-responsive col-lg-12' style='padding:10px;width:96%;margin-left: 2%;margin-right: 2%;'>
                <table class='table-bordered table-hover table-striped text-center' style='width: 100%;'>
                <thead>
                """
        html += "<thead>"
        html += "<tr style='font-weight: bold;'>"
        html += "<td>Employee</td>"
        html += "<td>Record Entry Progress</td>"
        html += "<td>Page Completion Progress</td>"
        html += "</tr>"
        html += "</thead>"

        html += "<tbody>"

        rows = my_custom_sql(""" 
        select the_data.* from 
        (
        select
         concat_ws(' ',ua.first_name, ua.sur_name) as full_name
         , ( select ifnull(count(pa.id),0) from page_allocation pa where pa.agent_id = ua.id ) as allocated_page_count
         , ( select ifnull(sum(nbp.number_of_records),0) from page_allocation pa join nrm_book_page nbp on 
          nbp.id = pa.nrm_book_page_id where pa.agent_id = ua.id ) as allocated_record_count
         , ( select ifnull(count(nm.id),0) from page_allocation pa join nrm_book_page nbp on 
          nbp.id = pa.nrm_book_page_id join nrm_member nm on nm.nrm_book_page_id = nbp.id where pa.agent_id = ua.id and nm.entered_by_id = ua.id ) as completed_record_count
         , ( select ifnull(count(nbp.id),0) from page_allocation pa join nrm_book_page nbp on 
          nbp.id = pa.nrm_book_page_id where pa.agent_id = ua.id and ( select ifnull(count(nm.id),0) 
           from nrm_member nm where nm.nrm_book_page_id = nbp.id and nm.entered_by_id = ua.id ) >= nbp.number_of_records   ) as completed_page_count
          from user_account ua where user_group_id_id = 1
          ) as the_data order by the_data.completed_record_count DESC
        """)

        for ele in rows:
            html += "<tr>"
            html += "<td>"+ ele["full_name"] +"</td>"

            percentage = int((ele["completed_record_count"] / ele["allocated_record_count"]) * 100)
            ff = 8
            records_html = """
                            <div class="widget-content col-12">
                                <div class="widget-content-outer">
                                    <div class="widget-content-wrapper">
                                        <div class="widget-content-left">
                                            <div class="widget-numbers fsize-3 text-muted">
                                                """ + str(percentage) + """% 
                                                (""" + str(int(  ele["allocated_record_count"] - ele["completed_record_count"])) + """ left)
                                            </div>
                                        </div>
                                        <div class="widget-content-right">
                                            <div class="text-muted opacity-6">
                                                """ + str(ele["completed_record_count"]) + """ / """ + str(ele["allocated_record_count"]) + """ Records
                                            </div>
                                        </div>
                                    </div>
                                    <div class="widget-progress-wrapper mt-1">
                                        <div class="progress-bar-sm progress-bar-animated-alt progress">
                                            <div class="progress-bar bg-success" role="progressbar" 
                                            aria-valuenow='""" + str(percentage) + """' aria-valuemin="0" aria-valuemax="100" 
                                            style='width: """ + str(percentage) + """%;'></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            """

            html += "<td>"+ records_html +"</td>"

            percentage = int((ele["completed_page_count"] / ele["allocated_page_count"]) * 100)
            ff = 8
            pages_html = """
                                        <div class="widget-content col-12">
                                            <div class="widget-content-outer">
                                                <div class="widget-content-wrapper">
                                                    <div class="widget-content-left">
                                                        <div class="widget-numbers fsize-3 text-muted">
                                                """ + str(percentage) + """% 
                                                (""" + str(int(  ele["allocated_page_count"] - ele["completed_page_count"])) + """ left)
                                            </div>
                                                    </div>
                                                    <div class="widget-content-right">
                                                        <div class="text-muted opacity-6">
                                                            """ + str(ele["completed_page_count"]) + """ / """ + str(
                ele["allocated_page_count"]) + """ Pages
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="widget-progress-wrapper mt-1">
                                                    <div class="progress-bar-sm progress-bar-animated-alt progress">
                                                        <div class="progress-bar bg-success" role="progressbar" 
                                                        aria-valuenow='""" + str(percentage) + """' aria-valuemin="0" aria-valuemax="100" 
                                                        style='width: """ + str(percentage) + """%;'></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        """

            html += "<td>" + pages_html + "</td>"

            html += "</tr>"

        html += "</tbody>"

        html += """
                </table>
                </div>
                """


        api_response["error"] = False
        api_response["html"] = html
        api_response["error_msg"] = "Success"

        return api_response

    def perform_action(self, data=None):
        # in case data was passed from a form
        if data is None:
            data = dict()

        # perform desired action
        api_response = {"error": True, "error_msg": "Unsupported Operation"}
        if Security.check_permission(self.object_name, data["api_action"]) is False:
            api_response["error"] = True
            api_response["error_msg"] = Lang.phrase("permission_denied")
        elif data["api_action"] == Actions.List.code:
            api_response = self.html_list_form()
        elif data["api_action"] == "entry_vs_agent":
            api_response = self.entry_vs_agent(data)
        elif data["api_action"] == "get_entry_current_status":
            api_response = self.get_entry_current_status(data)
        elif data["api_action"] == "page_allocation_performance":
            api_response = self.page_allocation_performance(data)

        return api_response



