# Generated by Django 2.2.5 on 2020-06-01 13:17

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('JailuApp', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='NrmMemeber',
            fields=[
                ('id', models.CharField(max_length=50, primary_key=True, serialize=False)),
                ('member_number', models.CharField(max_length=20)),
                ('sur_name', models.CharField(max_length=50)),
                ('other_name', models.CharField(max_length=50)),
                ('gender', models.CharField(max_length=10)),
                ('date_of_birth', models.DateField(blank=True, null=True)),
                ('national_id_no', models.CharField(max_length=50)),
                ('youth_mark', models.CharField(max_length=10)),
                ('elderly_mark', models.CharField(max_length=10)),
                ('pwd_mark', models.CharField(max_length=10)),
                ('remarks_code', models.CharField(max_length=10)),
                ('entry_date', models.DateTimeField(blank=True, null=True)),
                ('last_modified', models.DateTimeField(blank=True, null=True)),
                ('entered_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='nrm_member_entered_by', to='JailuApp.UserAccount')),
                ('modified_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='nrm_member_modified_by', to='JailuApp.UserAccount')),
            ],
            options={
                'db_table': 'nrm_member',
            },
        ),
    ]
