# Generated by Django 2.2.5 on 2020-06-12 15:39

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('JailuApp', '0009_useraccount_primary_phone'),
    ]

    operations = [
        migrations.AddField(
            model_name='nrmmemeber',
            name='nrm_book_page',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='nrm_member_nrm_book_page', to='JailuApp.NrmBookPage'),
        ),
    ]
