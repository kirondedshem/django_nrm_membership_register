from django.test import TestCase
from JailuApp.classes.base_structures import *



# Test basic functions
class BasicFunctionsTestCase(TestCase):

    def test_is_empty(self):
        # """To correctly identify empty values"""
        self.assertEqual(is_empty(None), True, msg="None should be empty")
        self.assertEqual(is_empty(""), True, msg='"" should be empty')
        self.assertEqual(is_empty(''), True, msg="'' should be empty")
        self.assertEqual(is_empty("a"), False, msg='"a" should not be empty')
        self.assertEqual(is_empty(5), False, msg="5 should not be empty")
        self.assertEqual(is_empty(20.55), False, msg="20.55 should not be empty")

    def test_is_integer(self):
        # """To correctly identify empty values"""
        self.assertEqual(is_integer(5), True, msg='5 is a valid integer')
        self.assertEqual(is_integer(-5745), True, msg='-5745 is a valid integer')
        self.assertEqual(is_integer(50.36), False, msg="50.36 is not a valid integer")
        self.assertEqual(is_integer(-200.36), False, msg="-200.36 is not a valid integer")
        self.assertEqual(is_integer(None), False, msg="None is not a valid integer")
        self.assertEqual(is_integer(''), False, msg="'' is not a valid integer")
        self.assertEqual(is_integer('value'), False, msg="'value' is not a valid integer")

    def test_is_float(self):
        # """To correctly identify empty values"""
        self.assertEqual(is_float(5), True, msg='5 is a valid float')
        self.assertEqual(is_float(-5745), True, msg='-5745 is a valid float')
        self.assertEqual(is_float(50.36), True, msg="50.36 is a valid float")
        self.assertEqual(is_float(-200.36), True, msg="-200.36 is a valid float")
        self.assertEqual(is_float(None), False, msg="None is not a valid float")
        self.assertEqual(is_float(''), False, msg="'' is not a valid float")
        self.assertEqual(is_float('value'), False, msg="'value' is not a valid float")

    def test_is_datetime(self):
        # """To correctly identify empty values"""
        self.assertEqual(is_datetime('2020-03-07 15:20:46'), True, msg='2020-03-07 15:20:46 is a valid datetime in format %Y-%m-%d %H:%M:%S')
        self.assertEqual(is_datetime('2020-20-07 15:20:46'), False, msg='months should not be out of range')
        self.assertEqual(is_datetime('2020-07-40 15:20:46'), False, msg='days should not be out of range')
        self.assertEqual(is_datetime(50.36), False, msg="numbers is not a valid datetime")
        self.assertEqual(is_datetime('value'), False, msg="strings is not a valid datetime")
        self.assertEqual(is_datetime('2020-03-07'), False, msg="Date only is not a valid datetime")
        self.assertEqual(is_datetime('2020-03-07 03:20:46 PM'), False, msg="Datetime should be in 24 hour format")

    def test_is_date(self):
        self.assertEqual(is_date('2020-03-07'), True, msg='2020-03-07 is a valid date in format %Y-%m-%d')
        self.assertEqual(is_date('2020-20-07'), False, msg='months should not be out of range')
        self.assertEqual(is_date('2020-07-40'), False, msg='days should not be out of range')
        self.assertEqual(is_date(50.36), False, msg="numbers is not a valid date")
        self.assertEqual(is_date('value'), False, msg="strings is not a valid date")
        self.assertEqual(is_date('2020-03-07 03:20:46'), False, msg="DateTime only is not a valid date")

    def test_excel_export(self):
        # """To correctly identify empty values"""
        self.assertEqual(excel_export([{'name': 'shem', 'Age': 30, 'Sex': 'Male'}
        , {'name': 'James', 'Age': 40, 'Sex': 'Female'}], 'test_export_excel.xls')
        , 'test_export_excel.xls', msg='Serer should be able to write excel files for download')

    def test_get_date_only_str(self):
        # """To correctly identify empty values"""
        import datetime
        self.assertEqual(get_date_only_str(None), None, msg="None should return None")
        self.assertEqual(get_date_only_str(""), "", msg='"" should be ""')
        self.assertEqual(get_date_only_str(datetime.date(2020, 1, 1)), "Wed-01-Jan", msg="Date object of 2020-01-01 should return Wed-01-Jan")
        self.assertEqual(get_date_only_str("2020-01-01"), "Wed-01-Jan", msg="string 2020-01-01 should return Wed-01-Jan")
        self.assertEqual(get_date_only_str("nothing"), "nothing", msg="Improper date or propoer date string should return itself")

    def test_get_short_month(self):
        # """To correctly identify empty values"""
        self.assertEqual(get_short_month(None), None, msg="None should be None")
        self.assertEqual(get_short_month(1), "Jan", msg='1 should be Jan')
        self.assertEqual(get_short_month(12), "Dec", msg='12 should be Dec')
        self.assertEqual(get_short_month("01"), "Jan", msg='string int 01 should be Jan')
        self.assertEqual(get_short_month('kl'), "kl", msg="invalid value should be themselves")


# test language functionality
class LanguageTestCase(TestCase):

    def setUp(self):
        self.en = Language("en")

    def test_english(self):
        self.assertEqual(self.en.phrase("btn_add"), "Save", msg="English btn_add should equal to Save")
        self.assertEqual(self.en.tbl_phrase("user_group"), "User Group"
                         , msg="English user_group caption should equal to User Group")
        self.assertEqual(self.en.fld_phrase("user_group","name"), "Group Name"
                         , msg="English user_group, name caption should equal to Group Name")
        self.assertEqual(self.en.phrase("12234"), "12234", msg="Unknown phrases should default to id")
        self.assertEqual(self.en.tbl_phrase("5451534"), "5451534"
                         , msg="Unknown table caption should default to ids")
        self.assertEqual(self.en.fld_phrase("5451534", "8562413"), "8562413"
                         , msg="Unknown field caption should default to id")
        self.assertEqual(self.en.men_phrase("mi_user_group"), "User Groups"
                         , msg="Unknown table caption should default to ids")
        self.assertEqual(self.en.men_phrase("5451534"), "5451534"
                         , msg="Unknown menu caption should default to ids")


# Test file operations
class FileOperationsTestCase(TestCase):

    def test_upload_into_possible_folders(self):
        from django_jailuapp.settings import MEDIA_ROOT
        import os
        sample_file = None # open sample file
        try:

            from django.core.files.storage import FileSystemStorage
            fs = FileSystemStorage()

            sample_file = fs.open(os.path.join(MEDIA_ROOT, 'sample_file.fileext'))
        except Exception as x:
            error = str(x)

        self.assertEqual(upload_temp_file(sample_file,'sample_file.fileext')["error"], False, msg="should be able to upload a file into media temp folder")
        self.assertEqual(copy_file(os.path.join(MEDIA_ROOT, 'temp', 'sample_file.fileext'), os.path.join(MEDIA_ROOT, 'some_folder', 'sample_file.fileext'))["error"], False, msg='"" should be able to upload into some_folder media folder')
        self.assertEqual(delete_file(os.path.join(MEDIA_ROOT, 'temp', 'sample_file.fileext'))["error"], False, msg='should be able to delete file from temp media folder')
        self.assertEqual(delete_file(os.path.join(MEDIA_ROOT, 'some_folder', 'sample_file.fileext'))["error"],False, msg='"" should be able to delete from some_folder media folder')
        self.assertEqual(delete_file(os.path.join(MEDIA_ROOT, 'temp', '1436271839.fileext1'))["error"], False,msg='delete should succedd even for non existing file')

    def test_open_cvs_file(self):
        from django_jailuapp.settings import MEDIA_ROOT
        import os

        self.assertEqual(open_cvs_file(os.path.join(MEDIA_ROOT, 'sample csv file.csv'))["error"], False, msg='"" should be able to read a csv file successfully')
        self.assertEqual(open_cvs_file(os.path.join(MEDIA_ROOT, 'empty csv file.csv'))["error"], False, msg='shouold be able to successfully read an empty csv file')
        self.assertEqual(open_cvs_file(os.path.join(MEDIA_ROOT, 'sample_file.fileext'))["error"], True,
                         msg='should fail if file is non csv format')

    def test_is_integer(self):
        # """To correctly identify empty values"""
        self.assertEqual(is_integer(5), True, msg='5 is a valid integer')
        self.assertEqual(is_integer(-5745), True, msg='-5745 is a valid integer')
        self.assertEqual(is_integer(50.36), False, msg="50.36 is not a valid integer")
        self.assertEqual(is_integer(-200.36), False, msg="-200.36 is not a valid integer")
        self.assertEqual(is_integer(None), False, msg="None is not a valid integer")
        self.assertEqual(is_integer(''), False, msg="'' is not a valid integer")
        self.assertEqual(is_integer('value'), False, msg="'value' is not a valid integer")

    def test_is_float(self):
        # """To correctly identify empty values"""
        self.assertEqual(is_float(5), True, msg='5 is a valid float')
        self.assertEqual(is_float(-5745), True, msg='-5745 is a valid float')
        self.assertEqual(is_float(50.36), True, msg="50.36 is a valid float")
        self.assertEqual(is_float(-200.36), True, msg="-200.36 is a valid float")
        self.assertEqual(is_float(None), False, msg="None is not a valid float")
        self.assertEqual(is_float(''), False, msg="'' is not a valid float")
        self.assertEqual(is_float('value'), False, msg="'value' is not a valid float")

    def test_is_datetime(self):
        # """To correctly identify empty values"""
        self.assertEqual(is_datetime('2020-03-07 15:20:46'), True, msg='2020-03-07 15:20:46 is a valid datetime in format %Y-%m-%d %H:%M:%S')
        self.assertEqual(is_datetime('2020-20-07 15:20:46'), False, msg='months should not be out of range')
        self.assertEqual(is_datetime('2020-07-40 15:20:46'), False, msg='days should not be out of range')
        self.assertEqual(is_datetime(50.36), False, msg="numbers is not a valid datetime")
        self.assertEqual(is_datetime('value'), False, msg="strings is not a valid datetime")
        self.assertEqual(is_datetime('2020-03-07'), False, msg="Date only is not a valid datetime")
        self.assertEqual(is_datetime('2020-03-07 03:20:46 PM'), False, msg="Datetime should be in 24 hour format")

    def test_convert_to_datetime(self):
        # """To correctly identify empty values"""
        import datetime
        self.assertNotEqual(convert_to_datetime('2020-03-07 15:20:46'), None, msg='dates of format %Y-%m-%d %H:%M:%S are convertable by default')
        self.assertNotEqual(convert_to_datetime('2020-03-07'), None,
                            msg='dates of format %Y-%m-%d are convertable by default')
        self.assertNotEqual(convert_to_datetime(datetime.datetime.now()), None,
                            msg='datetime objects are valid')
        self.assertNotEqual(convert_to_datetime('2020-03-07 15:20:46',"%Y-%m-%d %H:%M:%S"), None,
                            msg='converting using format %Y-%m-%d %H:%M:%S should work')
        self.assertNotEqual(convert_to_datetime('2020-03-07', "%Y-%m-%d"), None,
                            msg='converting using format %Y-%m-%d %H:%M:%S should work')
        self.assertEqual(convert_to_datetime('2020-03-07 15:20:46', "%Y-%m-%d"), None,
                            msg='converting using wrong format should NOT work')
        self.assertEqual(convert_to_datetime('2020-03-07', "%Y-%m-%d %H:%M:%S"), None,
                            msg='converting using wrong format should NOT work')
        self.assertNotEqual(convert_to_datetime('2020/03/07 15:20:46', "%Y/%m/%d %H:%M:%S"), None,
                            msg='converting using format %Y/%m/%d %H:%M:%S should work')
        self.assertNotEqual(convert_to_datetime('2020/03/07', "%Y/%m/%d"), None,
                            msg='converting using format %Y/%m/%d should work')
        self.assertNotEqual(convert_to_datetime('2020/03/07 15/20/46', "%Y/%m/%d %H/%M/%S"), None,
                            msg='converting using format %Y/%m/%d %H/%M/%S should work')
        self.assertNotEqual(convert_to_datetime('03/07/2020', "%m/%d/%Y"), None,
                            msg='converting using format %m/%d/%Y should work')
        self.assertEqual(convert_to_datetime(15105151), None, msg='intergers values should fail')
        self.assertEqual(convert_to_datetime(""), None, msg='empty string values should fail')
        self.assertEqual(convert_to_datetime(None), None, msg='empty values should fail')

    def test_excel_export(self):
        # """To correctly identify empty values"""
        self.assertEqual(excel_export([{'name': 'shem', 'Age': 30, 'Sex': 'Male'}
        , {'name': 'James', 'Age': 40, 'Sex': 'Female'}], 'test_export_excel.xls')
        , 'test_export_excel.xls', msg='Serer should be able to write excel files for download')





